<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_us extends CI_Controller
{
	public function __construct()
	{
		parent:: __construct();
		$this->load->library('main');
	}

	public function index()
	{
		$data = $this->main->data_front();
		$data['page'] = $this->db->where(array('type'=>'contact_us','language_id'=>$data['language_id']))->get('pages')->row();
		$data['captcha'] = $this->main->captcha();
		$this->template->front('contact_us', $data);
	}

	public function captcha_check($str)
	{
		if ($str == $this->session->userdata('captcha_mwz')) {
			return TRUE;
		} else {
			$this->form_validation->set_message('captcha_check', 'security code was wrong, please fill again truly');
			return FALSE;
		}
	}

	public function send() {
		error_reporting(0);

		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Nama', 'required');
		$this->form_validation->set_rules('phone', 'No Telepon', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('message', 'Pesan', 'required');
		$this->form_validation->set_rules('captcha', 'Security Code', 'required|callback_captcha_check');
		$this->form_validation->set_error_delimiters('', '');

		if ($this->form_validation->run() === FALSE) {
			echo json_encode(array(
				'status' => 'error',
				'message' => 'Fill form completly',
				'errors' => array(
					'name' => form_error('name'),
					'phone' => form_error('phone'),
                    'email' => form_error('email'),
					'message' => form_error('message'),
					'captcha' => form_error('captcha'),
				)
			));
		} else {
			$email_admin = $this->db->where('use', 'yes')->get('email')->result();
			$name = $this->input->post('name');
			$email = $this->input->post('email');
			$phone = $this->input->post('phone');
			$message = $this->input->post('message');


			$message_admin = '

Dear Admin,<br /><br />
You have contact us message from web form<br />
form details as follows:<br /><br />

Name : '. $name . '<br>
Email : ' . $email . '<br>
Telephone : ' . $phone. '<br>
Message : ' . $message . '<br /><br />


Regarding,<br />
Contact Us System '.$this->main->web_name().'<br /><br />';

			foreach($email_admin as $r) {
//			    echo $r->email.', ';
				$this->main->mailer_auth('Kontak Kami - Website', $r->email, $this->main->web_name().' Administrator ', $message_admin);
			}

			echo json_encode(array(
				'status'=>'success',
				'message'=>'Reservation has been send, please wait a moment for our reply ^_^'
			));

		}
	}
}
