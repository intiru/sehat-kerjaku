<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cba extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function intro()
    {
        $this->load->view('front/cba/intro');
    }
}
