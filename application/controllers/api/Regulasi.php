<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class regulasi extends CI_Controller{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('M_regulasi');
    }
    public function recent(){
        $read = $data['regulasi'] = $this
            ->db
            ->select('team.title AS team_title, team.position AS team_position, team.description AS team_description, team.thumbnail AS team_thumbnail, regulasi.*')
            ->join('team', 'team.id = regulasi.id_team', 'left')
            ->where('regulasi.use', 'yes')
            ->order_by('regulasi.id', 'DESC')
            ->limit(5,0)
            ->get('regulasi')
            ->result();
        foreach ($read as $row){
            $row->created_at = date("d F Y",strtotime($row->created_at));
        }
        foreach ($read as$row){
            $row->thumbnail = base_url().'upload/regulasi_thumbnail/'.$row->thumbnail;
        }
        foreach ($read as$row){
            $row->team_thumbnail = base_url().'upload/images/'.$row->team_thumbnail;
        }
        header('Content-Type: application/json');
        echo json_encode(array(
            'status' => 'success',
            'message' => 'Data Berhasil Di Ambil',
            'data' => $read
        ));
    }
    public function read($offset,$limit){
        $read = $data['regulasi'] = $this
            ->db
            ->select('team.title AS team_title, team.position AS team_position, team.description AS team_description, team.thumbnail AS team_thumbnail, regulasi.*')
            ->join('team', 'team.id = regulasi.id_team', 'left')
            ->where('regulasi.use', 'yes')
            ->order_by('regulasi.id', 'DESC')
            ->limit($limit,$offset)
            ->get('regulasi')
            ->result();
        foreach ($read as $row){
            $row->created_at = date("d F Y",strtotime($row->created_at));
        }
        foreach ($read as$row){
            $row->thumbnail = base_url().'upload/regulasi_thumbnail/'.$row->thumbnail;
        }
        foreach ($read as$row){
            $row->team_thumbnail = base_url().'upload/images/'.$row->team_thumbnail;
        }
        header('Content-Type: application/json');
        echo json_encode(array(
            'status' => 'success',
            'message' => 'Data Berhasil Di Ambil',
            'data' => $read
        ));
    }
    public function createview($id){
        $where = array('id' => $id);
        $data = $this->M_regulasi->row_data($where);
        $viewplus = $data->views + 1;
        $viewplus2 = array(
            'views'=>$viewplus,
        );
        $this->M_regulasi->update_data($where,$viewplus2);

        header('Content-Type: application/json');
        echo json_encode(array(
            'status' => 'success',
            'message' => 'Data Berhasil Di Ambil',
            'data' => $data
        ));
    }
    public function search($offset,$limit,$search){
        $read = $data['regulasi'] = $this
            ->db
            ->select('team.title AS team_title, team.position AS team_position, team.description AS team_description, team.thumbnail AS team_thumbnail, regulasi.*')
            ->join('team', 'team.id = regulasi.id_team', 'left')
            ->where('regulasi.use', 'yes')
            ->order_by('regulasi.id', 'DESC')
            ->like('regulasi.title',urldecode($search))
            ->or_like('regulasi.description',urldecode($search))
            ->limit($limit,$offset)
            ->get('regulasi')
            ->result();
        foreach ($read as $row){
            $row->created_at = date("d F Y",strtotime($row->created_at));
        }
        foreach ($read as$row){
            $row->thumbnail = base_url().'upload/regulasi_thumbnail/'.$row->thumbnail;
        }
        foreach ($read as$row){
            $row->team_thumbnail = base_url().'upload/images/'.$row->team_thumbnail;
        }
        header('Content-Type: application/json');
        echo json_encode(array(
            'status' => 'success',
            'message' => 'Data Berhasil Di Ambil',
            'data' => $read
        ));
    }





}
