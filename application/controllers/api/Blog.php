<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class blog extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('M_blog');
    }

    public function recent()
    {
        $read = $data['blog'] = $this
            ->db
            ->select('team.title AS team_title, team.position AS team_position, team.description AS team_description, team.thumbnail AS team_thumbnail, blog.*, blog_category.title AS blog_category_title')
            ->where('blog.use', 'yes')
            ->join('team', 'team.id = blog.id_team', 'left')
            ->join('blog_category', 'blog_category.id = blog.id_blog_category', 'left')
            ->order_by('blog.id', 'DESC')
            ->limit(5, 0)
            ->get('blog')
            ->result();
        foreach ($read as $row) {
            $row->created_at = date("d  F Y", strtotime($row->created_at));
        }
        foreach ($read as $row) {
            $row->team_thumbnail = base_url() . 'upload/images/' . $row->team_thumbnail;
        }
        foreach ($read as $row) {
            $row->thumbnail = base_url() . 'upload/images/' . $row->thumbnail;
        }
        header('Content-Type: application/json');
        echo json_encode(array(
            'status' => 'success',
            'message' => 'Data Berhasil Di Ambil',
            'data' => $read
        ));
    }

    public function read($offset, $limit)
    {
        $read = $data['blog'] = $this
            ->db
            ->select('team.title AS team_title,team.position AS team_position, team.description AS team_description, team.thumbnail AS team_thumbnail, blog.*, blog_category.title AS blog_category_title')
            ->where('blog.use', 'yes')
            ->join('team', 'team.id = blog.id_team', 'left')
            ->join('blog_category', 'blog_category.id = blog.id_blog_category', 'left')
            ->order_by('blog.id', 'DESC')
            ->limit($limit, $offset)
            ->get('blog')
            ->result();
        foreach ($read as $row) {
            $row->created_at = date("d  F Y", strtotime($row->created_at));
        }
        foreach ($read as $row) {
            $row->team_thumbnail = base_url() . 'upload/images/' . $row->team_thumbnail;
        }
        foreach ($read as $row) {
            $row->thumbnail = base_url() . 'upload/images/' . $row->thumbnail;
        }
        header('Content-Type: application/json');
        echo json_encode(array(
            'status' => 'success',
            'message' => 'Data Berhasil Di Ambil',
            'data' => $read
        ));
    }

    public function readCategory($id, $offset, $limit)
    {
        $read = $data['blog'] = $this
            ->db
            ->select('team.title AS team_title,team.position AS team_position, team.description AS team_description, team.thumbnail AS team_thumbnail, blog.*, blog_category.title AS blog_category_title')
            ->where('blog.use', 'yes')
            ->join('team', 'team.id = blog.id_team', 'left')
            ->join('blog_category', 'blog_category.id = blog.id_blog_category', 'left')
            ->order_by('blog.id', 'DESC')
            ->limit($limit, $offset)
            ->get_where('blog', array('id_blog_category' => $id))
//            ->get('blog')
            ->result();

        foreach ($read as $row) {
            $row->created_at = date("d F Y", strtotime($row->created_at));
        }
        foreach ($read as $row) {
            $row->team_thumbnail = base_url() . 'upload/images/' . $row->team_thumbnail;
        }
        foreach ($read as $row) {
            $row->thumbnail = base_url() . 'upload/images/' . $row->thumbnail;
        }
        header('Content-Type: application/json');
        echo json_encode(array(
            'status' => 'success',
            'message' => 'Data Berhasil Di Ambil',
            'data' => $read
        ));
    }

    public function readCategoryRelated($id, $erased)
    {
        $read = $data['blog'] = $this
            ->db
            ->select('team.title AS team_title,team.position AS team_position, team.description AS team_description, team.thumbnail AS team_thumbnail, blog.*, blog_category.title AS blog_category_title')
            ->where('blog.use', 'yes')
            ->where('blog.id !=', $erased)
            ->join('team', 'team.id = blog.id_team', 'left')
            ->join('blog_category', 'blog_category.id = blog.id_blog_category', 'left')
            ->order_by('blog.id', 'DESC')
            ->limit(3, 0)
            ->get_where('blog', array('id_blog_category' => $id))
            ->result();
        foreach ($read as $row) {
            $row->created_at = date("d  F Y", strtotime($row->created_at));
        }
        foreach ($read as $row) {
            $row->team_thumbnail = base_url() . 'upload/images/' . $row->team_thumbnail;
        }
        foreach ($read as $row) {
            $row->thumbnail = base_url() . 'upload/images/' . $row->thumbnail;
        }
        header('Content-Type: application/json');
        echo json_encode(array(
            'status' => 'success',
            'message' => 'Data Berhasil Di Ambil',
            'data' => $read
        ));
    }

    public function createview($id)
    {
        $where = array('id' => $id);
        $data = $this->M_blog->row_data($where);
        $viewplus = $data->views + 1;
        $viewplus2 = array(
            'views' => $viewplus,
        );
        $this->M_blog->update_data($where, $viewplus2);


        header('Content-Type: application/json');
        echo json_encode(array(
            'status' => 'success',
            'message' => 'Data Berhasil Di Ambil',
            'data' => $data
        ));
    }

    public function search($offset, $limit, $search)
    {
        $read = $this
            ->db
            ->select('team.title AS team_title, team.position AS team_position, team.description AS team_description, team.thumbnail AS team_thumbnail, blog.*, blog_category.title AS blog_category_title')
            ->where('blog.use', 'yes')
            ->join('team', 'team.id = blog.id_team', 'left')
            ->join('blog_category', 'blog_category.id = blog.id_blog_category', 'left')
            ->like('blog.title', urldecode($search))
            ->or_like('blog.description', urldecode($search))
            ->order_by('blog.id', 'DESC')
            ->limit($limit, $offset)
            ->get('blog')
            ->result();
        foreach ($read as $row) {
            $row->created_at = date("d  F Y", strtotime($row->created_at));
        }
        foreach ($read as $row) {
            $row->team_thumbnail = base_url() . 'upload/images/' . $row->team_thumbnail;
        }
        foreach ($read as $row) {
            $row->thumbnail = base_url() . 'upload/images/' . $row->thumbnail;
        }

        header('Content-Type: application/json');
        echo json_encode(array(
            'status' => 'success',
            'message' => 'Data Berhasil Di Ambil',
            'data' => $read
        ));
    }
}
