<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class ebook extends CI_Controller{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('M_ebook');
    }
    public function recent(){
        $read = $data['ebook'] = $this
            ->db
            ->order_by('ebook.id', 'DESC')
            ->limit(5,0)
            ->get('ebook')
            ->result();
        foreach ($read as $row){
            $row->thumbnail = base_url().'upload/ebook_thumbnail/'.$row->thumbnail;
        }
        foreach ($read as $row){
            $row->attachment = base_url().'upload/ebook/'.$row->attachment;
        }
        foreach ($read as $row){
            $row->created_at = date("d  F Y",strtotime($row->created_at));
        }
        header('Content-Type: application/json');
        echo json_encode(array(
            'status' => 'success',
            'message' => 'Data Berhasil Di Ambil',
            'data' => $read
        ));
    }
    public function read($offset,$limit){
        $read = $data['ebook'] = $this
            ->db
            ->order_by('ebook.id', 'DESC')
            ->limit($limit,$offset)
            ->get('ebook')
            ->result();
        foreach ($read as $row){
            $row->thumbnail = base_url().'upload/ebook_thumbnail/'.$row->thumbnail;
        }
        foreach ($read as $row){
            $row->attachment = base_url().'upload/ebook/'.$row->attachment;
        }
        foreach ($read as $row){
            $row->created_at = date("d  F Y",strtotime($row->created_at));
        }
        header('Content-Type: application/json');
        echo json_encode(array(
            'status' => 'success',
            'message' => 'Data Berhasil Di Ambil',
            'data' => $read
        ));
    }
    public function createview($id){
        $where = array('id' => $id);
        $data = $this->M_ebook->row_data($where);
        $viewplus = $data->views + 1;
        $viewplus2 = array(
            'views'=>$viewplus,
        );
        $this->M_ebook->update_data($where,$viewplus2);

        header('Content-Type: application/json');
        echo json_encode(array(
            'status' => 'success',
            'message' => 'Data Berhasil Di Ambil',
            'data' => $data
        ));
    }
    public function search($offset,$limit,$search){
        $read = $data['ebook'] = $this
            ->db
            ->order_by('ebook.id', 'DESC')
            ->like('ebook.title',urldecode($search))
            ->or_like('ebook.description',urldecode($search))
            ->limit($limit,$offset)
            ->get('ebook')
            ->result();
        foreach ($read as $row){
            $row->thumbnail = base_url().'upload/ebook_thumbnail/'.$row->thumbnail;
        }
        foreach ($read as $row){
            $row->attachment = base_url().'upload/ebook/'.$row->attachment;
        }
        foreach ($read as $row){
            $row->created_at = date("d  F Y",strtotime($row->created_at));
        }
        header('Content-Type: application/json');
        echo json_encode(array(
            'status' => 'success',
            'message' => 'Data Berhasil Di Ambil',
            'data' => $read
        ));


    }
}
