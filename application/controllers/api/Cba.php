<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cba extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model(array(
            'M_hazard',
            'M_hazard_params_data',
            'M_hazard_params_description',
            'M_hazard_params_staff',
            'M_staff',
            'M_staff_job',
            'M_staff_risks',
            'M_staff_tasks'
        ));
        $this->load->library(array('Main', 'form_validation'));
    }

    public function list()
    {
        $id_user = $this->main->token();
        $this->form_validation->set_rules('offset', 'Offset', 'required');
        $this->form_validation->set_rules('limit', 'Limit', 'required');
        $this->form_validation->set_error_delimiters('', '');
        if ($this->form_validation->run() == FALSE) {
            $response_data = array(
                'offset' => form_error('offset'),
                'limit' => form_error('limit'),
            );
            $this->main->response('error', 'post', $response_data);
        } else {
            $limit = $this->input->post('limit');
            $offset = $this->input->post('offset');
            $data = $this
                ->db
                ->where(array(
                    'id_user' => $id_user,
                    'c.deleted_at' => null
                ))
                ->limit($limit, $offset)
                ->order_by('id_cba', 'DESC')
                ->get('cba c')
                ->result();

            foreach ($data as $row) {
                $row->datetime = date('d F Y, H:i', strtotime($row->created_at));
                unset($row->created_at);
            }

            $this->main->response('success', 'post', $data);
        }
    }

    public function create()
    {
        $id_user = $this->main->token();
        $date_time = date('Y-m-d H:i:s');
        $data_insert = array(
            'id_user' => $id_user,
            'cba_step_at' => 1,
            'cba_start_time' => $date_time,
            'created_at' => $date_time
        );

        $this->db->insert('cba', $data_insert);
        $id_cba = $this->db->insert_id();

        $response_data = array(
            'id_cba' => $id_cba
        );

        $this->main->response('success', 'post', $response_data);
    }

    public function delete()
    {
        $this->main->token();
        $this->form_validation->set_rules('id_cba', 'ID Hazard', 'required|callback_check_id_cba');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == FALSE) {
            $response_data = array(
                'id_cba' => form_error('id_cba'),
            );
            $this->main->response('error', 'post', $response_data);
        } else {
            $id_cba = $this->input->post('id_cba');
            $this->db->where('id_cba', $id_cba)->update('cba', array('deleted_at' => date('Y-m-d H:i:s')));

            $this->main->response('success', 'post');
        }
    }

    public function step_check()
    {
        $this->main->token();
        $this->form_validation->set_rules('id_cba', 'ID CBA', 'required|callback_check_id_cba');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == FALSE) {
            $response_data = array(
                'id_cba' => form_error('id_cba'),
            );
            $this->main->response('error', 'post', $response_data);
        } else {
            $id_cba = $this->input->post('id_cba');
            $cba = $this->db->where('id_cba', $id_cba)->get('cba')->row();

            $response_data = array(
                'step' => $cba->step_at
            );

            $this->main->response('success', 'post', $response_data);
        }
    }

    public function step_1_data()
    {
        $this->main->token();
        $this->form_validation->set_rules('id_cba', 'ID CBA', 'required|callback_check_id_cba');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == FALSE) {
            $response_data = array(
                'id_cba' => form_error('id_cba'),
            );
            $this->main->response('error', 'post', $response_data);
        } else {
            $id_cba = $this->input->post('id_cba');
            $response_data = $this->db->where('id_cba', $id_cba)->get('cba_identity')->row();

            $this->main->response('success', 'post', $response_data);
        }
    }

    public function step_1_send()
    {
        $this->main->token();
        $this->form_validation->set_rules('id_cba', 'ID CBA', 'required|callback_check_id_cba');
        $this->form_validation->set_rules('jenis_resiko', 'Jenis Resiko', 'required');
        $this->form_validation->set_rules('deskripsi_resiko', 'Deskripsi Resiko', 'required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == FALSE) {
            $response_data = array(
                array(
                    'id_cba' => form_error('id_cba'),
                ),
                array(
                    'jenis_resiko' => form_error('jenis_resiko'),
                ),
                array(
                    'deskripsi_resiko' => form_error('deskripsi_resiko'),
                )
            );
            $this->main->response('error', 'post', $response_data);
        } else {
            $id_cba = $this->input->post('id_cba');
            $cbi_jenis_resiko = $this->input->post('jenis_resiko');
            $cbi_deskripsi_resiko = $this->input->post('deskripsi_resiko');

            $check_cba = $this->db->where('id_cba', $id_cba)->get('cba_identity')->num_rows();

            if ($check_cba == 0) {
                $data_insert = array(
                    'id_cba' => $id_cba,
                    'cbi_jenis_resiko' => $cbi_jenis_resiko,
                    'cbi_deskripsi_resiko' => $cbi_deskripsi_resiko,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                );

                $this->db->insert('cba_identity', $data_insert);
            } else {
                $data_update = array(
                    'id_cba' => $id_cba,
                    'cbi_jenis_resiko' => $cbi_jenis_resiko,
                    'cbi_deskripsi_resiko' => $cbi_deskripsi_resiko,
                    'updated_at' => date('Y-m-d H:i:s'),
                );

                $this->db->where('id_cba', $id_cba)->update('cba_identity', $data_update);
            }

            $this->db->where('id_cba', $id_cba)->update('cba', array('cba_step_at' => 2, 'updated_at' => date('Y-m-d H:i:s')));

            $response_data = array();

            $this->main->response('success', 'post', $response_data);
        }
    }

    public function step_2_data()
    {
        $this->main->token();
        $this->form_validation->set_rules('id_cba', 'ID CBA', 'required|callback_check_id_cba');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == FALSE) {
            $response_data = array(
                'id_cba' => form_error('id_cba'),
            );
            $this->main->response('error', 'post', $response_data);
        } else {
            $id_cba = $this->input->post('id_cba');
            $response_data = $this->db->where('id_cba', $id_cba)->get('cba_baseline')->row();
            $cba_baseline_detail = $this->db->where('id_cba_baseline', $response_data->id_cba_baseline)->get('cba_baseline_detail')->result();
            $response_data->detail = $cba_baseline_detail;

            $this->main->response('success', 'post', $response_data);
        }
    }

    public function step_2_send()
    {
        $this->main->token();
        $this->form_validation->set_rules('id_cba', 'ID CBA', 'required|callback_check_id_cba');
        $this->form_validation->set_rules('likelihood', 'Likelihood', 'required');
        $this->form_validation->set_rules('impact', 'Impact', 'required');
        $this->form_validation->set_rules('total_cost', 'Total Cost', 'required');
        $this->form_validation->set_rules('total_baseline_cost', 'Total Baseline Cost', 'required');

        $this->form_validation->set_rules('keterangan[]', "Keterangan", "required");

        $this->form_validation->set_rules('nominal[]', "Nominal", "required");

        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == FALSE) {
            $response_data = array(
                array(
                    'id_cba' => form_error('id_cba'),
                ),
                array(
                    'likelihood' => form_error('likelihood'),
                ),
                array(
                    'impact' => form_error('impact'),
                ),
                array(
                    'total_cost' => form_error('total_cost'),
                ),
                array(
                    'total_baseline_cost' => form_error('total_baseline_cost'),
                ),
                array(
                    'keterangan[]' => form_error('keterangan'),
                ),
                array(
                    'nominal[]' => form_error('nominal'),
                )
            );
            $this->main->response('error', 'post', $response_data);
        } else {
            $id_cba = $this->input->post('id_cba');
            $cbb_likelihood = $this->input->post('likelihood');
            $cbb_impact = $this->input->post('impact');
            $cbb_total_cost = $this->input->post('total_cost');
            $cbb_total_baseline_cost = $this->input->post('total_baseline_cost');

            $cbd_keterangan_arr = $this->input->post('keterangan');
            $cbd_nominal_arr = $this->input->post('nominal');

            $check = $this->db->where('id_cba', $id_cba)->get('cba_baseline')->num_rows();

            if ($check == 0) {
                $data_insert = array(
                    'id_cba' => $id_cba,
                    'cbb_likelihood' => $cbb_likelihood,
                    'cbb_impact' => $cbb_impact,
                    'cbb_total_cost' => $cbb_total_cost,
                    'cbb_total_baseline_cost' => $cbb_total_baseline_cost,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );
                $this->db->insert('cba_baseline', $data_insert);
                $id_cba_baseline = $this->db->insert_id();

                $cba_baseline_data = array();
                foreach ($cbd_keterangan_arr as $key => $cbd_keterangan) {
                    $cba_baseline_data[] = array(
                        'id_cba_baseline' => $id_cba_baseline,
                        'cbd_keterangan' => $cbd_keterangan,
                        'cbd_nominal' => $cbd_nominal_arr[$key],
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    );
                }

                $this->db->insert_batch('cba_baseline_detail', $cba_baseline_data);

            } else {
                $id_cba_baseline = $this->db->where('id_cba', $id_cba)->get('cba_baseline')->row()->id_cba_baseline;

                $data_update = array(
                    'cbb_likelihood' => $cbb_likelihood,
                    'cbb_impact' => $cbb_impact,
                    'cbb_total_cost' => $cbb_total_cost,
                    'cbb_total_baseline_cost' => $cbb_total_baseline_cost,
                    'updated_at' => date('Y-m-d H:i:s')
                );

                $this->db->where('id_cba', $id_cba)->update('cba_baseline', $data_update);
                $this->db->where('id_cba_baseline', $id_cba_baseline)->delete('cba_baseline_detail');


                $cba_baseline_data = array();
                foreach ($cbd_keterangan_arr as $key => $cbd_keterangan) {
                    $cba_baseline_data[] = array(
                        'id_cba_baseline' => $id_cba_baseline,
                        'cbd_keterangan' => $cbd_keterangan,
                        'cbd_nominal' => $cbd_nominal_arr[$key],
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    );
                }

                $this->db->insert_batch('cba_baseline_detail', $cba_baseline_data);
            }

            $this->db->where('id_cba', $id_cba)->update('cba', array('cba_step_at' => 3, 'updated_at' => date('Y-m-d H:i:s')));

            $this->main->response('success', 'post', array());
        }
    }

    public function step_3_data()
    {
        $this->main->token();
        $this->form_validation->set_rules('id_cba', 'ID CBA', 'required|callback_check_id_cba');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == FALSE) {
            $response_data = array(
                'id_cba' => form_error('id_cba'),
            );
            $this->main->response('error', 'post', $response_data);
        } else {
            $id_cba = $this->input->post('id_cba');
            $response_data = $this->db->where('id_cba', $id_cba)->get('cba_residual')->row();
            $cba_residual_detail = $this->db->where('id_cba_residual', $response_data->id_cba_residual)->get('cba_residual_detail')->result();
            $response_data->detail = $cba_residual_detail;

            $this->main->response('success', 'post', $response_data);
        }
    }

    public function step_3_send()
    {
        $this->main->token();
        $this->form_validation->set_rules('id_cba', 'ID CBA', 'required|callback_check_id_cba');
        $this->form_validation->set_rules('likelihood', 'Likelihood', 'required');
        $this->form_validation->set_rules('impact', 'Impact', 'required');
        $this->form_validation->set_rules('total_residual_cost', 'Total Residual Cost', 'required');
        $this->form_validation->set_rules('benefit_result', 'Total Benefit Result', 'required');
        $this->form_validation->set_rules('total_implementation_cost', 'Total Implementation Cost', 'required');

        $this->form_validation->set_rules('keterangan[]', "Keterangan", "required");

        $this->form_validation->set_rules('nominal[]', "Nominal", "required");

        $this->form_validation->set_error_delimiters('', '');

//        $this->main->response('success', 'post', $_POST);

        if ($this->form_validation->run() == FALSE) {
            $response_data = array(
                array(
                    'id_cba' => form_error('id_cba'),
                ),
                array(
                    'likelihood' => form_error('likelihood'),
                ),
                array(
                    'impact' => form_error('impact'),
                ),
                array(
                    'total_residual_cost' => form_error('total_residual_cost'),
                ),
                array(
                    'benefit_result' => form_error('benefit_result'),
                ),
                array(
                    'total_implementation_cost' => form_error('total_implementation_cost'),
                ),
                array(
                    'keterangan[]' => form_error('keterangan'),
                ),
                array(
                    'nominal[]' => form_error('nominal'),
                )
            );
            $this->main->response('error', 'post', $response_data);
        } else {
            $id_cba = $this->input->post('id_cba');
            $cbr_likelihood = $this->input->post('likelihood');
            $cbr_impact = $this->input->post('impact');
            $cbr_total_residual_cost = $this->input->post('total_residual_cost');
            $cbr_benefit_result = $this->input->post('benefit_result');
            $cbr_total_implementation_cost = $this->input->post('total_implementation_cost');

            $crd_keterangan_arr = $this->input->post('keterangan');
            $crd_nominal_arr = $this->input->post('nominal');

            $check = $this->db->where('id_cba', $id_cba)->get('cba_residual')->num_rows();

            if ($check == 0) {
                $data_insert = array(
                    'id_cba' => $id_cba,
                    'cbr_likelihood' => $cbr_likelihood,
                    'cbr_impact' => $cbr_impact,
                    'cbr_total_residual_cost' => $cbr_total_residual_cost,
                    'cbr_benefit_result' => $cbr_benefit_result,
                    'cbr_total_implementation_cost' => $cbr_total_implementation_cost,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );
                $this->db->insert('cba_residual', $data_insert);
                $id_cba_residual = $this->db->insert_id();

                $cba_residual_data = array();
                foreach ($crd_keterangan_arr as $key => $crd_keterangan) {
                    $cba_residual_data[] = array(
                        'id_cba_residual' => $id_cba_residual,
                        'crd_keterangan' => $crd_keterangan,
                        'crd_nominal' => $crd_nominal_arr[$key],
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    );
                }

                $this->db->insert_batch('cba_residual_detail', $cba_residual_data);

            } else {
                $id_cba_residual = $this->db->where('id_cba', $id_cba)->get('cba_residual')->row()->id_cba_residual;

                $data_update = array(
                    'cbr_likelihood' => $cbr_likelihood,
                    'cbr_impact' => $cbr_impact,
                    'cbr_total_residual_cost' => $cbr_total_residual_cost,
                    'cbr_benefit_result' => $cbr_benefit_result,
                    'cbr_total_implementation_cost' => $cbr_total_implementation_cost,
                    'updated_at' => date('Y-m-d H:i:s')
                );

                $this->db->where('id_cba', $id_cba)->update('cba_residual', $data_update);
                $this->db->where('id_cba_residual', $id_cba_residual)->delete('cba_residual_detail');


                $cba_residual_data = array();
                foreach ($crd_keterangan_arr as $key => $crd_keterangan) {
                    $cba_residual_data[] = array(
                        'id_cba_residual' => $id_cba_residual,
                        'crd_keterangan' => $crd_keterangan,
                        'crd_nominal' => $crd_nominal_arr[$key],
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    );
                }

                $this->db->insert_batch('cba_residual_detail', $cba_residual_data);
            }

            $this->db->where('id_cba', $id_cba)->update('cba', array('cba_step_at' => 4, 'updated_at' => date('Y-m-d H:i:s')));

            $this->main->response('success', 'post', array());
        }
    }

    public function step_4_data()
    {
        $this->main->token();
        $this->form_validation->set_rules('id_cba', 'ID Cba', 'required|callback_check_id_cba');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == FALSE) {
            $response_data = array(
                'id_cba' => form_error('id_cba'),
            );
            $this->main->response('error', 'post', $response_data);
        } else {
            $id_cba = $this->input->post('id_cba');

            $cbr_benefit_result = $this->db->where('id_cba', $id_cba)->get('cba_residual')->row()->cbr_benefit_result;
            $cbr_total_implementation_cost = $this->db->where('id_cba', $id_cba)->get('cba_residual')->row()->cbr_total_implementation_cost;

            $cba_rasio_manfaat_biaya = $cbr_benefit_result / $cbr_total_implementation_cost;
            $cba_status_kelayakan = $cba_rasio_manfaat_biaya < 0.7 ? 'Tidak Layak' : 'Layak';

            $response_data = array(
                'rasio_manfaat_biaya' => $cba_rasio_manfaat_biaya,
                'status_kelayakan' => $cba_status_kelayakan
            );



            $this->main->response('success', 'post', $response_data);
        }
    }

    /*    public function data_step_3()
        {
            $this->main->token();
            $this->form_validation->set_rules('id_hazard', 'ID Hazard', 'required|callback_check_id_hazard');
            $this->form_validation->set_error_delimiters('', '');

            if ($this->form_validation->run() == FALSE) {
                $response_data = array(
                    'id_hazard' => form_error('id_hazard'),
                );
                $this->main->response('error', 'post', $response_data);
            } else {
                $id_hazard = $this->input->post('id_hazard');
                $response_data = $this->db->where('id_hazard', $id_hazard)->get('staff_tasks')->result();

                $this->main->response('error', 'post', $response_data);
            }
        }

        public function data_step_4()
        {
            $this->main->token();
            $this->form_validation->set_rules('id_hazard', 'ID Hazard', 'required|callback_check_id_hazard');
            $this->form_validation->set_error_delimiters('', '');

            if ($this->form_validation->run() == FALSE) {
                $response_data = array(
                    'id_hazard' => form_error('id_hazard'),
                );
                $this->main->response('error', 'post', $response_data);
            } else {
                $id_hazard = $this->input->post('id_hazard');
                $response_data = $this->db->where('id_hazard', $id_hazard)->get('staff_risks')->result();

                $this->main->response('error', 'post', $response_data);
            }
        }*/

    public function check_id_cba($id_cba)
    {
        $id_user = $this->main->token();
        $check = $this->db
            ->where(array(
                'id_user' => $id_user,
                'id_cba' => $id_cba
            ))
            ->get('cba')
            ->num_rows();

        if ($check == 0) {
            $this->form_validation->set_message('check_id_cba', 'ID CBA tidak tersedia');
            return FALSE;
        } else {
            return TRUE;
        }
    }

}
