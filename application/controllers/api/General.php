<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('M_user');
    }

    public function apps_id_generate()
    {

        $this->load->library('main');
        $device_info = $this->input->post('device_info');
        $apps_id = $this->main->generate_random_string(7);

        $data_insert = array(
            'apps_id' => $apps_id,
            'device_info' => $device_info,
            'created_at' => date('Y-m-d H:i:s')
        );
        $this->M_device_apps->input_data($data_insert);

        $response_data = array(
            'apps_id' => $apps_id
        );

        $this->main->response('success', 'get', $response_data);
    }
}
