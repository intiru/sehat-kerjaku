<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hazard extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model(array(
            'M_hazard',
            'M_hazard_params_data',
            'M_hazard_params_description',
            'M_hazard_params_staff',
            'M_staff',
            'M_staff_job',
            'M_staff_risks',
            'M_staff_tasks'
        ));
        $this->load->library(array('Main', 'form_validation'));
    }

    public function list()
    {
        $id_user = $this->main->token();
        $this->form_validation->set_rules('offset', 'Offset', 'required');
        $this->form_validation->set_rules('limit', 'Limit', 'required');
        $this->form_validation->set_error_delimiters('', '');
        if ($this->form_validation->run() == FALSE) {
            $response_data = array(
                'offset' => form_error('offset'),
                'limit' => form_error('limit'),
            );
            $this->main->response('error', 'post', $response_data);
        } else {
            $limit = $this->input->post('limit');
            $offset = $this->input->post('offset');
            $data = $this
                ->db
                ->select('h.id, staff_name, company_name, position, variable_title, step_at, h.created_at')
                ->join('staff', 'staff.id_hazard = h.id', 'left')
                ->join('hazard_params_description hpdes', 'hpdes.id = h.id_hazard_params_description', 'left')
                ->where(array(
                    'id_user' => $id_user,
                    'h.deleted_at' => null
                ))
                ->limit($limit, $offset)
                ->order_by('id', 'DESC')
                ->get('hazard h')
                ->result();

            foreach ($data as $row) {
                $row->datetime = date('d F Y, H:i', strtotime($row->created_at));
                $row->id_hazard = $row->id;
                unset($row->created_at);
                unset($row->id);
            }

            $this->main->response('success', 'post', $data);
        }
    }

    public function create()
    {
        $id_user = $this->main->token();
        $date_time = date('Y-m-d H:i:s');
        $data_insert = array(
            'id_user' => $id_user,
            'step_at' => 1,
            'start_time' => $date_time,
            'created_at' => $date_time
        );

        $this->M_hazard->input_data($data_insert);
        $id_hazard = $this->db->insert_id();

        $response_data = array(
            'id_hazard' => $id_hazard
        );

        $this->main->response('success', 'post', $response_data);
    }

    public function delete()
    {
        $this->main->token();
        $this->form_validation->set_rules('id_hazard', 'ID Hazard', 'required|callback_check_id_hazard');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == FALSE) {
            $response_data = array(
                'id_hazard' => form_error('id_hazard'),
            );
            $this->main->response('error', 'post', $response_data);
        } else {
            $id_hazard = $this->input->post('id_hazard');
            $this->db->where('id', $id_hazard)->update('hazard', array('deleted_at' => date('Y-m-d H:i:s')));

            $this->main->response('success', 'post');
        }
    }

    public function step_check()
    {
        $this->main->token();
        $this->form_validation->set_rules('id_hazard', 'ID Hazard', 'required|callback_check_id_hazard');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == FALSE) {
            $response_data = array(
                'id_hazard' => form_error('id_hazard'),
            );
            $this->main->response('error', 'post', $response_data);
        } else {
            $id_hazard = $this->input->post('id_hazard');
            $hazard = $this->db->where('id', $id_hazard)->get('hazard')->row();

            $response_data = array(
                'step' => $hazard->step_at
            );

            $this->main->response('success', 'post', $response_data);
        }
    }

    public function step_1_data()
    {
        $this->main->token();
        $this->form_validation->set_rules('id_hazard', 'ID Hazard', 'required|callback_check_id_hazard');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == FALSE) {
            $response_data = array(
                'id_hazard' => form_error('id_hazard'),
            );
            $this->main->response('error', 'post', $response_data);
        } else {
            $id_hazard = $this->input->post('id_hazard');
            $response_data = $this->db->where('id_hazard', $id_hazard)->get('staff')->row();

            $this->main->response('success', 'post', $response_data);
        }
    }

    public function step_1_send()
    {
        $this->main->token();
        $this->form_validation->set_rules('id_hazard', 'ID Hazard', 'required|callback_check_id_hazard');
        $this->form_validation->set_rules('staff_name', 'Nama Staff', 'required');
        $this->form_validation->set_rules('company_name', 'Nama Perusahaan', 'required');
        $this->form_validation->set_rules('position', 'Posisi di Perusahaan', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email');
        $this->form_validation->set_rules('phone', 'Telepon', 'required');
        $this->form_validation->set_rules('address', 'Alamat', 'required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == FALSE) {
            $response_data = array(
                array(
                    'id_hazard' => form_error('id_hazard'),
                ),
                array(
                    'staff_name' => form_error('staff_name'),
                ),
                array(
                    'company_name' => form_error('company_name'),
                ),
                array(
                    'position' => form_error('position'),
                ),
                array(
                    'email' => form_error('email'),
                ),
                array(
                    'phone' => form_error('phone'),
                ),
                array(
                    'address' => form_error('address'),
                ),
            );
            $this->main->response('error', 'post', $response_data);
        } else {
            $id_hazard = $this->input->post('id_hazard');
            $staff_name = $this->input->post('staff_name');
            $company_name = $this->input->post('company_name');
            $position = $this->input->post('position');
            $email = $this->input->post('email');
            $phone = $this->input->post('phone');
            $address = $this->input->post('address');

            $check_staff = $this->db->where('id_hazard', $id_hazard)->get('staff')->num_rows();

            if ($check_staff == 0) {
                $data_insert = array(
                    'id_hazard' => $id_hazard,
                    'staff_name' => $staff_name,
                    'company_name' => $company_name,
                    'position' => $position,
                    'email' => $email,
                    'phone' => $phone,
                    'address' => $address,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                );

                $this->db->insert('staff', $data_insert);
            } else {
                $data_update = array(
                    'staff_name' => $staff_name,
                    'company_name' => $company_name,
                    'position' => $position,
                    'email' => $email,
                    'phone' => $phone,
                    'address' => $address,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                );

                $this->db->where('id_hazard', $id_hazard)->update('staff', $data_update);
            }

            $this->db->where('id', $id_hazard)->update('hazard', array('step_at' => 2, 'updated_at' => date('Y-m-d H:i:s')));

            $response_data = array();

            $this->main->response('success', 'post', $response_data);
        }
    }

    public function step_2_data()
    {
        $this->main->token();
        $this->form_validation->set_rules('id_hazard', 'ID Hazard', 'required|callback_check_id_hazard');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == FALSE) {
            $response_data = array(
                'id_hazard' => form_error('id_hazard'),
            );
            $this->main->response('error', 'post', $response_data);
        } else {
            $id_hazard = $this->input->post('id_hazard');
            $response_data = $this->db->where('id_hazard', $id_hazard)->get('staff_job')->row();

            $this->main->response('success', 'post', $response_data);
        }
    }

    public function step_2_send()
    {
        $this->main->token();
        $this->form_validation->set_rules('id_hazard', 'ID Hazard', 'required|callback_check_id_hazard');
        $this->form_validation->set_rules('job_activity_description', 'Deskripsi Aktivitas Pekerjaan', 'required');
        $this->form_validation->set_rules('job_location_description', 'Deskripsi Lokasi Pekerjaan', 'required');
        $this->form_validation->set_rules('job_risk_people', 'Pekerja yang Berisiko Dalam Pekerjaan', 'required');
        $this->form_validation->set_rules('job_tasks_description', 'Deskripsi Tugas saat Bekerja', 'required');
        $this->form_validation->set_rules('job_risks_description', 'Deskripsi Resiko saat Bekerja', 'required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == FALSE) {
            $response_data = array(
                array(
                    'id_hazard' => form_error('id_hazard'),
                ),
                array(
                    'job_activity_description' => form_error('job_activity_description'),
                ),
                array(
                    'job_location_description' => form_error('job_location_description'),
                ),
                array(
                    'job_risk_people' => form_error('job_risk_people'),
                ),
                array(
                    'job_tasks_description' => form_error('job_tasks_description'),
                ),
                array(
                    'job_risks_description' => form_error('job_risks_description'),
                )
            );
            $this->main->response('error', 'post', $response_data);
        } else {
            $id_hazard = $this->input->post('id_hazard');
            $job_activity_description = $this->input->post('job_activity_description');
            $job_location_description = $this->input->post('job_location_description');
            $job_risk_people = $this->input->post('job_risk_people');
            $job_tasks_description = $this->input->post('job_tasks_description');
            $job_risks_description = $this->input->post('job_risks_description');

            $check = $this->db->where('id_hazard', $id_hazard)->get('staff_job')->num_rows();

            if ($check == 0) {
                $data_insert = array(
                    'id_hazard' => $id_hazard,
                    'job_activity_description' => $job_activity_description,
                    'job_location_description' => $job_location_description,
                    'job_risk_people' => $job_risk_people,
                    'job_tasks_description' => $job_tasks_description,
                    'job_risks_description' => $job_risks_description,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );

                $this->db->insert('staff_job', $data_insert);
            } else {
                $data_update = array(
                    'job_activity_description' => $job_activity_description,
                    'job_location_description' => $job_location_description,
                    'job_risk_people' => $job_risk_people,
                    'job_tasks_description' => $job_tasks_description,
                    'job_risks_description' => $job_risks_description,
                    'updated_at' => date('Y-m-d H:i:s')
                );

                $this->db->where('id_hazard', $id_hazard)->update('staff_job', $data_update);
            }

            $this->db->where('id', $id_hazard)->update('hazard', array('step_at' => 3, 'updated_at' => date('Y-m-d H:i:s')));

            $this->main->response('success', 'post', array());
        }
    }

    public function step_3_data()
    {
        $this->main->token();
        $this->form_validation->set_rules('id_hazard', 'ID Hazard', 'required|callback_check_id_hazard');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == FALSE) {
            $response_data = array(
                'id_hazard' => form_error('id_hazard'),
            );
            $this->main->response('error', 'post', $response_data);
        } else {
            $id_hazard = $this->input->post('id_hazard');
            $response_data = $this->db->where('id_hazard', $id_hazard)->get('hazard_params_staff')->row();

            $this->main->response('success', 'post', $response_data);
        }
    }

    public function step_3_consequences()
    {
        $this->main->token();
        $response_data = array(
            array(
                'value' => '1',
                'title' => 'Tidak Signifikan',
                'description' => 'Cedera yang tidak membutuhkan pertolongan pertama'
            ),
            array(
                'value' => '2',
                'title' => 'Kecil',
                'description' => 'Pertolongan pertama diperlukan'
            ),
            array(
                'value' => '3',
                'title' => 'Sedang',
                'description' => 'Perawatan medis diperlukan'
            ),
            array(
                'value' => '4',
                'title' => 'Mayor',
                'description' => 'Harus masuk rumah sakit'
            ),
            array(
                'value' => '5',
                'title' => 'Parah',
                'description' => 'Kematian atau cacat permanen pada satu atau lebih orang'
            ),
        );

        $this->main->response('error', 'post', $response_data);
    }

    public function step_3_possibility()
    {
        $this->main->token();
        $response_data = array(
            array(
                'value' => 'A',
                'title' => 'Hampir Pasti',
                'description' => 'Diharapkan terjadi dalam banyak keadaan orang'
            ),
            array(
                'value' => 'B',
                'title' => 'Likely',
                'description' => 'Mungkin akan terjadi dalam banyak situasi'
            ),
            array(
                'value' => 'C',
                'title' => 'Possible',
                'description' => 'Dapat terjadi pada suatu waktu'
            ),
            array(
                'value' => 'D',
                'title' => 'Tidak Mungkin',
                'description' => 'Tidak mungkin terjadi dalam keadaan normal'
            ),
            array(
                'value' => 'E',
                'title' => 'Jarang',
                'description' => 'Dapat terjadi hanya dalam keadaan luar biasa'
            ),
        );

        $this->main->response('error', 'post', $response_data);
    }

    public function step_3_send()
    {
        $this->main->token();
        $this->form_validation->set_rules('id_hazard', 'ID Hazard', 'required|callback_check_id_hazard');
        $this->form_validation->set_rules('consequences', 'Pertimbangan Konsekuensi', 'required');
        $this->form_validation->set_rules('possibility', 'Pertimbangan Kemungkinan', 'required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == FALSE) {
            $response_data = array(
                array(
                    'id_hazard' => form_error('id_hazard'),
                ),
                array(
                    'consequences' => form_error('consequences'),
                ),
                array(
                    'possibility' => form_error('possibility'),
                )
            );
            $this->main->response('error', 'post', $response_data);
        } else {
            $id_hazard = $this->input->post('id_hazard');
            $consequences = $this->input->post('consequences');
            $possibility = $this->input->post('possibility');

            $check = $this->db->where(array('id_hazard' => $id_hazard))->get('hazard_params_staff')->num_rows();

            if ($check == 0) {
                $data_insert = array(
                    'id_hazard' => $id_hazard,
                    'consequences' => $consequences,
                    'possibility' => $possibility,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                );

                $this->db->insert('hazard_params_staff', $data_insert);
            } else {
                $data_update = array(
                    'consequences' => $consequences,
                    'possibility' => $possibility,
                    'updated_at' => date('Y-m-d H:i:s')
                );

                $this->db->where('id_hazard', $id_hazard)->update('hazard_params_staff', $data_update);
            }

            $hazard_params_data = $this
                ->db
                ->where(array(
                    'consequences' => $consequences,
                    'possibility' => $possibility
                ))
                ->get('hazard_params_data')
                ->row();

            $id_hazard_params_description = $this->db->where(array('variable_value' => $hazard_params_data->value))->get('hazard_params_description')->row()->id;

            $data_update = array(
                'id_hazard_params_description' => $id_hazard_params_description,
                'id_hazard_params_data' => $hazard_params_data->id,
                'step_at' => 4,
                'finish_time' => date('Y-m:d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );

            $this->db->where('id', $id_hazard)->update('hazard', $data_update);

            $this->main->response('success', 'post');
        }
    }

    public function step_4_data()
    {
        $this->main->token();
        $this->form_validation->set_rules('id_hazard', 'ID Hazard', 'required|callback_check_id_hazard');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == FALSE) {
            $response_data = array(
                'id_hazard' => form_error('id_hazard'),
            );
            $this->main->response('error', 'post', $response_data);
        } else {
            $id_hazard = $this->input->post('id_hazard');
            $params = $this->db->where('id_hazard', $id_hazard)->get('hazard_params_staff')->row();
            $value = $this
                ->db
                ->where(array(
                    'consequences' => $params->consequences,
                    'possibility' => $params->possibility
                ))
                ->get('hazard_params_data')
                ->row()
                ->value;
            $params_description = $this->db->where(array('variable_value' => $value))->get('hazard_params_description')->row();
            $params_description->detail = $this->db->where('id_hazard_params_description', $params_description->id)->order_by('id', 'ASC')->get('hazard_params_description_data')->result();

            foreach ($params_description->detail as $key => $row) {
                $row->number = $key + 1;
                $row->sub = $row->description_detail;

                unset($row->id);
                unset($row->id_hazard_params_description);
                unset($row->description_detail);
            }

            $response_data = $params_description;

            $this->main->response('success', 'post', $response_data);
        }
    }

    /*    public function data_step_3()
        {
            $this->main->token();
            $this->form_validation->set_rules('id_hazard', 'ID Hazard', 'required|callback_check_id_hazard');
            $this->form_validation->set_error_delimiters('', '');

            if ($this->form_validation->run() == FALSE) {
                $response_data = array(
                    'id_hazard' => form_error('id_hazard'),
                );
                $this->main->response('error', 'post', $response_data);
            } else {
                $id_hazard = $this->input->post('id_hazard');
                $response_data = $this->db->where('id_hazard', $id_hazard)->get('staff_tasks')->result();

                $this->main->response('error', 'post', $response_data);
            }
        }

        public function data_step_4()
        {
            $this->main->token();
            $this->form_validation->set_rules('id_hazard', 'ID Hazard', 'required|callback_check_id_hazard');
            $this->form_validation->set_error_delimiters('', '');

            if ($this->form_validation->run() == FALSE) {
                $response_data = array(
                    'id_hazard' => form_error('id_hazard'),
                );
                $this->main->response('error', 'post', $response_data);
            } else {
                $id_hazard = $this->input->post('id_hazard');
                $response_data = $this->db->where('id_hazard', $id_hazard)->get('staff_risks')->result();

                $this->main->response('error', 'post', $response_data);
            }
        }*/

    public function check_id_hazard($id_hazard)
    {
        $id_user = $this->main->token();
        $check = $this->db
            ->where(array(
                'id_user' => $id_user,
                'id' => $id_hazard
            ))
            ->get('hazard')
            ->num_rows();

        if ($check == 0) {
            $this->form_validation->set_message('check_id_hazard', 'ID Hazard tidak tersedia');
            return FALSE;
        } else {
            return TRUE;
        }
    }

}
