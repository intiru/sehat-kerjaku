<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class blog_category extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('M_blog_category');
    }
    public function read(){
        $read = array();
        array_push($read,
            array(
                'id' => '0',
                'title' => 'Semua',
                'thumbnail' => 'tips-praktisi2.png',
                'thumbnail_alt' => 'Semua',
                'use' => 'yes',
                'meta_keywords' => 'Semua',
                'meta_title' => 'Semua',
                'meta_description' => 'Semua',
            )
        );

//        $value = $this->M_blog_category->get_data() ->result();
//        $table = array_merge($read,$value);
//
//        foreach ($table as $row){
//            $row->thumbnail = base_url().'upload/images/'.$row->thumbnail;
//        }
        $this->db->order_by('title','ASC');
        $value = $this->db->get('blog_category')->result();
        $table = array_merge($read,$value);

        foreach ($table as $row){
            $row->thumbnail = base_url().'upload/images/'.$row->thumbnail;
        }


        header('Content-Type: application/json');

        echo json_encode(array(
            'status' => 'success',
            'message' => 'Data Berhasil Di Ambil',
            'data' => $table
        ));
    }

}
