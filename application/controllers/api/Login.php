<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('M_user');
        $this->load->library(array('form_validation', 'main'));
    }

    public function google()
    {
        $this->form_validation->set_rules('device_info', 'Device Info', 'required');
        $this->form_validation->set_rules('display_name', 'Display Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('id_vendor', 'ID Vendor', 'required');
        $this->form_validation->set_rules('photo_url', 'Photo URL', 'required');
//        $this->form_validation->set_rules('id_token', 'ID Token', 'required');
//        $this->form_validation->set_rules('google_sign_in', 'Google Sign In', 'required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() == FALSE) {
            $response_data = array(
                'device_info' => form_error('device_info'),
                'display_name' => form_error('display_name'),
                'email' => form_error('email'),
                'id_vendor' => form_error('id_vendor'),
                'photo_url' => form_error('photo_url'),
//                'id_token' => form_error('id_token'),
//                'google_sign_in' => form_error('google_sign_in'),
            );
            $this->main->response('error', 'post', $response_data);
        } else {

            $apps_id = $this->main->generate_random_string(7);
            $device_info = $this->input->post('device_info');
            $email = $this->input->post('email');
            $display_name = $this->input->post('display_name');
            $id_vendor = $this->input->post('id_vendor');
            $photo_url = $this->input->post('photo_url');
            $id_token = $this->input->post('id_token');
            $google_sign_in = $this->input->post('google_sign_in');
            $token = $this->main->token_generate();

            $check_email = $this->M_user->count(array('email' => $email));

            if ($check_email == 0) {
                $data_insert = array(
                    'token' => $token,
                    'apps_id' => $apps_id,
                    'device_info' => $device_info,
                    'display_name' => $display_name,
                    'email' => $email,
                    'id_vendor' => $id_vendor,
                    'photo_url' => $photo_url,
                    'id_token' => $id_token,
                    'google_sign_in' => $google_sign_in,
                    'vendor_type' => 'google',
                    'created_at' => date('Y-m-d H:i:s')
                );
                $this->M_user->input_data($data_insert);
                $response_data = array(
                    'token' => $token
                );
            } else {
                $device_info = $this->M_user->row_data(array('email' => $email));
                $token = $device_info->token;

                $response_data = array(
                    'token' => $token
                );

                $this->M_user->update_data(array(
                    'email' => $email
                ), array(
                    'updated_at' => date('Y-m-d H:i:s')
                ));

            }

            $this->main->response('success', 'get', $response_data);

        }

    }

    public function apps()
    {
        error_reporting(E_ALL);
        $id_user = $this->main->token_login();

        $user = $this->M_user->row_data(array('id' => $id_user));
        $token = $user->token;

        $response_data = array(
            'token'=> $token,
        );

        $this->main->response('success', 'get', $response_data);
    }
}
