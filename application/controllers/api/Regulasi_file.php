<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class regulasi_file extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model('M_regulasi_file');
    }
    public function read($id){
        $read = $this
            ->db
            ->select('*')
            ->where('regulasi_file.id_regulasi', $id)
            ->get('regulasi_file')
            ->result();

        foreach ($read as$row){
            $row->attachment = base_url().'upload/regulasi_file/'.$row->attachment;
        }
        header('Content-Type: application/json');
        echo json_encode(array(
            'status' => 'success',
            'message' => 'Data Berhasil Di Ambil',
            'data' => $read
        ));
    }
}
