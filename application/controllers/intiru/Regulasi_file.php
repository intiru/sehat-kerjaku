<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Regulasi_file extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model(array('m_regulasi_file'));
        $this->load->library('main');
        $this->main->check_admin();
    }

    public function list($id_regulasi)
    {

        $data = $this->main->data_main();
        $data['regulasi_file'] = $this->m_regulasi_file->get_data_where('DESC', array('id_regulasi' => $id_regulasi))->result();
        $data['id_regulasi'] = $id_regulasi;
        $data['regulasi'] = $this->db->where('id', $id_regulasi)->get('regulasi')->row();
        $this->template->set('regulasi', 'kt-menu__item--active');
        $this->template->set('breadcrumb', 'Management Regulasi');
        $this->template->load_admin('regulasi_file/index', $data);
    }

    public function createprocess()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_error_delimiters('', '');

        $title = $this->input->post('title');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'Isi form belum benar',
                'errors' => array(
                    'title' => form_error('title'),
                )
            ));
        } else {

            $data = $this->input->post(NULL, TRUE);

            if ($_FILES['attachment']['name']) {
                $response = $this->main->upload_regulasi('attachment', $title);
                if (!$response['status']) {
                    echo json_encode(array(
                        'status' => 'error',
                        'message' => 'Isi form belum benar',
                        'errors' => array(
                            'image' => $response['message']
                        )
                    ));
                    exit;
                } else {
                    $data['attachment'] = $response['filename'];
                }
            }

            $this->m_regulasi_file->input_data($data);

            echo json_encode(array(
                'status' => 'success',
                'message' => 'data berhasil diinput',
            ));
        }
    }

    public function delete($id)
    {

        $where = array('id' => $id);
//		$row = $this->m_gallery->row_data($where);
//		$this->main->delete_file($row->thumbnail);
        $this->m_regulasi_file->delete_data($where);
    }

    public function update()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'Isi form belum benar',
                'errors' => array(
                    'title' => form_error('title')
                )
            ));
        } else {
            $id = $this->input->post('id');
            $data = $this->input->post(NULL, TRUE);
            $title = $this->input->post('title');
            $where = array(
                'id' => $id
            );

            if ($_FILES['attachment']['name']) {
                $response = $this->main->upload_regulasi('attachment', $title);
                if (!$response['status']) {
                    echo json_encode(array(
                        'status' => 'error',
                        'message' => 'Isi form belum benar',
                        'errors' => array(
                            'image' => $response['message']
                        )
                    ));
                    exit;
                } else {
                    $data['attachment'] = $response['filename'];
                }
            }

            $this->m_regulasi_file->update_data($where, $data);
            echo json_encode(array(
                'status' => 'success',
                'message' => 'data berhasil diinputkan'
            ));
        }
    }
}
