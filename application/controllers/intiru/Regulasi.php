<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Regulasi extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->model(array('m_regulasi','m_regulasi_file'));
        $this->load->library('main');
        $this->main->check_admin();
    }

    public function index()
    {

        $data = $this->main->data_main();
        $data['regulasi'] = $this->m_regulasi->get_data()->result();
        $this->template->set('regulasi', 'kt-menu__item--active');
        $this->template->set('breadcrumb', 'Management Regulasi');
        $this->template->load_admin('regulasi/index', $data);
    }

    public function createprocess()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('use', 'Use', 'required');
        $this->form_validation->set_error_delimiters('', '');

        $title = $this->input->post('title');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'Isi form belum benar',
                'errors' => array(
                    'title' => form_error('title'),
                    'description' => form_error('description'),
                    'use' => form_error('use'),
                )
            ));
        } else {

            $data = $this->input->post(NULL, TRUE);
            $data['created_at'] = date('Y-m-d H:i:s');

            if ($_FILES['thumbnail']['name']) {
                $response = $this->main->upload_regulasi_thumbnail('thumbnail', $title);
                if (!$response['status']) {
                    echo json_encode(array(
                        'status' => 'error',
                        'message' => 'Isi form belum benar',
                        'errors' => array(
                            'image' => $response['message']
                        )
                    ));
                    exit;
                } else {
                    $data['thumbnail'] = $response['filename'];
                }
            }

            $this->m_regulasi->input_data($data);

            echo json_encode(array(
                'status' => 'success',
                'message' => 'data berhasil diinput',
            ));
        }
    }

    public function delete($id)
    {

        $where = array('id' => $id);
//		$row = $this->m_gallery->row_data($where);
//		$this->main->delete_file($row->thumbnail);
        $this->m_regulasi->delete_data($where);
    }

    public function update()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('title', 'Title', 'required');
        $this->form_validation->set_rules('description', 'Description', 'required');
        $this->form_validation->set_rules('use', 'Use', 'required');
        $this->form_validation->set_error_delimiters('', '');

        if ($this->form_validation->run() === FALSE) {
            echo json_encode(array(
                'status' => 'error',
                'message' => 'Isi form belum benar',
                'errors' => array(
                    'title' => form_error('title'),
                    'description' => form_error('description'),
                    'use' => form_error('use'),
                )
            ));
        } else {
            $id = $this->input->post('id');
            $data = $this->input->post(NULL, TRUE);
            $title = $this->input->post('title');
            $where = array(
                'id' => $id
            );

            if ($_FILES['thumbnail']['name']) {
                $response = $this->main->upload_regulasi_thumbnail('thumbnail', $title);
                if (!$response['status']) {
                    echo json_encode(array(
                        'status' => 'error',
                        'message' => 'Isi form belum benar',
                        'errors' => array(
                            'image' => $response['message']
                        )
                    ));
                    exit;
                } else {
                    $data['thumbnail'] = $response['filename'];
                }
            }

            $this->m_regulasi->update_data($where, $data);
            echo json_encode(array(
                'status' => 'success',
                'message' => 'data berhasil diinputkan'
            ));
        }
    }
}
