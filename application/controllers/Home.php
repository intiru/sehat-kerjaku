<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function index()
    {
        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'home', 'language_id' => $data['language_id']))->get('pages')->row();
        $data['sesi_1'] = $this->db->where(array('type' => 'home_sesi_1', 'language_id' => $data['language_id']))->get('pages')->row();
        $data['sesi_2'] = $this->db->where(array('type' => 'home_sesi_2', 'language_id' => $data['language_id']))->get('pages')->row();
        $data['sesi_3'] = $this->db->where(array('type' => 'home_sesi_3', 'language_id' => $data['language_id']))->get('pages')->row();
        $services_data = $this->db->where('use', 'yes')->order_by('title', 'ASC')->get('tour')->result();
        $services_left = array();
        $services_right = array();
        foreach($services_data as $key => $row) {
            if($key % 2) {
                $services_left[$key] = $row;
            } else {
                $services_right[$key] = $row;
            }
        }

        $data['services_left'] = $services_left;
        $data['services_right'] = $services_right;


        $data['blog_related'] = $this
            ->db
            ->select('team.title AS team_title, team.description AS team_description, team.thumbnail AS team_thumbnail, blog.*, blog_category.title AS blog_category_title')
            ->join('team', 'team.id = blog.id_team', 'left')
            ->join('blog_category', 'blog_category.id = blog.id_blog_category', 'left')
            ->where('blog.use', 'yes')
            ->order_by('blog.id', 'DESC')
            ->get('blog', 3, 0)
            ->result();

        $this->template->front('home', $data);

    }
}
