<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends CI_Controller
{
    public function __construct()
    {
        parent:: __construct();
        $this->load->library('main');
    }

    public function index()
    {
        $offset = $this->uri->segment(2);
        $keyword = $_GET['search'];

        $data = $this->main->data_front();
        $data['page'] = $this->db->where(array('type' => 'blog', 'language_id' => $data['language_id']))->get('pages')->row();
        $category_data = $this->db->where('use', 'yes')->order_by('title', 'ASC')->get('blog_category')->result();
        $category = array();
        foreach ($category_data as $key => $row) {
            $category[$key] = $row;
            $category[$key]->blog_count = $this->db->where('id_blog_category', $row->id)->get('blog')->num_rows();
        }

        if($keyword) {
            $jumlah_data = $this->db
                ->where('use', 'yes')
                ->like('title', $keyword)
                ->like('description', $keyword)
                ->get('blog')
                ->num_rows();
        } else {
            $jumlah_data = $this->db->where('use', 'yes')->get('blog')->num_rows();
        }
        $this->load->library('pagination');
        $config['base_url'] = site_url('artikel');
        $config['total_rows'] = $jumlah_data;
        $config['per_page'] = 6;

        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span>Next</li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);


        if($keyword) {
            $data['blog'] = $this
                ->db
                ->select('team.title AS team_title, team.description AS team_description, team.thumbnail AS team_thumbnail, blog.*, blog_category.title AS blog_category_title')
                ->join('team', 'team.id = blog.id_team', 'left')
                ->join('blog_category', 'blog_category.id = blog.id_blog_category', 'left')
                ->where('blog.use', 'yes')
                ->like('blog.title', $keyword)
                ->like('blog.description', $keyword)
                ->order_by('blog.id', 'DESC')
                ->get('blog', 6, $offset)
                ->result();
        } else {
            $data['blog'] = $this
                ->db
                ->select('team.title AS team_title, team.description AS team_description, team.thumbnail AS team_thumbnail, blog.*, blog_category.title AS blog_category_title')
                ->join('team', 'team.id = blog.id_team', 'left')
                ->join('blog_category', 'blog_category.id = blog.id_blog_category', 'left')
                ->where('blog.use', 'yes')
                ->order_by('blog.id', 'DESC')
                ->get('blog', 6, $offset)
                ->result();
        }



        $data['category'] = $category;
        $data['id_blog_category'] = 0;

        $this->template->front('artikel', $data);
    }

    public function category($id_blog_category)
    {
        $uri_2 = $this->uri->segment(2);
        $offset = $this->uri->segment(3);

        $data = $this->main->data_front();
        $data['page'] = $this->db->where('id', $id_blog_category)->get('blog_category')->row();
        $category_data = $this->db->where('use', 'yes')->order_by('title', 'ASC')->get('blog_category')->result();
        $category = array();
        foreach ($category_data as $key => $row) {
            $category[$key] = $row;
            $category[$key]->blog_count = $this->db->where('id_blog_category', $row->id)->get('blog')->num_rows();
        }

        $jumlah_data = $this->db->where(array('use'=>'yes', 'id_blog_category'=>$id_blog_category))->get('blog')->num_rows();
        $this->load->library('pagination');
        $config['base_url'] = site_url('artikel/'.$uri_2);
        $config['total_rows'] = $jumlah_data;
        $config['per_page'] = 6;

        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span>Next</li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';

        $this->pagination->initialize($config);


        $data['blog'] = $this
            ->db
            ->select('team.title AS team_title, team.description AS team_description, team.thumbnail AS team_thumbnail, blog.*, blog_category.title AS blog_category_title')
            ->join('team', 'team.id = blog.id_team', 'left')
            ->join('blog_category', 'blog_category.id = blog.id_blog_category', 'left')
            ->where(array('blog.use'=>'yes', 'blog.id_blog_category'=>$id_blog_category))
            ->order_by('blog.id', 'DESC')
            ->get('blog', 6, $offset)
            ->result();
        $data['category'] = $category;
        $data['id_blog_category'] = $id_blog_category;

        $this->template->front('artikel', $data);
    }

    public function detail($id = '')
    {
        $data = $this->main->data_front();
        $views = $this->db->select('views')->where('id', $id)->get('blog')->row()->views;
        $views++;
        $this->db->where('id', $id)->update('blog', array('views'=>$views));
        $data['views'] = $views;
        $data['page'] = $this
            ->db
            ->select('team.title AS team_title, team.position AS team_position, team.thumbnail AS team_thumbnail, blog.*, blog_category.title AS blog_category_title')
            ->join('team', 'team.id = blog.id_team', 'left')
            ->join('blog_category', 'blog_category.id = blog.id_blog_category', 'left')
            ->where('blog.id', $id)
            ->order_by('blog.id', 'DESC')
            ->get('blog')
            ->row();
        $data['page']->type = 'blog';
        $data['category'] = $this->db->where('use', 'yes')->order_by('title', 'ASC')->get('blog_category')->result();
        $data['artikel_permalink'] = $this->main->permalink(array('artikel', $data['page']->title));


        $category_data = $this->db->where('use', 'yes')->order_by('title', 'ASC')->get('blog_category')->result();
        $category = array();
        foreach ($category_data as $key => $row) {
            $category[$key] = $row;
            $category[$key]->blog_count = $this->db->where('id_blog_category', $row->id)->get('blog')->num_rows();
        }

        $data['category'] = $category;
        $data['blog_related'] = $this
            ->db
            ->select('team.title AS team_title, team.description AS team_description, team.thumbnail AS team_thumbnail, blog.*, blog_category.title AS blog_category_title')
            ->join('team', 'team.id = blog.id_team', 'left')
            ->join('blog_category', 'blog_category.id = blog.id_blog_category', 'left')
            ->where('blog.use', 'yes')
            ->where_not_in('blog.id', array($id))
            ->order_by('blog.id', 'DESC')
            ->get('blog', 3, 0)
            ->result();

        $this->template->front('artikel_detail', $data);
    }
}
