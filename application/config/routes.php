<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/


$connection = mysqli_connect('localhost', 'root', '', 'mahendra_sehatkerjaku');


$route['default_controller'] = 'home';


$route['intiru'] = 'intiru/login';
$route['intiru/login'] = 'intiru/login/process';
$route['layanan-kami'] = 'services';
$route['layanan-kami-detil'] = 'services/detail';
$route['artikel'] = 'article';
$route['artikel/(:num)'] = 'article';
//$route['artikel-detil'] = 'article/detail';
//$route['artikel/tips-tetap-aman-dan-selamat-saat-berlibur-ke-pantai'] = 'article/detail/21';
$route['profil-kami'] = 'about_us';
$route['galeri-foto'] = 'gallery_photo';
$route['kontak-kami'] = 'contact_us';

$blog = mysqli_query($connection, "SELECT id, title FROM blog");
while ($data = mysqli_fetch_array($blog)) {
    $route['artikel/' . slug($data['title'])] = "article/detail/" . $data['id'];
}

$blog_category = mysqli_query($connection, "SELECT id, title FROM blog_category");
while ($data = mysqli_fetch_array($blog_category)) {
    $route['artikel/' . slug($data['title'])] = "article/category/".$data['id'];
    $route['artikel/' . slug($data['title']).'/(:num)'] = "article/category/" . $data['id'];
}

$services = mysqli_query($connection, "SELECT id, title FROM tour");
while ($data = mysqli_fetch_array($services)) {
    $route['layanan/' . slug($data['title'])] = "services/detail/".$data['id'];
}

$team = mysqli_query($connection, "SELECT id, title FROM team");
while ($data = mysqli_fetch_array($team)) {
    $route[slug($data['title'])] = "about_us/team/".$data['id'];
}


$route['cba'] = 'cba/intro';

//$language_arr = array();
//$language = mysqli_query($connection, "SELECT code FROM language");
//
//while ($row = mysqli_fetch_array($language)) {
//	$language_arr[] = $row['code'];
//
////	$route['services'] = 'services';
////	$route['services/detail'] = 'services/detail';
////	$route['testimonial'] = 'testimonial';
//	$route[$row['code'] . '/layanan-kami'] = 'services';
//	$route[$row['code'] . '/gallery-photo'] = 'gallery_photo';
//	$route[$row['code'] . '/about-us'] = 'about_us';
//	$route[$row['code'] . '/contact-us'] = 'contact_us';
//	$route[$row['code'] . '/contact-us-send'] = 'contact_us/send';
//	$route[$row['code'] . '/reservation-send'] = 'reservation/send';
//
//	$route[$row['code']] = $route['default_controller'];
//
//}

//$category = mysqli_query($connection, "SELECT id,title FROM category");
//while ($data = mysqli_fetch_array($category)) {
//	foreach($language_arr as $code) {
//		$route[$code. '/' . slug($data['title'])] = "services/page/" . $data['id'];
//	}
//}
//


/**
 * API Beranda
 */
$route['api/v1/blog/recent'] = 'api/blog/recent';
$route['api/v1/blog/read/(:num)/(:num)'] = 'api/blog/read/$1/$2';
$route['api/v1/blog/read/category/(:num)/(:num)/(:num)'] = 'api/blog/readCategory/$1/$2/$3';
$route['api/v1/blog/createview/(:num)'] = 'api/blog/createview/$1';
$route['api/v1/blog/search/(:num)/(:num)/(:any)'] = 'api/Blog/search/$1/$2/$3';;
$route['api/v1/blog/related/category/(:num)/(:num)'] = 'api/blog/readCategoryRelated/$1/$2';

$route['api/v1/blogcategory/read'] = 'api/Blog_category/read';

$route['api/v1/ebook/recent'] = 'api/Ebook/recent';
$route['api/v1/ebook/read/(:num)/(:num)'] = 'api/Ebook/read/$1/$2';
$route['api/v1/ebook/createview/(:num)'] = 'api/Ebook/createview/$1';
$route['api/v1/ebook/search/(:num)/(:num)/(:any)'] = 'api/Ebook/search/$1/$2/$3';

$route['api/v1/regulasi/recent'] = 'api/Regulasi/recent';
$route['api/v1/regulasi/read/(:num)/(:num)'] = 'api/Regulasi/read/$1/$2';
$route['api/v1/regulasi/createview/(:num)'] = 'api/Regulasi/createview/$1';
$route['api/v1/regulasi/search/(:num)/(:num)/(:any)'] = 'api/Regulasi/search/$1/$2/$3';

$route['api/v1/regulasifile/read/(:num)'] = 'api/Regulasi_file/read/$1';

/**
 * Hazard Identification
 */

/**
 * General
 */
$route['api/v1/apps-id'] = 'api/General/apps_id_generate';

/**
 * Login
 */
$route['api/v1/login/google'] = 'api/Login/google';
$route['api/v1/login/apps'] = 'api/Login/apps';

/**
 * Data List Hazard
 */

$route['api/v1/hazard/list'] = 'api/Hazard/list';
$route['api/v1/hazard/create'] = 'api/Hazard/create';
$route['api/v1/hazard/delete'] = 'api/Hazard/delete';
$route['api/v1/hazard/step/check'] = 'api/Hazard/step_check';

/**
 * Step 1 Data Personal Staff
 */

$route['api/v1/hazard/step/1/data'] = 'api/Hazard/step_1_data';
$route['api/v1/hazard/step/1/send'] = 'api/Hazard/step_1_send';

/**
 * Step 2 Tentang Pekerjaan
 */
$route['api/v1/hazard/step/2/data'] = 'api/Hazard/step_2_data';
$route['api/v1/hazard/step/2/send'] = 'api/Hazard/step_2_send';

/**
 * Step 3 Parameter Peringkat K3
 */

$route['api/v1/hazard/step/3/data'] = 'api/Hazard/step_3_data';
$route['api/v1/hazard/step/3/consequences'] = 'api/Hazard/step_3_consequences';
$route['api/v1/hazard/step/3/possibility'] = 'api/Hazard/step_3_possibility';
$route['api/v1/hazard/step/3/send'] = 'api/Hazard/step_3_send';

/**
 * Step 4 Hasil Tingkat Resiko
 */

$route['api/v1/hazard/step/4/data'] = 'api/Hazard/step_4_data';




/**
 * Data List Cba
 */

$route['api/v1/cba/list'] = 'api/Cba/list';
$route['api/v1/cba/create'] = 'api/Cba/create';
$route['api/v1/cba/delete'] = 'api/Cba/delete';
$route['api/v1/cba/step/check'] = 'api/Cba/step_check';

/**
 * Step 1 Data CBA Identity
 */

$route['api/v1/cba/step/1/data'] = 'api/Cba/step_1_data';
$route['api/v1/cba/step/1/send'] = 'api/Cba/step_1_send';

/**
 * Step 2 Data CBA Baseline
 */

$route['api/v1/cba/step/2/data'] = 'api/Cba/step_2_data';
$route['api/v1/cba/step/2/send'] = 'api/Cba/step_2_send';

/**
 * Step 3 Data CBA Residual
 */

$route['api/v1/cba/step/3/data'] = 'api/Cba/step_3_data';
$route['api/v1/cba/step/3/send'] = 'api/Cba/step_3_send';

/**
 * Step 4 Hasil Tingkat Resiko
 */

$route['api/v1/cba/step/4/data'] = 'api/Cba/step_4_data';


///**
// * Step 3 Tasks Pekerjaan
// */
//$route['api/v1/hazard/step/3/data'] = 'api/Hazard/data_step_3';
//
///**
// * Step 4 Resiko Pekerjaan
// */
//
//$route['api/v1/hazard/step/4/data'] = 'api/Hazard/data_step_4';


$route['404_override'] = 'not_found';
$route['translate_uri_dashes'] = FALSE;


function slug($string)
{
    $find = array(' ', '/', '&', '\\', '\'', ',','(',')');
    $replace = array('-', '-', 'and', '-', '-', '-','','');

    $slug = str_replace($find, $replace, strtolower($string));

    return $slug;
}
