<?php

Class Main
{

    private $ci;
    private $web_name = 'Sehat Kerjaku';
    private $file_info = 'Images with resolution 800px x 600px and size 100KB';
    private $file_info_slider = 'Images with resolution 1920px x 900px and size 250KB';
    private $path_images = 'upload/images/';
    private $image_size_preview = 200;
    private $help_thumbnail_alt = 'Penting untuk SEO Gambar';
    private $help_meta = 'Penting untuk SEO Halaman Website';
    private $short_desc_char = 100;

    public function __construct()
    {
        error_reporting(0);
        $this->ci =& get_instance();
    }

    function short_desc($string)
    {
        return substr(strip_tags($string), 0, $this->short_desc_char) . ' ...';
    }

    function web_name()
    {
        return $this->web_name;
    }

    function date_view($date)
    {
        return date('d F Y', strtotime($date));
    }

    function help_thumbnail_alt()
    {
        return $this->help_thumbnail_alt;
    }

    function help_meta()
    {
        return $this->help_meta;
    }

    function file_info()
    {
        return $this->file_info;
    }

    function file_info_slider()
    {
        return $this->file_info_slider;
    }

    function path_images()
    {
        return $this->path_images;
    }

    function image_size_preview()
    {
        return $this->image_size_preview;
    }

    function image_preview_url($filename)
    {
        return base_url($this->path_images . $filename);
    }

    function delete_file($filename)
    {
        if ($filename) {
            if (file_exists(FCPATH . $this->path_images . $filename)) {
//				/unlink($this->path_images . $filename);
            }
        }
    }

    function data_main()
    {
        $language_id = $this->ci->session->userdata('language_id') ?
            $this->ci->session->userdata('language_id') :
            $this->ci->db->select('id')->where('use', 'yes')->order_by('id', 'ASC')->get('language')->row()->id;

        $data = array(
            'web_name' => $this->web_name,
            'menu_list' => $this->menu_list(),
            'name' => $this->ci->session->userdata('name'),
            'language' => $this->ci->db->where('use', 'yes')->get('language')->result(),
            'language_id' => $language_id,
        );

        $tab_language = $this->ci->load->view('admins/components/tab_language', $data, TRUE);
        $data['tab_language'] = $tab_language;

        return $data;
    }

    function data_front()
    {

        $lang_code = 'id'; //$this->ci->lang->lang();
        $lang_active = $this->ci->db->where('code', $lang_code)->get('language')->row();
        $footer = $this->ci->db->select('description')->where('type', 'footer')->get('pages')->row()->description;
//		$service_list = $this->ci->db->select('title,id')->where('language_id', $lang_active->id)->order_by('title', 'ASC')->get('category')->result();
//		$language_list = $this->ci->db->where('use','yes')->order_by('title','ASC')->get('language')->result();

        $data = array(

            'alamat' => 'Raya Kampial Block D Nomer 14, Nusa Dua, Bali, Indonesia',
            'telephone' => '',
            'phone' => '+62 812-3978-5261',
            'whatsapp' => '+62 812-3978-5261',
            'whatsapp_link' => 'https://api.whatsapp.com/send?phone=6285737606529&text=Hello,%20i%20want%20to%20ask%20you%20about%20your%20services.',
            'wechat_id' => '',
            'wechat_link' => '',
            'email_link' => 'mailto:harsabaliholiday@gmail.com',
            'email' => 'harsabaliholiday@gmail.com',
            'facebook_link' => 'https://www.facebook.com/harsa.baliholiday.9',
            'line_link' => 'https://msng.link/ln/harsa.bali.holiday',
            'twitter_link' => '',
            'linkedin_link' => '',
            'instagram_link' => 'https://www.instagram.com/harsa_bali_holiday/',
            'view_secret' => FALSE,
            'author' => 'www.sehatkerjaku.com',
//			'services_list' => $service_list,
//			'language_list' => $language_list,
            'lang_code' => $lang_code,
            'lang_active' => $lang_active,
            'language_id' => $lang_active->id,
            'footer' => $footer
        );

        return $data;
    }

    function check_admin()
    {
        if ($this->ci->session->userdata('status') !== 'login') {
            redirect('login');
        }
    }

    function check_login()
    {
        if ($this->ci->session->userdata('status') == 'login') {
            redirect('intiru/dashboard');
        }
    }

    function permalink($data)
    {

        $slug = '';
        foreach ($data as $r) {
            $slug .= $this->slug($r) . '/';
        }

        return site_url($slug);
    }

    function breadcrumb($data)
    {
        $breadcrumb = '<ul class="breadcrumb">';
        $count = count($data);
        $no = 1;
        foreach ($data as $url => $label) {
            $current = '';
            if ($no == $count) {
                $current = ' class="current"';
            }

            $breadcrumb .= '<li' . $current . '><a href="' . $url . '">' . $label . '</a></li>';
        }

        $breadcrumb .= '</ul>';


        return $breadcrumb;
    }

    function slug($text)
    {

        $find = array(' ', '/', '&', '\\', '\'', ',', '(', ')', '.');
        $replace = array('-', '-', 'and', '-', '-', '-', '', '', '');

        $slug = str_replace($find, $replace, strtolower($text));

        return $slug;
    }

    function date_format_view($date)
    {
        return date('d M Y', strtotime($date));
    }

    function slug_back($slug)
    {
        $slug = trim($slug);
        if (empty($slug)) return '';
        $slug = str_replace('-', ' ', $slug);
        $slug = ucwords($slug);
        return $slug;
    }

    function upload_file_thumbnail($fieldname, $filename)
    {
        $config['upload_path'] = './upload/images/';
        $config['allowed_types'] = '*';
        $config['max_size'] = 10000;
        $config['max_width'] = 80000;
        $config['max_height'] = 60000;
//		$config['overwrite'] = TRUE;
        $config['file_name'] = $this->slug($filename);
        $this->ci->load->library('upload', $config);

        if (!$this->ci->upload->do_upload($fieldname)) {
            return array(
                'status' => FALSE,
                'message' => $this->ci->upload->display_errors()
            );
        } else {
            return array(
                'status' => TRUE,
                'filename' => $data['thumbnail'] = $this->ci->upload->file_name
            );
        }
    }

    function upload_ebook_thumbnail($fieldname, $filename)
    {
        $config['upload_path'] = './upload/ebook_thumbnail/';
        $config['allowed_types'] = '*';
        $config['max_size'] = 10000;
        $config['max_width'] = 80000;
        $config['max_height'] = 60000;
//		$config['overwrite'] = TRUE;
        $config['file_name'] = $this->slug($filename);
        $this->ci->load->library('upload', $config, 'thumbnail_upload');
        $this->ci->thumbnail_upload->initialize($config);

        if (!$this->ci->thumbnail_upload->do_upload($fieldname)) {
            return array(
                'status' => FALSE,
                'message' => $this->ci->thumbnail_upload->display_errors()
            );
        } else {
            return array(
                'status' => TRUE,
                'filename' => $this->ci->thumbnail_upload->file_name
            );
        }
    }

    function upload_ebook($fieldname, $filename)
    {
        $config_2['upload_path'] = './upload/ebook/';
        $config_2['allowed_types'] = '*';
        $config_2['max_size'] = 1000000;
        $config_2['max_width'] = 80000;
        $config_2['max_height'] = 60000;
//		$config['overwrite'] = TRUE;
        $config_2['file_name'] = $this->slug($filename);
        $this->ci->load->library('upload', $config_2, 'ebook_upload');
        $this->ci->ebook_upload->initialize($config_2);

        if (!$this->ci->ebook_upload->do_upload($fieldname)) {
            return array(
                'status' => FALSE,
                'message' => $this->ci->ebook_upload->display_errors()
            );
        } else {
            return array(
                'status' => TRUE,
                'filename' => $this->ci->ebook_upload->file_name
            );
        }
    }

    function upload_regulasi_thumbnail($fieldname, $filename)
    {
        $config['upload_path'] = './upload/regulasi_thumbnail/';
        $config['allowed_types'] = '*';
        $config['max_size'] = 10000;
        $config['max_width'] = 80000;
        $config['max_height'] = 60000;
//		$config['overwrite'] = TRUE;
        $config['file_name'] = $this->slug($filename);
        $this->ci->load->library('upload', $config, 'thumbnail_upload');
        $this->ci->thumbnail_upload->initialize($config);

        if (!$this->ci->thumbnail_upload->do_upload($fieldname)) {
            return array(
                'status' => FALSE,
                'message' => $this->ci->thumbnail_upload->display_errors()
            );
        } else {
            return array(
                'status' => TRUE,
                'filename' => $this->ci->thumbnail_upload->file_name
            );
        }
    }

    function upload_regulasi($fieldname, $filename)
    {
        $config_2['upload_path'] = './upload/regulasi_file/';
        $config_2['allowed_types'] = '*';
        $config_2['max_size'] = 1000000;
        $config_2['max_width'] = 80000;
        $config_2['max_height'] = 60000;
//		$config['overwrite'] = TRUE;
        $config_2['file_name'] = $this->slug($filename);
        $this->ci->load->library('upload', $config_2, 'regulasi_upload');
        $this->ci->regulasi_upload->initialize($config_2);

        if (!$this->ci->regulasi_upload->do_upload($fieldname)) {
            return array(
                'status' => FALSE,
                'message' => $this->ci->regulasi_upload->display_errors()
            );
        } else {
            return array(
                'status' => TRUE,
                'filename' => $this->ci->regulasi_upload->file_name
            );
        }
    }

    function upload_file_slider($fieldname, $filename)
    {
        $config['upload_path'] = './upload/images/';
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = 250;
        $config['max_width'] = 1920;
        $config['max_height'] = 900;
//		$config['overwrite'] = TRUE;
        $config['file_name'] = $this->slug($filename);
        $this->ci->load->library('upload', $config);

        if (!$this->ci->upload->do_upload($fieldname)) {
            return array(
                'status' => FALSE,
                'message' => $this->ci->upload->display_errors()
            );
        } else {
            return array(
                'status' => TRUE,
                'filename' => $data['thumbnail'] = $this->ci->upload->file_name
            );
        }
    }

    function captcha()
    {
        $this->ci->load->helper(array('captcha', 'string'));
        $this->ci->load->library('session');

        $vals = array(
            'img_path' => './upload/images/captcha/',
            'img_url' => base_url() . 'upload/images/captcha',
            'img_width' => '200',
            'img_height' => 35,
            'border' => 0,
            'expiration' => 7200,
            'word' => random_string('numeric', 5)
        );

        // create captcha image
        $cap = create_captcha($vals);

        // store image html code in a variable
        $captcha = $cap['image'];

        // store the captcha word in a session
        //$cap['word'];
        $this->ci->session->set_userdata('captcha_mwz', $cap['word']);

        return $captcha;
    }

    function share_link($socmed_type, $title, $link)
    {
        switch ($socmed_type) {
            case "facebook":
                return "https://www.facebook.com/sharer/sharer.php?u=" . $link;
                break;
            case "twitter":
                return "https://twitter.com/home?status=" . $link;
                break;
            case "googleplus":
                return "https://plus.google.com/share?url=" . $link;
                break;
            case "linkedin":
                return "https://www.linkedin.com/shareArticle?mini=true&url=" . $link . "&title=" . $title . "&summary=&source=";
                break;
            case "pinterest":
                return "https://pinterest.com/pin/create/button/?url=" . $title . "&media=" . $link . "&description=";
                break;
            case "email":
                return "mailto:" . $link . "?&subject=" . $title;
            default:
                return $link;
                break;
        }
    }

    function mailer_auth($subject, $to_email, $to_name, $body)
    {
        $this->ci->load->library('my_phpmailer');
        $mail = new PHPMailer;

        try {
            $mail->IsSMTP();
            $mail->SMTPSecure = "ssl";
            $mail->Host = "mail.sehatkerjaku.com"; //hostname masing-masing provider email
            $mail->SMTPDebug = 2;
            $mail->SMTPDebug = FALSE;
            $mail->do_debug = 0;
            $mail->Port = 465;
            $mail->SMTPAuth = true;
            $mail->Username = "support@sehatkerjaku.com"; //user email
            $mail->Password = "Support123!@#"; //password email
            $mail->SetFrom("support@sehatkerjaku.com", $this->web_name); //set email pengirim
            $mail->Subject = $subject; //subyek email
            $mail->AddAddress($to_email, $to_name); //tujuan email
            $mail->MsgHTML($body);
            $mail->Send();
            //echo "Message has been sent";
        } catch (phpmailerException $e) {
            echo $e->errorMessage(); //Pretty error messages from PHPMailer
        } catch (Exception $e) {
            echo $e->getMessage(); //Boring error messages from anything else!
        }
    }

    function generate_random_string($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function response($status = 'success', $type = 'get', $data = array())
    {
        switch ($type) {
            case "get":
                $message = 'Data Berhasil Didapat';
                break;
            case "post":
                $message = 'Data Berhasil Dikirim';
                break;
            case "put":
                $message = 'Data Berhasil Diperbarui';
                break;
            case "delete":
                $message = 'Data Berhasil Dihapus';
                break;
            case "token_error":
                $message = 'Token tidak tersedia';
                break;
            default:
                $message = 'Data Berhasil';
        }

        header('Content-Type: application/json');
        echo json_encode(array(
            'status' => $status,
            'message' => $message,
            'data' => $data
        ));
        exit;
    }

    function token_generate()
    {
        $token = openssl_random_pseudo_bytes(16);
        return bin2hex($token);
    }

    function token()
    {
        $this->ci->load->model('M_user');
        $header_authorization = $this->ci->input->get_request_header('Authorization');
        $header = explode(" ", $header_authorization);
        $prefix = $header[0];
        $token = $header[1];

        $check_token = $this->ci->M_user->count(array('token' => $token));

        if($prefix == 'barier' && $check_token > 0) {
            $user = $this->ci->M_user->row_data(array('token'=> $token));
            $id_user = $user->id;

            return $id_user;
        } else {
            $this->response(
                'error',
                'token_error',
                array()
            );
        }
    }

    function token_login()
    {
        $this->ci->load->model('M_user');
        $header_authorization = $this->ci->input->get_request_header('Authorization');

        if($header_authorization) {
            $header = explode(" ", $header_authorization);
            $prefix = $header[0];
            $token = $header[1];

            $check_token = $this->ci->M_user->count(array('token' => $token));
            if($prefix == 'barier' && $check_token > 0) {
                $user = $this->ci->M_user->row_data(array('token'=> $token));
                $id_user = $user->id;

                return $id_user;
            } else {
                $this->response(
                    'error',
                    'token_error',
                    array()
                );
            }
        } else {
            $token = $this->token_generate();
            $apps_id = $this->generate_random_string(7);

            $device_info = '';
            $email = 'sehatkerjaku@gmail.com';
            $display_name = 'User Apps';
            $id_vendor = '';
            $photo_url = 'https://www.sehatkerjaku.com/assets/template_front/img/user-apps.jpg';
            $id_token = '';
            $google_sign_in = '';

            $data_insert = array(
                'token' => $token,
                'apps_id' => $apps_id,
                'device_info' => $device_info,
                'display_name' => $display_name,
                'email' => $email,
                'id_vendor' => $id_vendor,
                'photo_url' => $photo_url,
                'id_token' => $id_token,
                'google_sign_in' => $google_sign_in,
                'vendor_type' => 'google',
                'created_at' => date('Y-m-d H:i:s')
            );
            $this->ci->db->insert('user', $data_insert);
            $id_user = $this->ci->db->insert_id();

            return $id_user;
        }
    }

    function menu_list()
    {
        $menu = array(
            'MAIN' => array(
                'dashboard' => array(
                    'label' => 'Dashboard',
                    'route' => base_url('intiru/dashboard'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
                'view_front' => array(
                    'label' => 'View Website',
                    'route' => base_url(''),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                )
            ),
            'PAGES' => array(
                'home' => array(
                    'label' => 'Beranda',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'home_page' => array(
                            'label' => 'Halama Umum Beranda',
                            'route' => base_url('intiru/pages/type/home'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'sesi_1' => array(
                            'label' => 'Sesi 1',
                            'route' => base_url('intiru/pages/type/home_sesi_1'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'sesi_2' => array(
                            'label' => 'Sesi 2',
                            'route' => base_url('intiru/pages/type/home_sesi_2'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'sesi_3' => array(
                            'label' => 'Sesi 3',
                            'route' => base_url('intiru/pages/type/home_sesi_3'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
//						'home_slider' => array(
//							'label' => 'Home Slider',
//							'route' => base_url('intiru/home_slider'),
//							'icon' => 'fab fa-asymmetrik'
//						),
                    )
                ),
                'profile' => array(
                    'label' => 'Profil Kami',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'profile_page' => array(
                            'label' => 'Halaman Profil Kami',
                            'route' => base_url('intiru/pages/type/profile'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'profile_team' => array(
                            'label' => 'Daftar Team',
                            'route' => base_url('intiru/profile_team'),
                            'icon' => 'fab fa-asymmetrik'

                        ),
                    )
                ),
                'blog' => array(
                    'label' => 'Artikel',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'blog_page' => array(
                            'label' => 'Halaman Artikel',
                            'route' => base_url('intiru/pages/type/blog'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'blog_category' => array(
                            'label' => 'Kategori Artikel',
                            'route' => base_url('intiru/blog_category'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'blog_list' => array(
                            'label' => 'Daftar Artikel',
                            'route' => base_url('intiru/blog_content'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                    )
                ),
                'services' => array(
                    'label' => 'Layanan Kami',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
//						'services_page' => array(
//							'label' => 'Services Page',
//							'route' => base_url('intiru/pages/type/services'),
//							'icon' => 'fab fa-asymmetrik'
//						),
                        'categories' => array(
                            'label' => 'Halaman Layanan Kami',
                            'route' => base_url('intiru/pages/type/services'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'services' => array(
                            'label' => 'Daftar Item Layanan',
                            'route' => base_url('intiru/tour'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
//                        'services_gallery' => array(
//                            'label' => 'Services Gallery List',
//                            'route' => base_url('intiru/tour_gallery'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
                    )
                ),
//				'testimonial' => array(
//					'label' => 'Testimonial',
//					'route' => 'javascript:;',
//					'icon' => 'fab fa-asymmetrik',
//					'sub_menu' => array(
//						'testimonial_page' => array(
//							'label' => 'Testimonial Page',
//							'route' => base_url('intiru/pages/type/testimonial'),
//							'icon' => 'fab fa-asymmetrik'
//						),
//						'testimonial_list' => array(
//							'label' => 'Testimonial List',
//							'route' => base_url('intiru/testimonial'),
//							'icon' => 'fab fa-asymmetrik'
//						),
//					)
//				),
                'gallery_photo' => array(
                    'label' => 'Galeri Foto',
                    'route' => 'javascript:;',
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array(
                        'gallery_photo_page' => array(
                            'label' => 'Halaman Galeri Foto',
                            'route' => base_url('intiru/pages/type/gallery_photo'),
                            'icon' => 'fab fa-asymmetrik'
                        ),
                        'gallery_photo_list' => array(
                            'label' => 'Daftar Galeri Foto',
                            'route' => base_url('intiru/gallery_photo'),
                            'icon' => 'fab fa-asymmetrik'

                        ),
                    )
                ),
//				'gallery_video' => array(
//					'label' => 'Gallery Video',
//					'route' => 'javascript:;',
//					'icon' => 'fab fa-asymmetrik',
//					'sub_menu' => array(
//						'gallery_photo_page' => array(
//							'label' => 'Gallery Video Page',
//							'route' => base_url('intiru/pages/type/gallery_video'),
//							'icon' => 'fab fa-asymmetrik'
//						),
//						'gallery_photo_list' => array(
//							'label' => 'Gallery Video List',
//							'route' => base_url('intiru/gallery_video'),
//							'icon' => 'fab fa-asymmetrik'
//
//						),
//					)
//				),
//				'about_us' => array(
//					'label' => 'About Us',
//					'route' => base_url('intiru/pages/type/about_us'),
//					'icon' => 'fab fa-asymmetrik',
//					'sub_menu' => array()
//				),

                'ebook' => array(
                    'label' => 'E-Book',
                    'route' => base_url('intiru/ebook'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
                'regulasi' => array(
                    'label' => 'Regulasi',
                    'route' => base_url('intiru/regulasi'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
                'contact_us' => array(
                    'label' => 'Kontak Kami',
                    'route' => base_url('intiru/pages/type/contact_us'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
//                'reservation' => array(
//                    'label' => 'Reservation',
//                    'route' => 'javascript:;',
//                    'icon' => 'fab fa-asymmetrik',
//                    'sub_menu' => array(
//                        'reservation_page' => array(
//                            'label' => 'Reservation Page',
//                            'route' => base_url('intiru/pages/type/reservation'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                        'reservation_list' => array(
//                            'label' => 'Reservation List',
//                            'route' => base_url('intiru/reservation'),
//                            'icon' => 'fab fa-asymmetrik'
//                        ),
//                    )
//                ),
                'footer' => array(
                    'label' => 'Footer Web',
                    'route' => base_url('intiru/pages/type/footer'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
            ),
            'OTHERS MENU' => array(
                'file_manager' => array(
                    'label' => 'File Manager',
                    'route' => base_url('intiru/file_manager'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
                'email' => array(
                    'label' => 'Email',
                    'route' => base_url('intiru/email'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
//                'language' => array(
//                    'label' => 'Language',
//                    'route' => base_url('intiru/language'),
//                    'icon' => 'fab fa-asymmetrik',
//                    'sub_menu' => array()
//                ),
                'admin' => array(
                    'label' => 'Manage Admin',
                    'route' => base_url('intiru/admin'),
                    'icon' => 'fab fa-asymmetrik',
                    'sub_menu' => array()
                ),
            ),
        );

        return $menu;
    }
}
