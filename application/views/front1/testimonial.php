<section>
	<div class="pagenation-holder">
		<div class="container">
			<div class="row">
				<div class="col-md-6"><h1 class="uppercase"><?php echo $page->title ?></h1></div>
				<div class="col-md-6">
					<ol class="breadcrumb">
						<li class="current"><a href="<?php echo base_url() ?>">Home</a></li>
						<li><?php echo $page->title ?></li>
					</ol>
				</div>

			</div>
		</div>
	</div>
</section>
<div class="clearfix"></div>

<section class="sec-padding">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-centered">
				<div class="sec-title-container">
					<div class="ce-title-line"></div>
					<div class="clearfix"></div>
					<div class="col-md-12 nopadding">
						<?php echo $page->description ?>
					</div>
				</div>
			</div>
		</div>

		<div class="row testimonial-wrapper">

			<?php foreach($testimonial as $r){ ?>

			<div class="col-md-4 col-sm-12 col-xs-12 clearfix align-left">
				<div class="ce-feature-box-16 border margin-bottom">
					<div class="text-box text-center">
						<div class="imgbox-small round center overflow-hidden">
							<img src="<?php echo $this->main->image_preview_url($r->thumbnail) ?>" alt="<?php echo $r->thumbnail_alt ?>" class="img-responsive">
						</div>
						<div class="text-box">
							<h6 class="title less-mar-1"><?php echo $r->title ?></h6>
						</div>

						<br>
						<p class="content"><?php echo $r->description ?></p>
					</div>
				</div>
			</div>

			<?php } ?>
		</div>
	</div>
</section>



