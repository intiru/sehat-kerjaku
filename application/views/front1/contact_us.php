<section>
	<div class="pagenation-holder">
		<div class="container">
			<div class="row">
				<div class="col-md-6"><h1 class="uppercase"><?php echo $page->title ?></h1></div>
				<div class="col-md-6">
					<ol class="breadcrumb">
						<li class="current"><a href="<?php echo base_url() ?>">Home</a></li>
						<li><?php echo $page->title ?></li>
					</ol>
				</div>

			</div>
		</div>
	</div>
</section>
<div class="clearfix"></div>

<section class="sec-padding">
	<div class="container">
		<div class="row">

			<div class="col-md-8">

				<div class="one_half">


					<div class="cforms_sty3">


						<form action="<?php echo site_url('contact-us-send') ?>" method="post" class="form-send">

							<div class="row">
								<div class="col-md-12">
									<label class="lable-text" for="name"> First Name</label>
									<input id="name" class="form-control" type="text" name="first_name">
									<span class="help-block error"></span>
								</div>
								<div class="col-md-12">
									<label class="lable-text" for="name"> Last Name</label>
									<input id="name" class="form-control" type="text" name="last_name">
								</div>
								<div class="col-md-12">
									<label class="lable-text" for="email"> Email</label>
									<input id="email" class="form-control" type="text" name="email">
								</div>
								<div class="col-md-12">
									<label class="lable-text" for="email"> Phone</label>
									<input id=text class="form-control" type="text" name="phone">
								</div>
								<div class="col-md-12">
									<label class="lable-text" for="phone">Subject</label>
									<input id="phone" class="form-control" type="text" name="subject">
								</div>
								<div class="col-md-12">
									<label class="lable-text" for="message">Message</label>
									<textarea id="comment" class="form-control" name="message"></textarea>
								</div>
								<div class="col-md-12">
									<label class="lable-text" for="message">Security Code</label>
									<br/>
									<?php echo $captcha ?>
									<br/><br/>
									<input id="phone" class="form-control" type="text" name="captcha" style="width: 200px">
								</div>

								<div class="col-md-12 text-center">
									<br/>
									<button type="submit"
											class="btn btn-medium btn-orange btn-anim-1 uppercase xround-4">
										<i class="fa fa-check" aria-hidden="true"></i>
										<span>Send Message</span>
									</button>
								</div>
							</div>
						</form>

					</div>

				</div>

			</div>
			<!--end item-->

			<div class="col-md-4 text-left">
				<a href="<?php echo $this->main->image_preview_url('harsa-bali-holiday-logo.png') ?>" target="_blank">
					<img src="<?php echo $this->main->image_preview_url('harsa-bali-holiday-logo-light.png') ?>">
				</a>
				<br /><br />
				<address>
					<strong>Office Address:</strong><br>
					<?php echo $alamat ?>
				</address>
				<abbr title="Phone Number"><strong>Telephone:</strong></abbr> <a
					href="telp:<?php echo $telephone ?>"><?php echo $telephone ?></a><br>
				<abbr title="Phone Number"><strong>Phone:</strong></abbr> <a
					href="telp:<?php echo $phone ?>"><?php echo $phone ?> </a><br>
				<abbr title="Phone Number"><strong>WhatsApp:</strong></abbr> <a href="<?php echo $whatsapp_link ?>"
																				target="_blank"><?php echo $whatsapp ?></a><br>
				<abbr title="Phone Number"><strong>WeChat:</strong></abbr> <a href="<?php echo $wechat_link ?>"
																			  target="_blank"><?php echo $wechat_id ?></a><br>
				<abbr title="Email Address"><strong>Email:</strong></abbr> <a
					href="mailto:<?php echo $email ?>"><?php echo $email ?></a>
				<br/><br/>
				<div class="widget noborder notoppadding">
					<div id="s-icons" class="widget quick-contact-widget clearfix">
						<h4 class="highlight-me nopadding">Connect Socially</h4>
						<ul class="footer-social-icons blue left-align icons-plain nopadding text-center">
							<li><a href="<?php echo $facebook_link ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
							<li><a href="<?php echo $instagram_link ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
							<li><a href="<?php echo $line_link ?>" target="_blank"><img src="<?php echo base_url('assets/front/images/line.png') ?>"></a></li>
							<li><a href="<?php echo $whatsapp_link ?>" target="_blank"><i class="fa fa-whatsapp"></i></a></li>
						</ul>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>

<iframe
	src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15780.721936937447!2d115.22448505!3d-8.578638649999995!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2dd23e98ce81d05d%3A0xeba44397531d427f!2sGreen%20Village%20Bali!5e0!3m2!1sid!2sid!4v1570255584559!5m2!1sid!2sid"
	width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

<script type="text/javascript"
		src="<?php echo base_url() ?>assets/vendors/general/sweetalert2/dist/sweetalert2.min.js"></script>
<link rel="stylesheet" type="text/css"
	  href="<?php echo base_url() ?>assets/vendors/general/sweetalert2/dist/sweetalert2.min.css">
<script type="text/javascript" src="<?php echo base_url() ?>assets/front/js/app.js"></script>


