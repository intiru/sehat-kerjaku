<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Virat | Multi-Purpose HTML5 Template </title>
	<meta name="keywords" content=""/>
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Mobile view -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Favicon -->
	<link rel="shortcut icon" href="images/fav-icon.ico">
	<link rel="stylesheet" type="text/css" href="js/bootstrap/bootstrap.min.css">

	<!-- Google fonts  -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i"
		  rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700"
		  rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Yesteryear" rel="stylesheet">

	<!-- Template's stylesheets -->
	<link rel="stylesheet" href="js/megamenu/stylesheets/screen.css">
	<link rel="stylesheet" href="css/theme-default.css" type="text/css">
	<link rel="stylesheet" href="js/loaders/stylesheets/screen.css">
	<link rel="stylesheet" href="css/corporate.css" type="text/css">
	<link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.min.css" type="text/css">
	<link rel="stylesheet" type="text/css" href="fonts/Simple-Line-Icons-Webfont/simple-line-icons.css" media="screen"/>
	<link rel="stylesheet" href="fonts/et-line-font/et-line-font.css">
	<link rel="stylesheet" type="text/css" href="js/revolution-slider/css/settings.css">
	<link rel="stylesheet" type="text/css" href="js/revolution-slider/css/layers.css">
	<link rel="stylesheet" type="text/css" href="js/revolution-slider/css/navigation.css">
	<link rel="stylesheet" href="js/parallax/main.css">
	<link rel="stylesheet" type="text/css" href="js/cubeportfolio/cubeportfolio.min.css">
	<link href="js/owl-carousel/owl.carousel.css" rel="stylesheet">
	<link href="js/owl-carousel/owl.theme.css" rel="stylesheet">
	<link href="js/tabs/css/responsive-tabs.css" rel="stylesheet" type="text/css" media="all"/>
	<link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
	<link rel="stylesheet" href="js/ytplayer/ytplayer.css"/>
	<link href="js/accordion/css/smk-accordion.css" rel="stylesheet">
	<link rel="stylesheet" href="fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
	<!-- Template's stylesheets END -->

	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Style Customizer's stylesheets -->

	<link rel="stylesheet/less" type="text/css" href="less/skin.less">
	<!-- Style Customizer's stylesheets END -->

	<!-- Skin stylesheet -->

</head>

<body>
<div class="over-loader loader-live">
	<div class="loader">
		<div class="loader-item style4">
			<div class="cube1"></div>
			<div class="cube2"></div>
		</div>
	</div>
</div>
<!--end loading-->


<div class="wrapper-boxed">
	<div class="site-wrapper">
		<div class="col-md-12 nopadding">
			<div class="header-section style1 pin-style">
				<div class="container">
					<div class="mod-menu">
						<div class="row">
							<div class="col-sm-2">
								<a href="index.html" title="" class="logo style-2 mar-4">
									<img src="images/logo/logo.png" alt="">
								</a>
							</div>
							<div class="col-sm-10">
								<div class="main-nav">
									<ul class="nav navbar-nav top-nav">
										<li class="search-parent"><a href="javascript:void(0)" title=""><i
													aria-hidden="true" class="fa fa-search"></i></a>
											<div class="search-box ">
												<div class="content">
													<div class="form-control">
														<input type="text" placeholder="Type to search"/>
														<a href="#" class="search-btn mar-1"><i aria-hidden="true"
																								class="fa fa-search"></i></a>
													</div>
													<a href="#" class="close-btn mar-1">x</a></div>
											</div>
										</li>
										<li class="cart-parent"><a href="javascript:void(0)" title=""> <i
													aria-hidden="true" class="fa fa-shopping-cart"></i> <span
													class="number mar2"> 4 </span> </a>
											<div class="cart-box">
												<div class="content">
													<div class="row">
														<div class="col-xs-8"> 2 item(s)</div>
														<div class="col-xs-4 text-right"><span>$99</span></div>
													</div>
													<ul>
														<li><img src="http://via.placeholder.com/80x80" alt=""> Jean &
															Teashirt <span>$30</span> <a href="#" title=""
																						 class="close-btn">x</a></li>
														<li><img src="http://via.placeholder.com/80x80" alt=""> Jean &
															Teashirt <span>$30</span> <a href="#" title=""
																						 class="close-btn">x</a></li>
													</ul>
													<div class="row">
														<div class="col-xs-6"><a href="#" title="View Cart"
																				 class="btn btn-block btn-warning">View
																Cart</a></div>
														<div class="col-xs-6"><a href="#" title="Check out"
																				 class="btn btn-block btn-primary">Check
																out</a></div>
													</div>
												</div>
											</div>
										</li>
										<li class="visible-xs menu-icon"><a href="javascript:void(0)"
																			class="navbar-toggle collapsed"
																			data-toggle="collapse" data-target="#menu"
																			aria-expanded="false"> <i aria-hidden="true"
																									  class="fa fa-bars"></i>
											</a></li>
									</ul>
									<div id="menu" class="collapse">
										<ul class="nav navbar-nav">
											<li class="right active"><a href="#">Home</a> <span class="arrow"></span>
												<ul class="dm-align-2">
													<li><a href="index2.html">Home Page 2</a></li>
													<li><a href="index3.html">Home Page 3</a></li>
													<li><a href="index4.html">Home Page 4</a></li>
													<li><a href="index5.html">Home Page 5</a></li>
													<li><a href="index6.html">Home Page 6</a></li>
													<li><a href="index7.html">Home Page 7</a></li>
													<li><a href="index8.html">Home Page 8</a></li>
													<li><a href="index9.html">Home Page 9</a></li>
													<li><a href="index10.html">Home Page 10</a></li>
													<li class="active"><a href="index.html">Home Default</a></li>
												</ul>
											</li>
											<li><a href="page-about2.html">Pages</a> <span class="arrow"></span>
												<ul class="dm-align-2">
													<li><a href="#">About <span class="sub-arrow dark pull-right"><i
																	class="fa fa-angle-right"
																	aria-hidden="true"></i></span> </a> <span
															class="arrow"></span>
														<ul>
															<li><a href="page-about1.html">About Style 1</a></li>
															<li><a href="page-about2.html">About Style 2</a></li>
															<li><a href="page-about3.html">About Style 3</a></li>
															<li><a href="page-about4.html">About Style 4</a></li>
															<li><a href="page-about5.html">About Me</a></li>
														</ul>
													</li>
													<li><a href="#">Services <span class="sub-arrow dark pull-right"><i
																	class="fa fa-angle-right"
																	aria-hidden="true"></i></span> </a> <span
															class="arrow"></span>
														<ul>
															<li><a href="page-services1.html">Services Style 1</a></li>
															<li><a href="page-services2.html">Services Style 2</a></li>
															<li><a href="page-services3.html">Services Style 3</a></li>
															<li><a href="page-services4.html">Services Style 4</a></li>
														</ul>
													</li>
													<li><a href="#">Team <span class="sub-arrow dark pull-right"><i
																	class="fa fa-angle-right"
																	aria-hidden="true"></i></span> </a> <span
															class="arrow"></span>
														<ul>
															<li><a href="page-team-classic.html">Team Classic</a></li>
															<li><a href="page-team-parallax.html">Team Parallax</a></li>
															<li><a href="page-team-dark.html">Team dark Style</a></li>
															<li><a href="page-team-creative.html">Team Creative</a></li>
														</ul>
													</li>
													<li><a href="#">FAQ <span class="sub-arrow dark pull-right"><i
																	class="fa fa-angle-right"
																	aria-hidden="true"></i></span> </a> <span
															class="arrow"></span>
														<ul>
															<li><a href="page-faq1.html">FAQ Style 1</a></li>
															<li><a href="page-faq2.html">FAQ Style 2</a></li>
														</ul>
													</li>
													<li><a href="#">Contact<span class="sub-arrow dark pull-right"><i
																	class="fa fa-angle-right"
																	aria-hidden="true"></i></span> </a> <span
															class="arrow"></span>
														<ul>
															<li><a href="page-contact.html">Contact Classic</a></li>
															<li><a href="page-contact-left-sidebar.html">Contact Left
																	Sidebar</a></li>

															<li><a href="page-contact-map.html">Full Width Map</a></li>


														</ul>
													</li>
													<li><a href="#">Other Pages 1<span
																class="sub-arrow dark pull-right"><i
																	class="fa fa-angle-right"
																	aria-hidden="true"></i></span> </a> <span
															class="arrow"></span>
														<ul>
															<li><a href="page-full-width.html">Full Width Page</a></li>
															<li><a href="page-left-sidebar.html">Left Sidebar</a></li>
															<li><a href="page-right-sidebar.html">Right Sidebar</a></li>
															<li><a href="page-packages.html">Package Plans</a></li>
															<li><a href="page-careers.html">Careers</a></li>
															<li><a href="page-coming-soon.html">Coming Soon</a></li>
														</ul>
													</li>
													<li><a href="#">Other Pages 2<span
																class="sub-arrow dark pull-right"><i
																	class="fa fa-angle-right"
																	aria-hidden="true"></i></span> </a> <span
															class="arrow"></span>
														<ul>

															<li><a href="page-sitemap.html">Sitemap</a></li>
															<li><a href="page-maintenance.html">Maintenance</a></li>
															<li><a href="page-404.html">404 Error Page</a></li>
															<li><a href="page-404-2.html">404 Error Page 2</a></li>
														</ul>
													</li>
												</ul>
											</li>
											<li><a href="page-about4.html">Features</a> <span class="arrow"></span>
												<ul class="dm-align-2">

													<li><a href="#">Headers <span class="sub-arrow dark pull-right"><i
																	class="fa fa-angle-right"
																	aria-hidden="true"></i></span> </a> <span
															class="arrow"></span>
														<ul>
															<li><a href="page-about4.html">Header Light</a></li>
															<li><a href="page-about5.html">Header Dark</a></li>
															<li><a href="index4.html">Header Modern</a></li>
															<li><a href="index.html">Header Transparent</a></li>
															<li><a href="page-team-creative.html">Header Creative</a>
															</li>
															<li><a href="index.html">Header Left Logo</a></li>
															<li><a href="page-team-creative.html">Header Center Logo</a>
															</li>
															<li><a href="index7.html">Header White</a></li>
														</ul>
													</li>
													<li><a href="#">Menu Styles <span class="sub-arrow dark pull-right"><i
																	class="fa fa-angle-right"
																	aria-hidden="true"></i></span> </a> <span
															class="arrow"></span>
														<ul>
															<li><a href="index.html">Menu Transparent</a></li>
															<li><a href="index2.html">Menu Left logo</a></li>
															<li><a href="index7.html">Menu light</a></li>
															<li><a href="index2.html">Menu Dark</a></li>
															<li><a href="page-team-creative.html">Menu Center Logo</a>
															</li>
															<li><a href="index4.html">Menu Boxed</a></li>
															<li><a href="page-team-creative.html">Menu Center</a></li>
														</ul>
													</li>
													<li><a href="#">Loading Styles<span
																class="sub-arrow dark pull-right"><i
																	class="fa fa-angle-right"
																	aria-hidden="true"></i></span> </a> <span
															class="arrow"></span>
														<ul>
															<li><a href="loading-style1.html">Loading Style 1</a></li>
															<li><a href="loading-style2.html">Loading Style 2</a></li>
															<li><a href="loading-style3.html">Loading Style 3</a></li>
															<li><a href="loading-style4.html">Loading Style 4</a></li>
															<li><a href="loading-style5.html">Loading Style 5</a></li>
															<li><a href="loading-style6.html">Loading Style 6</a></li>
															<li><a href="loading-style7.html">Loading Style 7</a></li>
															<li><a href="loading-style8.html">Loading Style 8</a></li>
															<li><a href="loading-style9.html">Loading Style 9</a></li>
															<li><a href="loading-style10.html">Loading Style 10</a></li>
														</ul>
													</li>
													<li><a href="#">Footer Styles<span
																class="sub-arrow dark pull-right"><i
																	class="fa fa-angle-right"
																	aria-hidden="true"></i></span> </a> <span
															class="arrow"></span>
														<ul>
															<li><a href="index3.html">Footer Dark</a></li>
															<li><a href="index4.html">Footer Light</a></li>
															<li><a href="index3.html">Footer Simple</a></li>
															<li><a href="page-about1.html">Footer Parallax</a></li>
															<li><a href="index8.html">Footer Big</a></li>
															<li><a href="index5.html">Footer Modern</a></li>
															<li><a href="index10.html">Footer With Map</a></li>
															<li><a href="page-about1.html">Footer Transparent</a></li>
															<li><a href="index4.html">Footer Classic</a></li>
														</ul>
													</li>
													<li><a href="#">Videos<span class="sub-arrow dark pull-right"><i
																	class="fa fa-angle-right"
																	aria-hidden="true"></i></span> </a> <span
															class="arrow"></span>
														<ul>
															<li><a href="shortcodes-videos.html">Youtube Videos</a></li>
															<li><a href="shortcodes-videos.html">Vimeo Videos</a></li>
															<li><a href="shortcodes-videos.html">HTML 5 Videos</a></li>
														</ul>
													</li>
													<li><a href="#">Layered PSD Files</a></li>
													<li><a href="#">Unlimited Colors</a></li>
													<li><a href="#">Wide & Boxed</a></li>
												</ul>
											</li>
											<li class="mega-menu"><a href="portfolio-full-width.html">Portfolio</a>
												<span class="arrow"></span>
												<ul>
													<li><a href="#" title="home samples">Portfolio columns</a> <span
															class="arrow"></span>
														<ul>
															<li><a href="portfolio-1-columns.html"><i
																		class="fa fa-angle-right"></i> &nbsp; One Column</a>
															</li>
															<li><a href="portfolio-2-columns.html"><i
																		class="fa fa-angle-right"></i> &nbsp; Two Column</a>
															</li>
															<li><a href="portfolio-3-columns.html"><i
																		class="fa fa-angle-right"></i> &nbsp; Three
																	Column</a></li>
															<li><a href="portfolio-4-columns.html"><i
																		class="fa fa-angle-right"></i> &nbsp; Four
																	Column</a></li>
															<li><a href="portfolio-5-columns.html"><i
																		class="fa fa-angle-right"></i> &nbsp; Five
																	Column</a></li>
															<li><a href="portfolio-6-columns.html"><i
																		class="fa fa-angle-right"></i> &nbsp; Six Column</a>
															</li>
														</ul>
													</li>
													<li><a href="#">Portfolio Styles</a> <span class="arrow"></span>
														<ul>
															<li><a href="portfolio-masonry.html"><i
																		class="fa fa-angle-right"></i> &nbsp;
																	Masonry</a></li>
															<li><a href="portfolio-masonry-projects.html"><i
																		class="fa fa-angle-right"></i> &nbsp;
																	Masonry-Projects</a></li>
															<li><a href="portfolio-mosaic.html"><i
																		class="fa fa-angle-right"></i> &nbsp; Mosaic</a>
															</li>
															<li><a href="portfolio-mosaic-flat.html"><i
																		class="fa fa-angle-right"></i> &nbsp;
																	Mosaic-Flat</a></li>
															<li><a href="portfolio-mosaic-projects.html"><i
																		class="fa fa-angle-right"></i> &nbsp;
																	Mosaic-Projects</a></li>
															<li><a href="portfolio-slider-projects.html"><i
																		class="fa fa-angle-right"></i> &nbsp;
																	slider-projects</a></li>
														</ul>
													</li>
													<li><a href="#">Portfolio Styles</a> <span class="arrow"></span>
														<ul>
															<li><a href="portfolio-full-width.html"><i
																		class="fa fa-angle-right"></i> &nbsp; Full Width</a>
															</li>
															<li><a href="portfolio-gallery.html"><i
																		class="fa fa-angle-right"></i> &nbsp;
																	Gallery</a></li>
															<li><a href="portfolio-classic.html"><i
																		class="fa fa-angle-right"></i> &nbsp;
																	Classic</a></li>
															<li><a href="portfolio-nospace.html"><i
																		class="fa fa-angle-right"></i> &nbsp; No
																	Space</a></li>
															<li><a href="portfolio-boxed-size.html"><i
																		class="fa fa-angle-right"></i> &nbsp; Boxed Size</a>
															</li>
															<li><a href="portfolio-modern.html"><i
																		class="fa fa-angle-right"></i> &nbsp; Modern</a>
															</li>
														</ul>
													</li>
													<li><a href="#">Portfolio Single Page</a> <span
															class="arrow"></span>
														<ul>
															<li><a href="portfolio-parallax.html"><i
																		class="fa fa-angle-right"></i> &nbsp; Parallax
																	Image</a></li>
															<li><a href="portfolio-video.html"><i
																		class="fa fa-angle-right"></i> &nbsp; Video
																	Background</a></li>
															<li><a href="portfolio-left-sidebar.html"><i
																		class="fa fa-angle-right"></i> &nbsp; Left
																	Sidebar</a></li>
															<li><a href="portfolio-right-sidebar.html"><i
																		class="fa fa-angle-right"></i> &nbsp; Right
																	Sidebar</a></li>
															<li><a href="portfolio-carousel.html"><i
																		class="fa fa-angle-right"></i> &nbsp;
																	Carousel</a></li>
															<li><a href="portfolio-fullwidth-image.html"><i
																		class="fa fa-angle-right"></i> &nbsp; Full width
																	Image</a></li>
														</ul>
													</li>
												</ul>
											</li>
											<li class="mega-menu five-col"><a href="shortcodes-carousel-sliders.html">Shortcodes</a>
												<span class="arrow"></span>
												<ul>
													<li><a href="#">Shortcodes 1</a> <span class="arrow"></span>
														<ul>
															<li><a href="shortcodes-accordions.html"><i
																		class="fa fa-plus-circle"></i> &nbsp; Accordions</a>
															</li>
															<li><a href="shortcodes-alerts.html"><i
																		class="fa fa-exclamation"
																		aria-hidden="true"></i> &nbsp; Alerts</a></li>
															<li><a href="shortcodes-animations.html"><i
																		class="fa fa-bars"></i> &nbsp; Animations</a>
															</li>
															<li><a href="shortcodes-blockquotes.html"><i
																		class="fa fa-quote-right"
																		aria-hidden="true"></i> &nbsp; Blockquotes</a>
															</li>
															<li><a href="shortcodes-breadcrumbs.html"><i
																		class="fa fa-share" aria-hidden="true"></i>
																	&nbsp; Breadcrumbs</a></li>
															<li><a href="shortcodes-buttons.html"><i
																		class="fa fa-external-link"
																		aria-hidden="true"></i> &nbsp; Buttons</a></li>
															<li><a href="shortcodes-call-to-action.html"><i
																		class="fa fa-level-up" aria-hidden="true"></i>
																	&nbsp; Call to Action</a></li>
															<li><a href="shortcodes-clients-logos.html"><i
																		class="fa fa-user" aria-hidden="true"></i>
																	&nbsp; Clients Logos</a></li>
															<li><a href="shortcodes-carousel-sliders.html"><i
																		class="fa fa-sort" aria-hidden="true"></i>
																	&nbsp; Carousel Sliders</a></li>
															<li><a href="shortcodes-counters.html"><i
																		class="fa fa-text-height"
																		aria-hidden="true"></i> &nbsp; Counters</a></li>
														</ul>
													</li>
													<li><a href="#">Shortcodes 2</a> <span class="arrow"></span>
														<ul>
															<li><a href="shortcodes-content-boxes.html"><i
																		class="fa fa-th" aria-hidden="true"></i> &nbsp;
																	Content Boxes</a></li>
															<li><a href="shortcodes-data-tables.html"><i
																		class="fa fa-table" aria-hidden="true"></i>
																	&nbsp; Data Tables</a></li>
															<li><a href="shortcodes-date-pickers.html"><i
																		class="fa fa-calendar" aria-hidden="true"></i>
																	&nbsp; Date Pickers</a></li>
															<li><a href="shortcodes-dropcaps.html"><i class="fa fa-font"
																									  aria-hidden="true"></i>
																	&nbsp; Dropcap & Highlight</a></li>
															<li><a href="shortcodes-dividers.html"><i
																		class="fa fa-minus" aria-hidden="true"></i>
																	&nbsp; Dividers</a></li>
															<li><a href="shortcodes-file-uploads.html"><i
																		class="fa fa-upload" aria-hidden="true"></i>
																	&nbsp; File Uploads</a></li>
															<li><a href="shortcodes-forms.html"><i
																		class="fa fa-envelope" aria-hidden="true"></i>
																	&nbsp; Forms</a></li>
															<li><a href="shortcodes-grids.html"><i class="fa fa-th-list"
																								   aria-hidden="true"></i>
																	&nbsp; Grids</a></li>
															<li><a href="shortcodes-heading-styles.html"><i
																		class="fa fa-text-height"
																		aria-hidden="true"></i> &nbsp; Heading
																	Styles</a></li>
															<li><a href="shortcodes-hover-styles.html"><i
																		class="fa fa-picture-o" aria-hidden="true"></i>
																	&nbsp; Hover Styles</a></li>
														</ul>
													</li>
													<li><a href="#">Shortcodes 3</a> <span class="arrow"></span>
														<ul>
															<li><a href="shortcodes-icon-boxes.html"><i
																		class="fa fa-th-large" aria-hidden="true"></i>
																	&nbsp; Icon Boxes</a></li>
															<li><a href="shortcodes-icon-circles.html"><i
																		class="fa fa-circle-o" aria-hidden="true"></i>
																	&nbsp; Icon Circles</a></li>
															<li><a href="shortcodes-countdown-timers.html"><i
																		class="fa fa-bars" aria-hidden="true"></i>
																	&nbsp; Countdown Timers</a></li>
															<li><a href="shortcodes-icon-lists.html"><i
																		class="fa fa-list" aria-hidden="true"></i>
																	&nbsp; Icon Lists</a></li>
															<li><a href="shortcodes-images.html"><i
																		class="fa fa-picture-o" aria-hidden="true"></i>
																	&nbsp; Images</a></li>
															<li><a href="shortcodes-labels-and-badges.html"><i
																		class="fa fa-adjust" aria-hidden="true"></i>
																	&nbsp; Labels and Badges</a></li>
															<li><a href="shortcodes-lightbox.html"><i class="fa fa-th"
																									  aria-hidden="true"></i>
																	&nbsp; Lightbox</a></li>
															<li><a href="shortcodes-lists.html"><i class="fa fa-list-ul"
																								   aria-hidden="true"></i>
																	&nbsp; Lists</a></li>
															<li><a href="shortcodes-maps.html"><i
																		class="fa fa-map-marker" aria-hidden="true"></i>
																	&nbsp; Maps</a></li>
															<li><a href="shortcodes-modal-popup.html"><i
																		class="fa fa-search-plus"
																		aria-hidden="true"></i> &nbsp; Modal Popup</a>
															</li>
														</ul>
													</li>
													<li><a href="#">Shortcode 4</a> <span class="arrow"></span>
														<ul>
															<li><a href="shortcodes-pagenation.html"><i
																		class="fa fa-exchange" aria-hidden="true"></i>
																	&nbsp; Pagenation</a></li>
															<li><a href="shortcodes-parallax-backgrounds.html"><i
																		class="fa fa-align-center"
																		aria-hidden="true"></i> &nbsp; Parallax
																	Backgrounds</a></li>
															<li><a href="shortcodes-pricing-tables.html"><i
																		class="fa fa-table" aria-hidden="true"></i>
																	&nbsp; Pricing Tables</a></li>
															<li><a href="shortcodes-pie-charts.html"><i
																		class="fa fa-pie-chart" aria-hidden="true"></i>
																	&nbsp; Pie Charts</a></li>
															<li><a href="shortcodes-pricing-badges.html"><i
																		class="fa fa-external-link"></i> &nbsp; Pricing
																	Badges</a></li>
															<li><a href="shortcodes-progress-bars.html"><i
																		class="fa fa-outdent" aria-hidden="true"></i>
																	&nbsp; Progress Bars</a></li>
															<li><a href="shortcodes-process-steps.html"><i
																		class="fa fa-long-arrow-right"
																		aria-hidden="true"></i> &nbsp; Process Steps</a>
															</li>
															<li><a href="shortcodes-post-styles.html"><i
																		class="fa fa-pencil" aria-hidden="true"></i>
																	&nbsp; Post Styles</a></li>
															<li><a href="shortcodes-toogle-switches.html"><i
																		class="fa fa-toggle-on" aria-hidden="true"></i>
																	&nbsp; Toogle Switches</a></li>
															<li><a href="shortcodes-timeline.html"><i
																		class="fa fa-align-left" aria-hidden="true"></i>
																	&nbsp; Timeline</a></li>
														</ul>
													</li>
													<li><a href="#">Shortcode 5</a> <span class="arrow"></span>
														<ul>
															<li><a href="shortcodes-star-ratings.html"><i
																		class="fa fa-star-half-o"
																		aria-hidden="true"></i> &nbsp; Star Ratings</a>
															</li>
															<li><a href="shortcodes-sections.html"><i
																		class="fa fa-square-o" aria-hidden="true"></i>
																	&nbsp; Sections</a></li>
															<li><a href="shortcodes-social-icons.html"><i
																		class="fa fa-twitter" aria-hidden="true"></i>
																	&nbsp; Social Icons</a></li>
															<li><a href="shortcodes-tabs.html"><i class="fa fa-th-large"
																								  aria-hidden="true"></i>
																	&nbsp; Tabs</a></li>
															<li><a href="shortcodes-team.html"><i class="fa fa-user"
																								  aria-hidden="true"></i>
																	&nbsp; Team</a></li>
															<li><a href="shortcodes-testimonials.html"><i
																		class="fa fa-pencil-square"></i> &nbsp;
																	Testimonials</a></li>
															<li><a href="shortcodes-tooltips.html"><i class="fa fa-font"
																									  aria-hidden="true"></i>
																	&nbsp; Tooltips</a></li>
															<li><a href="shortcodes-toogles.html"><i
																		class="fa fa-toggle-on" aria-hidden="true"></i>
																	&nbsp; Toogles</a></li>
															<li><a href="shortcodes-typography.html"><i
																		class="fa fa-text-height"
																		aria-hidden="true"></i> &nbsp; Typography</a>
															</li>
															<li><a href="shortcodes-videos.html"><i
																		class="fa fa-play-circle"
																		aria-hidden="true"></i> &nbsp; Videos</a></li>
														</ul>
													</li>
												</ul>
											</li>
											<li class="right"><a href="#">Blog</a> <span class="arrow"></span>
												<ul class="dm-align-2">

													<li><a href="blog-full-width.html">Full Width</a></li>
													<li><a href="blog-left-sidebar.html">Left Sidebar</a></li>
													<li><a href="blog-3-columns.html">Three Column</a></li>
													<li><a href="blog-default-author.html">Author Page</a></li>
													<li><a href="blog-default-comments.html">Blog Comments</a></li>
													<li><a href="blog-image-post.html">Image Post</a></li>
													<li><a href="blog-video-post.html">Video Post</a></li>

												</ul>
											</li>
											<li class="right"><a href="#">Shop</a> <span class="arrow"></span>
												<ul>
													<li><a href="shop-full-width.html">Full Width</a></li>
													<li><a href="shop-left-sidebar.html">Left Sidebar</a></li>
													<li><a href="shop-3-columns.html">Three Column</a></li>
													<li><a href="shop-single-full-width.html">Product Preview</a></li>
													<li><a href="shop-single-left-sidebar.html">Preview Sidebar</a></li>

												</ul>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--end menu-->

		</div>
		<!--end menu-->

		<div class="clearfix"></div>

		<!-- START REVOLUTION SLIDER 5.0 -->
		<div class="slide-tmargin">
			<div class="slidermaxwidth">
				<div class="rev_slider_wrapper">
					<!-- START REVOLUTION SLIDER 5.0 auto mode -->
					<div id="rev_slider" class="rev_slider" data-version="5.0">
						<ul>

							<!-- SLIDE  -->
							<li data-index="rs-1" data-transition="fade">

								<!-- MAIN IMAGE -->
								<img src="http://via.placeholder.com/2000x1300" alt="" width="1920" height="1280">

								<!-- LAYER NR. 2 -->
								<div class="tp-caption montserrat fweight-6 text-white tp-resizeme"
									 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
									 data-y="['top','top','top','top']" data-voffset="['320','100','100','110']"
									 data-fontsize="['70','70','50','30']"
									 data-lineheight="['100','100','100','50']"
									 data-width="none"
									 data-height="none"
									 data-whitespace="nowrap"
									 data-transform_idle="o:1;"
									 data-transform_in="x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
									 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
									 data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
									 data-start="1000"
									 data-splitin="none"
									 data-splitout="none"
									 data-responsive_offset="on"
									 style="z-index: 7; white-space: nowrap;">Business & Multi
								</div>

								<!-- LAYER NR. 3 -->
								<div class="tp-caption montserrat fweight-6 text-white tp-resizeme"
									 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
									 data-y="['top','top','top','top']" data-voffset="['380','150','150','150']"
									 data-fontsize="['70','60','50','30']"
									 data-lineheight="['100','100','100','50']"
									 data-width="none"
									 data-height="none"
									 data-whitespace="nowrap"
									 data-transform_idle="o:1;"
									 data-transform_in="x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
									 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
									 data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
									 data-start="1500"
									 data-splitin="none"
									 data-splitout="none"
									 data-responsive_offset="on"
									 style="z-index: 7; white-space: nowrap;">Pages Designs
								</div>

								<!-- LAYER NR. 4 -->
								<div class="tp-caption sbut2"
									 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
									 data-y="['top','top','top','top']" data-voffset="['550','350','370','300']"
									 data-speed="800"
									 data-start="2000"
									 data-transform_in="y:bottom;s:1500;e:Power3.easeOut;"
									 data-transform_out="opacity:0;s:3000;e:Power4.easeIn;s:3000;e:Power4.easeIn;"
									 data-endspeed="300"
									 data-captionhidden="off"
									 style="z-index: 6"><a href="#">Get Started now!</a></div>
							</li>

							<!-- SLIDE  -->
							<li data-index="rs-2" data-transition="slideleft">

								<!-- MAIN IMAGE -->
								<img src="http://via.placeholder.com/2000x1300" alt="" width="1920" height="1280">

								<!-- LAYER NR. 2 -->
								<div class="tp-caption montserrat fweight-6 text-white tp-resizeme"
									 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
									 data-y="['top','top','top','top']" data-voffset="['320','100','100','110']"
									 data-fontsize="['70','70','50','30']"
									 data-lineheight="['100','100','100','50']"
									 data-width="none"
									 data-height="none"
									 data-whitespace="nowrap"
									 data-transform_idle="o:1;"
									 data-transform_in="x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
									 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
									 data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
									 data-start="1000"
									 data-splitin="none"
									 data-splitout="none"
									 data-responsive_offset="on"
									 style="z-index: 7; white-space: nowrap;">Business & Multi
								</div>

								<!-- LAYER NR. 3 -->
								<div class="tp-caption montserrat fweight-6 text-white tp-resizeme"
									 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
									 data-y="['top','top','top','top']" data-voffset="['380','150','150','150']"
									 data-fontsize="['70','60','50','30']"
									 data-lineheight="['100','100','100','50']"
									 data-width="none"
									 data-height="none"
									 data-whitespace="nowrap"
									 data-transform_idle="o:1;"
									 data-transform_in="x:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
									 data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
									 data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
									 data-start="1500"
									 data-splitin="none"
									 data-splitout="none"
									 data-responsive_offset="on"
									 style="z-index: 7; white-space: nowrap;">Home Pages
								</div>

								<!-- LAYER NR. 4 -->
								<div class="tp-caption sbut2"
									 data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
									 data-y="['top','top','top','top']" data-voffset="['550','350','370','300']"
									 data-speed="800"
									 data-start="2000"
									 data-transform_in="y:bottom;s:1500;e:Power3.easeOut;"
									 data-transform_out="opacity:0;s:3000;e:Power4.easeIn;s:3000;e:Power4.easeIn;"
									 data-endspeed="300"
									 data-captionhidden="off"
									 style="z-index: 6"><a href="#">Get Started now!</a></div>
							</li>

						</ul>
					</div>
					<!-- END REVOLUTION SLIDER -->
				</div>
			</div>
			<!-- END REVOLUTION SLIDER WRAPPER -->
		</div>
		<div class="clearfix"></div>
		<!-- END OF SLIDER WRAPPER -->

		<section class="section-side-image section-light sec-padding clearfix">
			<div class="img-holder col-md-12 col-sm-12 pull-left">
				<div class="background-imgholder" style="background:url(http://via.placeholder.com/1500x1000);"><img
						class="nodisplay-image" src="http://via.placeholder.com/1500x1000" alt=""/></div>
			</div>
			<div class="container">
				<div class="row">

					<div class="col-sm-8 col-centered">
						<div class="sec-title-container text-left">
							<div class="ce-title-line"></div>
							<div class="clearfix"></div>
							<h2 class="font-weight-6 less-mar-1 line-height-5">Our Excellent Services <br/>
								& More Features </h2>
							<div class="col-md-6 nopadding"><h6 class="font-weight-3">Praesent mattis commodo augue
									Aliquam ornare hendrerit augue Cras tellus. </h6></div>
						</div>
					</div>
					<div class="clearfix"></div>
					<!--end title-->

					<div class="col-md-4">
						<div class="ce-feature-box-6 text-center">
							<div class="icon-plain-small center"><span class="icon-phone text-primary"></span></div>
							<div class="text-box text-center">
								<div class="top-line"></div>
								<h5 class="title font-weight-5">Responsive Design</h5>
								<p>Amet dolor Vestibulum ante ipsum primis in faucibus.</p>
							</div>
						</div>
					</div>
					<!--end item-->

					<div class="col-md-4">
						<div class="ce-feature-box-6 text-center">
							<div class="icon-plain-small center"><span class="icon-lock text-primary"></span></div>
							<div class="text-box text-center">
								<div class="top-line"></div>
								<h5 class="title font-weight-5">Secure Code</h5>
								<p class="content">Amet dolor Vestibulum ante ipsum primis in faucibus.</p>
							</div>
						</div>
					</div>
					<!--end item-->


					<div class="col-md-4">
						<div class="ce-feature-box-6 text-center">
							<div class="icon-plain-small center"><span
									class="icon-magnifying-glass text-primary"></span></div>
							<div class="text-box text-center">
								<div class="top-line"></div>
								<h5 class="title font-weight-5">Pixel Perfect</h5>
								<p>Amet dolor Vestibulum ante ipsum primis in faucibus.</p>
							</div>
						</div>
					</div>
					<!--end item-->

				</div>

			</div>
		</section>
		<div class=" clearfix"></div>
		<!--end section-->


		<section class="section-side-image section-primary sec-padding clearfix">
			<div class="img-holder col-md-12 col-sm-3 pull-left">
				<div class="background-imgholder" style="background:url(images/16.jpg);"><img class="nodisplay-image"
																							  src="images/16.jpg"
																							  alt=""/></div>
			</div>
			<div class="container">
				<div class="row">

					<div class="col-md-6">
						<img src="http://via.placeholder.com/550x700" alt="" class="img-responsive"/>
					</div>
					<!--end item-->


					<div class="col-md-6">

						<div class="ce-feature-box-12">
							<div class="col-sm-11 col-centered">
								<div class="sec-title-container text-left">
									<div class="ce-title-line white"></div>
									<div class="clearfix"></div>
									<h2 class="font-weight-6 less-mar-1 line-height-5 text-white">Our Multipage Template
										Can Really Help You to Chase Your Success</h2>
									<div class="col-md-9 nopadding"><h6 class="font-weight-3 text-white">Praesent mattis
											commodo augue Aliquam ornare hendrerit augue Cras tellus. </h6></div>
									<div class="clearfix"></div>
									<br/>

									<p class="text-white">Lorem ipsum dolor sit amet consectetuer adipiscing elit
										Suspendisse et justo Praesent mattis commodo augue Aliquam ornare hendrerit
										augue Cras tellus In pulvinar lectus a est Curabitur eget orci Cras laoreet
										ligula. Etiam sit amet dolor Vestibulum ante ipsum primis in faucibus</p>

									<p class="text-white">justo Praesent mattis commodo augue Aliquam ornare hendrerit
										augue Cras tellus In pulvinar lectus a est Curabitur eget orci Cras laoreet
										ligula. Etiam sit amet dolor Vestibulum ante ipsum primis in faucibus</p>

									<div class="clearfix"></div>
									<br/>
									<a class="btn btn-white uppercase" href="#"><i class="fa fa-play-circle"
																				   aria-hidden="true"></i> Get
										Started</a>

								</div>
							</div>
							<div class="clearfix"></div>
							<!--end title--></div>

					</div>
					<!--end item-->

				</div>
			</div>
		</section>
		<div class=" clearfix"></div>
		<!--end section-->


		<section class="sec-padding section-bgimg-3">
			<div class="container">
				<div class="row">

					<div class="col-sm-8 col-centered">
						<div class="sec-title-container text-left">
							<div class="ce-title-line"></div>
							<div class="clearfix"></div>
							<h2 class="font-weight-6 less-mar-1 line-height-5">Our Latest Works Can help You<br/>
								& Create Awesome Pages </h2>
							<div class="col-md-6 nopadding"><h6 class="font-weight-3">Praesent mattis commodo augue
									Aliquam ornare hendrerit augue Cras tellus. </h6></div>
						</div>
					</div>
					<div class="clearfix"></div>
					<!--end title-->


					<div id="js-filters-mosaic-flat" class="cbp-l-filters-buttonCenter round">
						<div data-filter="*" class="cbp-filter-item-active cbp-filter-item"> All
							<div class="cbp-filter-counter"></div>
						</div>
						<div data-filter=".print" class="cbp-filter-item"> Print
							<div class="cbp-filter-counter"></div>
						</div>
						<div data-filter=".web-design" class="cbp-filter-item"> Web Design
							<div class="cbp-filter-counter"></div>
						</div>
						<div data-filter=".graphic" class="cbp-filter-item"> Graphic
							<div class="cbp-filter-counter"></div>
						</div>
						<div data-filter=".motion" class="cbp-filter-item"> Motion
							<div class="cbp-filter-counter"></div>
						</div>
					</div>
					<div>
						<div id="js-grid-mosaic-flat" class="cbp cbp-l-grid-mosaic-flat">
							<div class="cbp-item web-design graphic"><a href="http://via.placeholder.com/1000x1000"
																		class="cbp-caption cbp-lightbox"
																		data-title="Bolt UI<br>by Tiberiu Neamu">
									<div class="cbp-caption-defaultWrap"><img src="http://via.placeholder.com/1000x1000"
																			  alt=""></div>
									<div class="cbp-caption-activeWrap">
										<div class="cbp-l-caption-alignCenter">
											<div class="cbp-l-caption-body">
												<div class="cbp-l-caption-title">Lorem ipsum dolor sit</div>
											</div>
										</div>
									</div>
								</a></div>
							<div class="cbp-item print motion"><a href="http://via.placeholder.com/1000x1000"
																  class="cbp-caption cbp-lightbox"
																  data-title="Bolt UI<br>by Tiberiu Neamu">
									<div class="cbp-caption-defaultWrap"><img src="http://via.placeholder.com/1000x1000"
																			  alt=""></div>
									<div class="cbp-caption-activeWrap">
										<div class="cbp-l-caption-alignCenter">
											<div class="cbp-l-caption-body">
												<div class="cbp-l-caption-title">Lorem ipsum dolor sit</div>
											</div>
										</div>
									</div>
								</a></div>
							<div class="cbp-item print motion"><a href="http://via.placeholder.com/1000x1000"
																  class="cbp-caption cbp-lightbox"
																  data-title="Bolt UI<br>by Tiberiu Neamu">
									<div class="cbp-caption-defaultWrap"><img src="http://via.placeholder.com/1000x1000"
																			  alt=""></div>
									<div class="cbp-caption-activeWrap">
										<div class="cbp-l-caption-alignCenter">
											<div class="cbp-l-caption-body">
												<div class="cbp-l-caption-title">Lorem ipsum dolor sit</div>
											</div>
										</div>
									</div>
								</a></div>
							<div class="cbp-item motion graphic"><a href="http://via.placeholder.com/1000x1000"
																	class="cbp-caption cbp-lightbox"
																	data-title="Bolt UI<br>by Tiberiu Neamu">
									<div class="cbp-caption-defaultWrap"><img src="http://via.placeholder.com/1000x1000"
																			  alt=""></div>
									<div class="cbp-caption-activeWrap">
										<div class="cbp-l-caption-alignCenter">
											<div class="cbp-l-caption-body">
												<div class="cbp-l-caption-title">Lorem ipsum dolor sit</div>
											</div>
										</div>
									</div>
								</a></div>
							<div class="cbp-item web-design print"><a href="http://via.placeholder.com/1000x1000"
																	  class="cbp-caption cbp-lightbox"
																	  data-title="Bolt UI<br>by Tiberiu Neamu">
									<div class="cbp-caption-defaultWrap"><img src="http://via.placeholder.com/1000x1000"
																			  alt=""></div>
									<div class="cbp-caption-activeWrap">
										<div class="cbp-l-caption-alignCenter">
											<div class="cbp-l-caption-body">
												<div class="cbp-l-caption-title">Lorem ipsum dolor sit</div>
											</div>
										</div>
									</div>
								</a></div>
							<div class="cbp-item print motion"><a href="http://via.placeholder.com/1000x1000"
																  class="cbp-caption cbp-lightbox"
																  data-title="Bolt UI<br>by Tiberiu Neamu">
									<div class="cbp-caption-defaultWrap"><img src="http://via.placeholder.com/1000x1000"
																			  alt=""></div>
									<div class="cbp-caption-activeWrap">
										<div class="cbp-l-caption-alignCenter">
											<div class="cbp-l-caption-body">
												<div class="cbp-l-caption-title">Lorem ipsum dolor sit</div>
											</div>
										</div>
									</div>
								</a></div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<div class="clearfix"></div>
		<!-- end section -->


		<section class="section-side-image section-light sec-padding clearfix">
			<div class="img-holder col-md-12 col-sm-12 pull-left">
				<div class="background-imgholder" style="background:url(images/5.jpg);"><img class="nodisplay-image"
																							 src="images/5.jpg" alt=""/>
				</div>
			</div>
			<div class="container">
				<div class="row">

					<div class="col-sm-8 col-centered">
						<div class="sec-title-container text-left">
							<div class="ce-title-line"></div>
							<div class="clearfix"></div>
							<h2 class="font-weight-6 less-mar-1 line-height-5">What We Offer For Successful<br/>
								Business and Features </h2>
							<div class="col-md-6 nopadding"><h6 class="font-weight-3">Praesent mattis commodo augue
									Aliquam ornare hendrerit augue Cras tellus. </h6></div>
						</div>
					</div>
					<div class="clearfix"></div>
					<!--end title-->


					<div class="col-md-4">
						<div class="ce-feature-box-3">
							<div class="img-box">
								<div class="text-box">
									<h5 class="title font-weight-5 title">Classic Style</h5>
									<p class="content">Vestibulum ante ipsum primis sit amet justo elit faucibus.</p>

								</div>
								<img src="http://via.placeholder.com/600x400" alt="" class="img-responsive"/>
							</div>
						</div>
					</div>
					<!--end item-->

					<div class="col-md-4">
						<div class="ce-feature-box-3">
							<div class="img-box">
								<div class="text-box">
									<h5 class="title font-weight-5 title">Flexible Design</h5>
									<p class="content">Vestibulum ante ipsum primis sit amet justo elit faucibus.</p>

								</div>
								<img src="http://via.placeholder.com/600x400" alt="" class="img-responsive"/>
							</div>
						</div>
					</div>
					<!--end item-->

					<div class="col-md-4">
						<div class="ce-feature-box-3">
							<div class="img-box">
								<div class="text-box">
									<h5 class="title font-weight-5 title">Clean Code</h5>
									<p class="content">Vestibulum ante ipsum primis sit amet justo elit faucibus.</p>

								</div>
								<img src="http://via.placeholder.com/600x400" alt="" class="img-responsive"/>
							</div>
						</div>
					</div>
					<!--end item-->


				</div>

			</div>
		</section>
		<div class=" clearfix"></div>
		<!--end section-->


		<div class="parallax vertical-align" data-parallax-bg-image="http://via.placeholder.com/2000x1300"
			 data-parallax-speed="0.9" data-parallax-direction="down">
			<div class="parallax-overlay bg-opacity-8">
				<div class="container sec-tpadding-2 sec-bpadding-2">
					<div class="row">

						<div class="col-sm-8 col-centered">
							<div class="sec-title-container text-left">
								<div class="ce-title-line"></div>
								<div class="clearfix"></div>
								<h2 class="font-weight-6 less-mar-1 line-height-5 text-white">Our Great Pricing
									Tables<br/>
									For Startups & Media </h2>
								<div class="col-md-6 nopadding"><h6 class="font-weight-3 text-white">Praesent mattis
										commodo augue Aliquam ornare hendrerit augue Cras tellus. </h6></div>
							</div>
						</div>
						<div class="clearfix"></div>
						<!--end title-->


					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<!-- end section -->

		<section class="section-light">
			<div class="container">
				<div class="row">

					<div class="ce-feature-box-1-main">
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="ce-feature-box-1 margin-bottom">
								<div class="border-box">
									<div class="inner-box">
										<h3 class="less-mar-1 font-weight-5 title">Basic</h3>
										<br/>
										<div class="price-circle">
											<div class="price"><sup>$</sup>19</div>
											<i>/mo</i></div>
										<br/>
										<ul class="plan_features">
											<li><i class="fa fa-check" aria-hidden="true"></i> &nbsp; 5GB Space</li>
											<li class="highlight"><i class="fa fa-check" aria-hidden="true"></i> &nbsp;
												2 Domains
											</li>
											<li><i class="fa fa-check" aria-hidden="true"></i> &nbsp; 5 mail Accounts
											</li>
											<li><span><i class="fa fa-times" aria-hidden="true"></i></span> &nbsp; Live
												Support
											</li>
										</ul>
										<div class="clearfix"></div>
										<a class="btn btn-prim btn-xround primary uppercase" href="#"><i
												class="fa fa-play-circle" aria-hidden="true"></i> Order Now !</a></div>
								</div>
							</div>
						</div>
						<!--end item-->

						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="ce-feature-box-1 active margin-bottom">
								<div class="border-box">
									<div class="inner-box">
										<h3 class="less-mar-1 font-weight-5 title">Standard</h3>
										<br/>
										<div class="price-circle">
											<div class="price"><sup>$</sup>29</div>
											<i>/mo</i></div>
										<br/>
										<ul class="plan_features">
											<li><i class="fa fa-check" aria-hidden="true"></i> &nbsp; 30GB Space</li>
											<li class="highlight"><i class="fa fa-check" aria-hidden="true"></i> &nbsp;
												10 Domains
											</li>
											<li><i class="fa fa-check" aria-hidden="true"></i> &nbsp; 20 mail Accounts
											</li>
											<li><span><i class="fa fa-times" aria-hidden="true"></i></span> &nbsp; Live
												Support
											</li>
										</ul>
										<div class="clearfix"></div>
										<a class="btn btn-prim btn-xround primary uppercase" href="#"><i
												class="fa fa-play-circle" aria-hidden="true"></i> Order Now !</a></div>
								</div>
							</div>
						</div>
						<!--end item-->

						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="ce-feature-box-1 margin-bottom">
								<div class="border-box">
									<div class="inner-box">
										<h3 class="less-mar-1 font-weight-5 title">Premium</h3>
										<br/>
										<div class="price-circle">
											<div class="price"><sup>$</sup>99</div>
											<i>/mo</i></div>
										<br/>
										<ul class="plan_features">
											<li><i class="fa fa-check" aria-hidden="true"></i> &nbsp; Unlimited Space
											</li>
											<li class="highlight"><i class="fa fa-check" aria-hidden="true"></i> &nbsp;
												40 Domains
											</li>
											<li><i class="fa fa-check" aria-hidden="true"></i> &nbsp; 50 mail Accounts
											</li>
											<li><i class="fa fa-check" aria-hidden="true"></i> &nbsp; Live Support</li>
										</ul>
										<div class="clearfix"></div>
										<a class="btn btn-prim btn-xround primary uppercase" href="#"><i
												class="fa fa-play-circle" aria-hidden="true"></i> Order Now !</a></div>
								</div>
							</div>
						</div>
						<!--end item-->
					</div>
				</div>
			</div>
		</section>
		<div class="clearfix"></div>
		<!-- end section -->


		<section class="sec-padding">
			<div class="container">
				<div class="row">
					<div class="col-sm-8 col-centered">
						<div class="sec-title-container text-left">
							<div class="ce-title-line"></div>
							<div class="clearfix"></div>
							<h2 class="font-weight-6 less-mar-1 line-height-5">Meet Our Creative Team <br/>
								Give You Best Services </h2>
							<div class="col-md-6 nopadding"><h6 class="font-weight-3">Praesent mattis commodo augue
									Aliquam ornare hendrerit augue Cras tellus. </h6></div>
						</div>
					</div>
					<div class="clearfix"></div>
					<!--end title-->


					<div class="col-md-3 col-sm-6 col-xs-12 margin-bottom">
						<div class="ce-feature-box-2 border">
							<div class="main-box">
								<div class="img-box">
									<div class="overlay">
										<p class="small-text text-center">Lorem ipsum dolor sit amet, consectetuer
											adipiscing elit. Suspendisse et justo. </p>
										<br/>
										<ul class="sc-icons">
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
											<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
									<img src="http://via.placeholder.com/550x700" alt="" class="img-responsive"/></div>
								<div class="text-box text-center">
									<h5 class="nopadding title">Alexander</h5>
									<p class="subtext">Founder and CEO</p>
								</div>
							</div>
						</div>
						<!--end feature box-->

					</div>
					<!--end item-->

					<div class="col-md-3 col-sm-6 col-xs-12 margin-bottom">
						<div class="ce-feature-box-2 border">
							<div class="main-box">
								<div class="img-box">
									<div class="overlay">
										<p class="small-text text-center">Lorem ipsum dolor sit amet, consectetuer
											adipiscing elit. Suspendisse et justo. </p>
										<br/>
										<ul class="sc-icons">
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
											<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
									<img src="http://via.placeholder.com/550x700" alt="" class="img-responsive"/></div>
								<div class="text-box text-center">
									<h5 class="nopadding title">Sharlene</h5>
									<p class="subtext">UI Developer</p>
								</div>
							</div>
						</div>
						<!--end feature box-->

					</div>
					<!--end item-->

					<div class="col-md-3 col-sm-6 col-xs-12 margin-bottom">
						<div class="ce-feature-box-2 border">
							<div class="main-box">
								<div class="img-box">
									<div class="overlay">
										<p class="small-text text-center">Lorem ipsum dolor sit amet, consectetuer
											adipiscing elit. Suspendisse et justo. </p>
										<br/>
										<ul class="sc-icons">
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
											<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
									<img src="http://via.placeholder.com/550x700" alt="" class="img-responsive"/></div>
								<div class="text-box text-center">
									<h5 class="nopadding title">Westley</h5>
									<p class="subtext">Manager</p>
								</div>
							</div>
						</div>
						<!--end feature box-->

					</div>
					<!--end item-->

					<div class="col-md-3 col-sm-6 col-xs-12 margin-bottom">
						<div class="ce-feature-box-2 border">
							<div class="main-box">
								<div class="img-box">
									<div class="overlay">
										<p class="small-text text-center">Lorem ipsum dolor sit amet, consectetuer
											adipiscing elit. Suspendisse et justo. </p>
										<br/>
										<ul class="sc-icons">
											<li><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
											<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
									<img src="http://via.placeholder.com/550x700" alt="" class="img-responsive"/></div>
								<div class="text-box text-center">
									<h5 class="nopadding title">Maggy</h5>
									<p class="subtext">Support</p>
								</div>
							</div>
						</div>
						<!--end feature box-->

					</div>
					<!--end item-->

				</div>

			</div>
		</section>
		<div class=" clearfix"></div>
		<!--end section-->


		<section class="section-side-image section-dark sec-padding clearfix">
			<div class="img-holder col-md-12 col-sm-12 pull-left">
				<div class="background-imgholder" style="background:url(images/7.jpg);"><img class="nodisplay-image"
																							 src="images/7.jpg" alt=""/>
				</div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-md-11 col-centered text-center slide-controls-2">
						<div id="owl-demo3" class="owl-carousel owl-theme">
							<div class="item">
								<h2 class="text-white font-weight-4 line-height-5">Vestibulum ante ipsum primis in
									Faucibus orci Luctus et Ultrices Posuere Cubilia Curae Nullam diam.</h2>
								<br/>
								<div class="clearfix"></div>
								<div class="imgbox-medium round center overflow-hidden"><img
										src="http://via.placeholder.com/200x200" alt="" class="img-responsive"/></div>

								<h5 class="text-white less-mar-1">Alexander</h5>
								<p class="text-primary">Designer</p>
							</div>
							<!--end carousel item-->

							<div class="item">
								<h2 class="text-white font-weight-4 line-height-5">Vestibulum ante ipsum primis in
									Faucibus orci Luctus et Ultrices Posuere Cubilia Curae Nullam diam.</h2>
								<br/>
								<div class="clearfix"></div>
								<div class="imgbox-medium round center overflow-hidden"><img
										src="http://via.placeholder.com/200x200" alt="" class="img-responsive"/></div>

								<h5 class="text-white less-mar-1">Sharlene</h5>
								<p class="text-primary">Support</p>
							</div>
							<!--end carousel item-->

							<div class="item">
								<h2 class="text-white font-weight-4 line-height-5">Vestibulum ante ipsum primis in
									Faucibus orci Luctus et Ultrices Posuere Cubilia Curae Nullam diam.</h2>
								<br/>
								<div class="clearfix"></div>
								<div class="imgbox-medium round center overflow-hidden"><img
										src="http://via.placeholder.com/200x200" alt="" class="img-responsive"/></div>

								<h5 class="text-white less-mar-1">Westley</h5>
								<p class="text-primary">Manager</p>
							</div>
							<!--end carousel item-->

						</div>
						<!--end carousel-->
					</div>
					<!--end item-->

				</div>

			</div>
		</section>
		<div class=" clearfix"></div>
		<!--end section-->


		<section class="sec-padding">
			<div class="container">
				<div class="row">

					<div class="col-sm-8 col-centered">
						<div class="sec-title-container text-left">
							<div class="ce-title-line"></div>
							<div class="clearfix"></div>
							<h2 class="font-weight-6 less-mar-1 line-height-5">Our Latest Blogs and Features<br/>
								Create Your Own Posts </h2>
							<div class="col-md-6 nopadding"><h6 class="font-weight-3">Praesent mattis commodo augue
									Aliquam ornare hendrerit augue Cras tellus. </h6></div>
						</div>
					</div>
					<div class="clearfix"></div>
					<!--end title-->


					<div class="col-md-6">
						<div class="ce-feature-box-4">
							<div class="img-box">
								<div class="date-box text-center"><span>25</span><br/> Jan</div>
								<div class="text-box">
									<h5 class="title font-weight-5 title"><a href="#">09 Tips on How to Create Ui
											Design</a></h5>

									<div class="blog-post-info"><span><i class="fa fa-user"></i> By Westley</span>
										<span><i class="fa fa-comments-o"></i> 20 Comments</span></div>
									<br/>
									<p class="content">Pulvinar lectus a est Curabitur eget orci Cras laoreet ligula
										Etiam sit amet dolor Vestibulum ante ipsum primis in faucibus orci luctus et
										ultrices posuere </p>

								</div>
								<img src="http://via.placeholder.com/700x500" alt="" class="img-responsive"/>
							</div>
						</div>
					</div>
					<!--end item-->


					<div class="col-md-6">
						<div class="ce-feature-box-4">
							<div class="img-box">
								<div class="date-box text-center"><span>27</span><br/> Jan</div>
								<div class="text-box">
									<h5 class="title font-weight-5 title"> Mockups & prototypes of Design</h5>

									<div class="blog-post-info"><span><i class="fa fa-user"></i> By Westley</span>
										<span><i class="fa fa-comments-o"></i> 20 Comments</span></div>
									<br/>
									<p class="content">Pulvinar lectus a est Curabitur eget orci Cras laoreet ligula
										Etiam sit amet dolor Vestibulum ante ipsum primis in faucibus orci luctus et
										ultrices posuere </p>

								</div>
								<img src="http://via.placeholder.com/700x500" alt="" class="img-responsive"/>
							</div>
						</div>
					</div>
					<!--end item-->

				</div>
			</div>
		</section>
		<div class="clearfix"></div>
		<!-- end section -->


		<section class="sec-padding-6 section-primary">
			<div class="container">
				<div class="row">
					<div id="owl-demo11" class="owl-carousel owl-theme">
						<div class="item">
							<ul class="clients-list grid-cols-5 hover-7 noborder">
								<li><a href="#"><img src="http://via.placeholder.com/225x120" alt=""></a></li>
								<li><a href="#"><img src="http://via.placeholder.com/225x120" alt=""></a></li>
								<li><a href="#"><img src="http://via.placeholder.com/225x120" alt=""></a></li>
								<li><a href="#"><img src="http://via.placeholder.com/225x120" alt=""></a></li>
								<li><a href="#"><img src="http://via.placeholder.com/225x120" alt=""></a></li>
							</ul>
						</div>
						<!--end carousel item-->

						<div class="item">
							<ul class="clients-list grid-cols-5 hover-7 noborder">
								<li><a href="#"><img src="http://via.placeholder.com/225x120" alt=""></a></li>
								<li><a href="#"><img src="http://via.placeholder.com/225x120" alt=""></a></li>
								<li><a href="#"><img src="http://via.placeholder.com/225x120" alt=""></a></li>
								<li><a href="#"><img src="http://via.placeholder.com/225x120" alt=""></a></li>
								<li><a href="#"><img src="http://via.placeholder.com/225x120" alt=""></a></li>
							</ul>
						</div>
						<!--end carousel item-->

					</div>
					<!--end carousel-->

				</div>
			</div>
		</section>
		<div class="clearfix"></div>
		<!-- end section -->

		<div class="section-dark sec-padding">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-12 colmargin clearfix margin-bottom">
						<div class="fo-map">
							<div class="footer-logo"><img src="images/logo/logo.png" alt=""/></div>
							<p class="text-light">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Suspendisse
								et justo. Praesent mattis commodo </p>
							<p class="text-light">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Suspendisse
								et justo. Praesent mattis commodo </p>
						</div>
					</div>
					<!--end item-->

					<div class="col-md-3 col-xs-12 clearfix margin-bottom">
						<h4 class="text-white less-mar3 font-weight-5">Company</h4>
						<div class="clearfix"></div>
						<br/>
						<ul class="footer-quick-links-4 white">
							<li><a href="#"><i class="fa fa-angle-right"></i> About Company</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i> Our Success Storys</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i> Our Work Process for Business</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i> How we Help you to Success</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i> About Our Happy Customers</a></li>
						</ul>
					</div>
					<!--end item-->

					<div class="col-md-3 col-xs-12 clearfix margin-bottom">
						<h4 class="text-white less-mar3 font-weight-5">Useful Links</h4>
						<div class="clearfix"></div>
						<br/>
						<ul class="footer-quick-links-4 white">
							<li><a href="#"><i class="fa fa-angle-right"></i> Startup Business Ideas</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i> Branding Icon Design</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i> Marketing Business Plans</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i> Financial Services</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i> Best Business Support </a></li>
						</ul>
					</div>
					<!--end item-->

					<div class="col-md-3 col-xs-12 clearfix margin-bottom">
						<h4 class="text-white less-mar3 font-weight-5">Get in Touch</h4>
						<div class="clearfix"></div>
						<br/>
						<address class="text-light">

							2877 Short Street, New York, NY 7510 <br>
							ipsum City, Country
						</address>
						<span class="text-light"> 406-822-6469</span><br>
						<span class="text-light"> support@domain.com </span><br>
						<span class="text-light"> 406-382-9305</span>
						<ul class="footer-social-icons white left-align icons-plain text-center">
							<li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a class="active" href="#"><i class="fa fa-google-plus"></i></a></li>
							<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
							<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
						</ul>
					</div>
					<!--end item-->

				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<!-- end section -->

		<section class="sec-padding-6 section-medium-dark">
			<div class="container">
				<div class="row">
					<div class="fo-copyright-holder text-center"> Copyright © 2019 l yourdomain.com. All rights
						reserved.
					</div>
				</div>
			</div>
		</section>
		<div class="clearfix"></div>
		<!-- end section -->

		<a href="#" class="scrollup"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
		<!-- end scroll to top of the page-->

	</div>
	<!--end site wrapper-->
</div>
<!--end wrapper boxed-->

<!-- Scripts -->
<script src="js/jquery/jquery.js"></script>
<script src="js/bootstrap/bootstrap.min.js"></script>
<script src="js/less/less.min.js" data-env="development"></script>
<!-- Scripts END -->

<!-- Template scripts -->
<script src="js/megamenu/js/main.js"></script>
<script type="text/javascript" src="js/ytplayer/jquery.mb.YTPlayer.js"></script>
<script type="text/javascript" src="js/ytplayer/elementvideo-custom.js"></script>
<script type="text/javascript" src="js/ytplayer/play-pause-btn.js"></script>
<!-- REVOLUTION JS FILES -->
<script type="text/javascript" src="js/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>
<!-- SLIDER REVOLUTION 5.0 EXTENSIONS
(Load Extensions only on Local File Systems !
The following part can be removed on Server for On Demand Loading) -->
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.carousel.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript"
		src="js/revolution-slider/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="js/revolution-slider/js/extensions/revolution.extension.video.min.js"></script>
<script type="text/javascript">
    var tpj = jQuery;
    var revapi4;
    tpj(document).ready(function () {
        if (tpj("#rev_slider").revolution == undefined) {
            revslider_showDoubleJqueryError("#rev_slider");
        } else {
            revapi4 = tpj("#rev_slider").show().revolution({
                sliderType: "standard",
                jsFileLocation: "js/revolution-slider/js/",
                sliderLayout: "auto",
                dottedOverlay: "none",
                delay: 9000,
                navigation: {
                    keyboardNavigation: "off",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation: "off",
                    onHoverStop: "off",
                    arrows: {
                        style: "uranus",
                        enable: true,
                        hide_onmobile: false,
                        hide_under: 100,
                        hide_onleave: true,
                        hide_delay: 200,
                        hide_delay_mobile: 1200,
                        tmp: '',
                        left: {
                            h_align: "left",
                            v_align: "center",
                            h_offset: 80,
                            v_offset: 0
                        },
                        right: {
                            h_align: "right",
                            v_align: "center",
                            h_offset: 80,
                            v_offset: 0
                        }
                    }
                    ,
                    touch: {
                        touchenabled: "on",
                        swipe_threshold: 75,
                        swipe_min_touches: 1,
                        swipe_direction: "horizontal",
                        drag_block_vertical: false
                    }
                    ,


                },
                viewPort: {
                    enable: true,
                    outof: "pause",
                    visible_area: "80%"
                },

                responsiveLevels: [1240, 1024, 778, 480],
                gridwidth: [1240, 1024, 778, 480],
                gridheight: [880, 730, 600, 550],
                lazyType: "smart",
                parallax: {
                    type: "mouse",
                    origo: "slidercenter",
                    speed: 2000,
                    levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50],
                },
                shadow: 0,
                spinner: "off",
                stopLoop: "off",
                stopAfterLoops: -1,
                stopAtSlide: -1,
                shuffle: "off",
                autoHeight: "off",
                hideThumbsOnMobile: "off",
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                disableProgressBar: "on",
                debugMode: false,
                fallbacks: {
                    simplifyAll: "off",
                    nextSlideOnWindowFocus: "off",
                    disableFocusListener: false,
                }
            });
        }
    });	/*ready*/
</script>

<script src="js/loaders/loading-custom.js"></script>
<script src="js/parallax/parallax-background.min.js"></script>
<script src="js/parallax/parallax-custom.js"></script>
<script src="js/tabs/js/responsive-tabs.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/cubeportfolio/jquery.cubeportfolio.min.js"></script>
<script type="text/javascript" src="js/cubeportfolio/main-mosaic3-cols3.js"></script>
<script src="js/owl-carousel/owl.carousel.js"></script>
<script src="js/owl-carousel/custom.js"></script>
<script src="js/owl-carousel/owl.carousel.js"></script>
<script src="js/accordion/js/smk-accordion.js"></script>
<script src="js/accordion/js/custom.js"></script>
<script src="js/progress-circle/raphael-min.js"></script>
<script src="js/progress-circle/custom.js"></script>
<script src="js/progress-circle/jQuery.circleProgressBar.js"></script>
<script src="js/functions/functions.js"></script>
</body>
</html>
