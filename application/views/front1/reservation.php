<section>
	<div class="pagenation-holder">
		<div class="container">
			<div class="row">
				<div class="col-md-6"><h1 class="uppercase">Reservation</h1></div>
				<div class="col-md-6">
					<ol class="breadcrumb">
						<li class="current"><a href="<?php echo base_url() ?>">Home</a></li>
						<li class="current"><a href="<?php echo base_url() ?>">Services</a></li>
						<li class="current"><a
								href="<?php echo $this->main->permalink(array($tour->title)) ?>"><?php echo $tour->title ?></a>
						</li>
						<li>Reservation</li>
					</ol>
				</div>

			</div>
		</div>
	</div>
</section>
<div class="clearfix"></div>

<section class="sec-padding" style="padding-top: 20px">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-9">
				<div class="form-body1 bg-light1">

					<form action="<?php echo site_url('reservation-send') ?>" method="post" class="form-send" style="z-index: 1000000">

						<input type="hidden" name="id_tour" value="<?php echo $tour->id ?>">
						<div class="row">
							<div class="col-md-12">
								<label class="lable-text" for="name"> Title</label>
								<select class="form-control" name="title">
									<option value="Mr.">Mr.</option>
									<option value="Mrs.">Mrs.</option>
									<option value="Ms.">Ms.</option>
								</select>
							</div>
							<div class="col-md-12">
								<label class="lable-text" for="name"> First Name</label>
								<input id="name" class="form-control" type="text" name="first_name">
								<span class="help-block error"></span>
							</div>
							<div class="col-md-12">
								<label class="lable-text" for="name"> Last Name</label>
								<input id="name" class="form-control" type="text" name="last_name">
							</div>
							<div class="col-md-12">
								<label class="lable-text" for="email"> Email</label>
								<input id="email" class="form-control" type="text" name="email">
							</div>
							<div class="col-md-12">
								<label class="lable-text" for="phone">Phone Number</label>
								<input id="phone" class="form-control" type="text" name="phone">
							</div>
							<div class="col-md-12">
								<label class="lable-text" for="phone">Address</label>
								<input id="phone" class="form-control" type="text" name="address">
							</div>
							<div class="col-md-12">
								<label class="lable-text" for="phone">Country</label>
								<select class="form-control" name="country">
									<?php require "components/country.php" ?>
								</select>
							</div>
							<div class="col-md-12">
								<label class="lable-text" for="phone">Tour Start</label>
								<input type='text' class="form-control" id='datetimepicker41'
									   placeholder="MM/DD/YYYY" name="tour_start"
									   value="<?php echo date('d-m-Y') ?>"/>
							</div>
							<div class="col-md-12">
								<label class="lable-text" for="phone">Total Adult</label>
								<input id="phone" class="form-control" type="number" name="total_adult" value="1" min="1">
							</div>
							<div class="col-md-12">
								<label class="lable-text" for="phone">Total Children</label>
								<input id="phone" class="form-control" type="number" name="total_children" value="0" min="0">
							</div>
							<div class="col-md-12">
								<label class="lable-text" for="message">Message</label>
								<textarea id="comment" class="form-control" name="message"></textarea>
							</div>
							<div class="col-md-12">
								<label class="lable-text" for="message">Security Code</label>
								<br />
								<?php echo $captcha ?>
								<br /><br />
								<input id="phone" class="form-control" type="text" name="captcha" style="width: 200px">
							</div>

							<div class="col-md-12 text-center">
								<br/>
								<button type="submit" class="btn btn-medium btn-orange btn-anim-1 uppercase xround-4">
									<i class="fa fa-check" aria-hidden="true"></i>
									<span>Send Reservation</span>
								</button>
							</div>
						</div>
					</form>
						<!-- end row -->
				</div>
			</div>
			<div class="col-xs-12 col-sm-3">
				<div class="sidebar col_last nobottommargin">
					<address>
						<strong>Office Address:</strong><br>
						<?php echo $alamat ?>
					</address>
					<abbr title="Phone Number"><strong>Telephone:</strong></abbr> <a
						href="telp:<?php echo $telephone ?>"><?php echo $telephone ?></a><br>
					<abbr title="Phone Number"><strong>Phone:</strong></abbr> <a
						href="telp:<?php echo $phone ?>"><?php echo $phone ?> </a><br>
					<abbr title="Phone Number"><strong>WhatsApp:</strong></abbr> <a href="<?php echo $whatsapp_link ?>"
																					target="_blank"><?php echo $whatsapp ?></a><br>
					<abbr title="Phone Number"><strong>WeChat:</strong></abbr> <a href="<?php echo $wechat_link ?>"
																				  target="_blank"><?php echo $wechat_id ?></a><br>
					<abbr title="Email Address"><strong>Email:</strong></abbr> <a
						href="mailto:<?php echo $email ?>"><?php echo $email ?></a>
					<br /><br />
					<div class="widget noborder notoppadding">
						<div id="s-icons" class="widget quick-contact-widget clearfix">
							<h4 class="highlight-me nopadding">Connect Socially</h4>
							<ul class="footer-social-icons blue left-align icons-plain nopadding text-center">
								<li><a href="<?php echo $facebook_link ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
								<li><a href="<?php echo $instagram_link ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
								<li><a href="<?php echo $line_link ?>" target="_blank"><img src="<?php echo base_url('assets/front/images/line.png') ?>"></a></li>
								<li><a href="<?php echo $whatsapp_link ?>" target="_blank"><i class="fa fa-whatsapp"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="clearfix"></div>


<link rel="stylesheet" href="<?php echo base_url() ?>assets/front/js/datepicker/bootstrap-datetimepicker.css"/>
<script src="<?php echo base_url() ?>assets/front/js/datepicker/moment.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/datepicker/bootstrap-datetimepicker.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/vendors/general/sweetalert2/dist/sweetalert2.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/vendors/general/sweetalert2/dist/sweetalert2.min.css">
<script type="text/javascript">
    $(function () {
        $('#datetimepicker4').datetimepicker();
    });
</script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/front/js/app.js"></script>


