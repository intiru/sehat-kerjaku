<div class="sec-padding section-dark">
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-xs-12 clearfix margin-bottom">
				<div class="footer-logo">
					<img src="<?php echo $this->main->image_preview_url('harsa-bali-holiday-logo-light-white.png') ?>">
					<h2><?php echo $this->main->web_name() ?></h2>
				</div>
				<span class="text-light"> <?php echo $telephone ?></span><br>
				<span class="text-light"> <?php echo $phone ?></span><br>
				<span class="text-light"> <?php echo $email ?> </span><br>
				<span class="text-light"> <?php echo $alamat ?> </span><br>
				<ul class="footer-social-icons white left-align icons-plain text-center">
					<li><a href="<?php echo $facebook_link ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
					<li><a href="<?php echo $instagram_link ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
					<li><a href="<?php echo $line_link ?>" target="_blank"><img src="<?php echo base_url('assets/front/images/line.png') ?>"></a></li>
					<li><a href="<?php echo $whatsapp_link ?>" target="_blank"><i class="fa fa-whatsapp"></i></a></li>
				</ul>
			</div>
			<div class="col-md-7 col-xs-12 clearfix margin-bottom">
				<h4 class="text-white less-mar3 font-weight-5">Menu</h4>
				<div class="clearfix"></div>
				<br/>
				<ul class="footer-quick-links-4 white">
					<li><a href="<?php echo base_url() ?>"><i class="fa fa-angle-right"></i> Home</a></li>

					<?php foreach ($services_list as $r) { ?>
						<li><a href="<?php echo $this->main->permalink(array($r->title)) ?>"><i class="fa fa-angle-right"></i> <?php echo $r->title ?></a></li>
					<?php } ?>
					<li><a href="<?php echo base_url() ?>gallery-photo"><i class="fa fa-angle-right"></i> Gallery Photo</a></li>
					<li><a href="<?php echo base_url() ?>contact-us"><i class="fa fa-angle-right"></i> Contact Us</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>

<section class="sec-padding-6 section-medium-dark">
	<div class="container">
		<div class="row">
			<div class="fo-copyright-holder text-center"> Copyright © <?php echo date('Y') ?> Development by <a
					href="https://intiru.com" target="_blank">Intiru.com</a></div>
		</div>
	</div>
</section>
<div class="clearfix"></div>


<a href="#" class="scrollup"><i class="fa fa-angle-up" aria-hidden="true"></i></a>
