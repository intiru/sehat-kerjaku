<div class="col-xs-12 app-top">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-1 app-logo-wrapper">
				<a href="<?php echo site_url() ?>">
					<img src="<?php echo base_url() ?>upload/images/harsa-bali-holiday-logo-light-white.png">
				</a>
			</div>
			<div class="col-xs-12 col-md-7 app-name-wrapper">
				<a href="<?php echo site_url() ?>">
					<div class="app-web-name"><?php echo $this->main->web_name() ?>
					</div>
				</a>
				<div class="app-web-name-sub">MAKE YOUR HOLIDAY MORE MEMORABLE</div>
			</div>
			<div class="col-xs-12 col-md-4 app-top-contact">
				<a href="mailto:<?php echo $email ?>"><?php echo $email ?></a>
				<span class="app-top-phone"><?php echo $phone ?></span>
				<ul class="footer-social-icons blue left-align icons-plain nopadding text-center">
					<li><a href="<?php echo $facebook_link ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
					<li><a href="<?php echo $instagram_link ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
					<li><a href="<?php echo $line_link ?>" target="_blank"><img src="<?php echo base_url('assets/front/images/line.png') ?>"></a></li>
					<li><a href="<?php echo $whatsapp_link ?>" target="_blank"><i class="fa fa-whatsapp"></i></a></li>
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="col-md-12 nopadding">
	<div class="header-section white light-dropdowns style1 links-dark top-menu-wrapper">
		<div class="container">
			<div class="mod-menu">
				<div class="row">
					<div class="col-sm-2 app-mobile-app-name">
						<a href="<?php echo site_url() ?>" title="" class="logo style-2 mar-4">
							Harsa Bali Holiday
						</a>
					</div>
					<div class="col-sm-12">
						<div class="main-nav">
							<ul class="nav navbar-nav top-nav">
								<li class="visible-xs menu-icon">
									<a href="javascript:void(0)"
									   class="navbar-toggle collapsed"
									   data-toggle="collapse"
									   data-target="#menu"
									   aria-expanded="false">
										<i aria-hidden="true"
										   class="fa fa-bars"></i>
									</a>
								</li>
							</ul>
							<div id="menu" class="collapse front-menu">
								<ul class="nav navbar-nav">
									<li class="<?php echo $page->type == 'home' ? 'active' : '' ?>">
										<a href="<?php echo site_url() ?>">Home</a>
									</li>

									<?php foreach ($services_list as $r) { ?>
										<li class="<?php echo $page->id == $r->id ? 'active' : '' ?>">
											<a href="<?php echo $this->main->permalink(array($r->title)) ?>"><?php echo $r->title ?></a>
										</li>
									<?php } ?>
									<li class="<?php echo $page->type == 'gallery_photo' ? 'active' : '' ?>">
										<a href="<?php echo site_url('gallery-photo') ?>">Gallery Photo</a>
									</li>
									<li class="<?php echo $page->type == 'contact_us' ? 'active' : '' ?>">
										<a href="<?php echo site_url('contact-us') ?>">Contact Us</a>
									</li>
									<li>
										<a href="#">
											<img src="<?php echo $this->main->image_preview_url($lang_active->thumbnail) ?>" height="20" alt="<?php echo $lang_active->title ?>"> <?php echo $lang_active->title ?>
										</a>
										<span class="arrow"></span>
										<ul class="dm-align-2">
											<?php foreach ($language_list as $r) { ?>
												<li>
													<?php echo anchor($this->lang->switch_uri($r->code), '<img
															src="'.$this->main->image_preview_url($r->thumbnail).'"
															height="20"
															alt="'.$r->title.'"> '.$r->title) ?>
												</li>
											<?php } ?>
										</ul>
									</li>
									<li>
										<a id="google_translate_element"></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>


<script type="text/javascript">
    function googleTranslateElementInit() {
        new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
    }
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
