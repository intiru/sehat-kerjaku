<section>
	<div class="pagenation-holder">
		<div class="container">
			<div class="row">
				<div class="col-md-6"><h1 class="uppercase">Gallery Photo</h1></div>
				<div class="col-md-6">
					<ol class="breadcrumb">
						<li><a href="#">Home</a></li>
						<li><a href="#">Blog</a></li>
						<li class="current"><a href="#">Blog Full Width</a></li>
					</ol>
				</div>

			</div>
		</div>
	</div>
</section>
<div class="clearfix"></div>


<section class="sec-padding">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-centered">
				<div class="sec-title-container">
					<div class="ce-title-line"></div>
					<div class="clearfix"></div>
					<div class="col-md-12 nopadding">
						<?php echo $page->description ?>
					</div>
				</div>
			</div>
		</div>

		<div class="row popup-gallery gallery-wrapper">
			<?php foreach($gallery as $r) { ?>
			<div class="col-md-3">
				<a href="<?php echo $this->main->image_preview_url($r->thumbnail) ?>" title="<?php echo $r->title ?>">
					<img src="<?php echo $this->main->image_preview_url($r->thumbnail) ?>" alt="<?php echo $r->thumbnail_alt ?>" class="img-responsive" >
				</a>
			</div>
			<?php } ?>
		</div>

	</div>
</section>


<link rel="stylesheet" href="<?php echo base_url() ?>assets/front/js/magnific-popup/magnific-popup.css">
<script src="<?php echo base_url() ?>assets/front/js/magnific-popup/jquery.magnific-popup.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/magnific-popup/magpop-custom.js"></script>
<script src="js/less/less.min.js" data-env="development"></script>




