<section>
	<div class="pagenation-holder">
		<div class="container">
			<div class="row">
				<div class="col-md-6"><h1 class="uppercase"><?php echo $page->title ?></h1></div>
				<div class="col-md-6">
					<ol class="breadcrumb">
						<li class="current"><a href="#">Home</a></li>
						<li class="current"><a href="#">Services</a></li>
						<li><?php echo $page->title ?></li>
					</ol>
				</div>

			</div>
		</div>
	</div>
</section>
<div class="clearfix"></div>

<section class="sec-padding" style="padding-top: 20px">
	<div class="container">

		<div id="owl-demo8" class="owl-carousel owl-theme popup-gallery">
			<?php foreach ($gallery as $r) { ?>
				<div class="item">
					<a href="<?php echo $this->main->image_preview_url($r->thumbnail) ?>"
					   title="<?php echo $r->thumbnail_alt ?>">
						<img src="<?php echo $this->main->image_preview_url($r->thumbnail) ?>"
							 alt="<?php echo $r->thumbnail_alt ?>"
							 width="100%">
					</a>
				</div>
			<?php } ?>
		</div>
		<br/>


		<div class="row tour-wrapper">
			<div class="col-xs-12 col-sm-9">
				<div class="text-box" style="padding: 20px;">
					<h5 class="title font-weight-5 title"><a href="#"><?php echo $page->title ?></a>
					</h5>

					<div class="blog-post-info">
						<span><i class="fa fa-user"></i> Administrator</span>
						<span><i
								class="fa fa-calendar"></i> <?php echo $this->main->date_view($page->created_at) ?></span>
					</div>
					<br/>
					<?php echo $page->description ?>

					<br/>
					<div class="text-center">
						<a class="btn btn-medium btn-orange btn-anim-1 uppercase xround-4"
						   href="<?php echo $this->main->permalink(array('reservation', $page->title)) ?>">
							<i class="fa fa-check" aria-hidden="true"></i>
							<span>Book Now</span>
						</a>
					</div>
					<br/>

				</div>
			</div>
			<div class="col-xs-12 col-sm-3">
				<h2 class="font-size-6">Other Service Related</h2>
				<?php foreach ($related as $r) { ?>
					<a href="<?php echo $this->main->permalink(array($r->title)) ?>" style="padding: 10px 0;">
						<img src="<?php echo $this->main->image_preview_url($r->thumbnail) ?>"
							 alt="<?php echo $r->thumbnail_alt ?>" class="img-responsive"
							 style="margin-bottom: 10px">
						<h4 class="font-size-4 font-weight-1"><?php echo $r->title ?></h4>
					</a>
					<hr/>
				<?php } ?>
			</div>
		</div>
	</div>
</section>
<div class="clearfix"></div>


<link href="<?php echo base_url() ?>assets/front/js/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="<?php echo base_url() ?>assets/front/js/owl-carousel/owl.theme.css" rel="stylesheet">

<link rel="stylesheet" href="<?php echo base_url() ?>assets/front/js/magnific-popup/magnific-popup.css">


<script src="<?php echo base_url() ?>assets/front/js/owl-carousel/owl.carousel.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/owl-carousel/custom.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/owl-carousel/owl.carousel.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/owl-carousel/custom.js"></script>

<script src="<?php echo base_url() ?>assets/front/js/magnific-popup/jquery.magnific-popup.js"></script>
<script src="<?php echo base_url() ?>assets/front/js/magnific-popup/magpop-custom.js"></script>
<script src="js/less/less.min.js" data-env="development"></script>


