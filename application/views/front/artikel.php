<div class="blog-home-page">
    <div class="blog-hero-home">
        <div class="blog-home-text">
            <h1><?php echo $page->title ?></h1>
            <p><?php echo $page->title_sub ?></p>
        </div>
    </div>
    <div class="container">
        <div class="main-blog-list">
            <div class="row">
                <div class="col-xl-12">
                    <div class="section-heading-3">
                        <?php echo $page->description ?>
                    </div>
                </div>
            </div>
            <div class="row blog-list">
                <div class="col-sm-12 blog-category">
                    <h4>Kategori</h4>
                    <?php foreach($category as $row) { ?>
                        <a href="<?php echo $this->main->permalink(array('artikel', $row->title)) ?>" class="float-left text-center <?php echo $row->id == $id_blog_category ? 'active':'' ?>">
                            <img src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>" class="circle-icon">
                            <br />
                            <?php echo $row->title ?><br />
                            <span style="color: rgba(0,0,0,0.4); font-size: 13px;">(<?php echo $row->blog_count ?> artikel)</span>
                        </a>
                    <?php } ?>
                </div>
                <div class="col-sm-12">
                    <br />
                    <h4>Daftar Artikel</h4>
                </div>
                <?php foreach ($blog as $row) { ?>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                        <a href="<?php echo $this->main->permalink(array('artikel', $row->title)) ?>"
                           class="home-single-blog">
                            <div class="s-blog-image">
                                <img src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>" alt="">
                                <div class="blog-post-date">
                                    <span><?php echo $this->main->date_format_view($row->created_at) ?></span>
                                </div>
                                <div class="blog-post-category">
                                    <span><?php echo $row->blog_category_title ?></span>
                                </div>
                                <div class="blog-post-views">
                                    <span><?php echo 'Views : '.number_format($row->views) ?></span>
                                </div>
                            </div>
                            <div class="s-blog-content">
                                <h4><?php echo $row->title ?></h4>
                                <p><?php echo substr(strip_tags($row->description), 0, 150) ?> ... </p>
                                <a href="<?php echo $this->main->permalink(array('artikel', $row->title)) ?>"><i
                                            class="fa fa-chevron-circle-right"></i> Baca Selengkapnya</a>
                            </div>
                        </a>
                    </div>
                <?php } ?>
            </div>
            <div class="col-xl-12">
                <div class="next-previous-page">
                    <?php echo $this->pagination->create_links(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
