<div class="about-hero-banner">
    <div class="about-hero-text">
        <h1><?php echo $page->title ?></h1>
        <p><?php echo $page->title_sub ?></p>
    </div>
</div>

<div class="about-counter-area">
    <div class="container">
        <div class="row">
            <!-- section heading text-->
            <div class="col-xl-12 d-flex align-items-center">
                <div class="section-heading-1 about-counter-text">
                    <?php echo $page->description ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="team-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="section-heading-3">
                    <h4>Personil</h4>
                    <h3>Tim Sehat Kerjaku</h3>
                </div>
            </div>
        </div>
        <div class="row profile-team-wrapper">
            <?php foreach ($team as $row) { ?>
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                    <a href="<?php echo $this->main->permalink(array($row->title)) ?>" class="single-team">
                        <div class="team-image">
                            <img src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>"
                                 alt="<?php echo $row->title ?>" title="<?php echo $row->title ?>">
                            <div class="team-content">
                                <button class="btn btn-success">Detail Profil</button>
                            </div>
                        </div>
                        <div class="person-name text-center">
                            <h4><?php echo $row->title ?></h4>
                            <p><?php echo $row->position ?></p>

                        </div>
                    </a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>