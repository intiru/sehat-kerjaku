<div class="portfolio-hero-banner">
    <div class="portfolio-hero-text">
        <br />
        <h1><?php echo $page->title ?></h1>
        <p><?php echo $page->title_sub ?></p>
    </div>
</div>
<div class="portfolio-details">
    <div class="container">
        <div class="portfolio-details-box">
            <div class="row">
                <div class="col-xs-12">

                </div>
                <div class="col-xl-5 col-lg-5 col-md-12">
                    <img src="<?php echo base_url('assets/template_front/img/services-detail.png') ?>" class="img-responsive" style="width: 100% !important;" />
                </div>
                <div class="col-xl-7 col-lg-7 col-md-12">
                    <div class="project-description">
                        <br />
                        <h3>Description</h3>
                        <?php echo $page->description ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
