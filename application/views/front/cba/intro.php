<html lang="id">
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/template_front/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/template_front/css/cba.css') ?>">

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="container cba-wrapper">
    <div class="row">
        <div class="col-xs-12 text-center">
            <br/>
            <br/>
            <img src="<?php echo base_url('assets/template_front/img/cba_1.png') ?>" width="100%">
            <br/>
            <br/>
            <h2>Cost of Benefit Analysis</h2>
            <p>
                Cost of Benefit Analysis merupakan proses perbandingan biaya yang diperkirakan dengan manfaat yang
                berkaita erat dengan pembuatan keputusan, untuk menentukan apakah keputusan yang akan dibuat tersebut
                masuk akan atau tidak dari perspektif bisnis
            </p>
            <br />
            <a href="" class="btn btn-warning btn-lg">Mulai Analisis</a>
        </div>
    </div>
</div>
</body>
</html>