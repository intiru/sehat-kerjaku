<div class="contact-hero-banner">
    <div class="contact-banner-text">
        <h1><?php echo $page->title ?></h1>
        <p><?php echo $page->title_sub ?></p>
    </div>
</div>
<div class="contactus-area">
    <div class="container">
        <div class="single-contact-area">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                    <div class="single-contact-info">
                        <div class="info-icon">
                            <i class="far fa-envelope"></i>
                        </div>
                        <div class="info-content">
                            <h6>Alamat Email</h6>
                            <span><a href="mailto:sehatkerjaku@gmail.com"
                                     target="_blank">sehatkerjaku@gmail.com</a></span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 margin-top-sb-30">
                    <div class="single-contact-info">
                        <div class="info-icon">
                            <i class="fas fa-mobile-alt"></i>
                        </div>
                        <div class="info-content">
                            <h6>No Telepon</h6>
                            <span><a href="tel:081239785261" target="_blank">0812 3978 5261</a></span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6 margin-top-sb-30">
                    <div class="single-contact-info large-mb-d">
                        <div class="info-icon">
                            <i class="fas fa-map-marker-alt"></i>
                        </div>
                        <div class="info-content">
                            <h6>Alamat Kantor</h6>
                            <span><a href="https://goo.gl/maps/fAp3Bzo5bv4FE8E57" target="_blank">Laplapan, Ubud, Gianyar, Bali</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div class="contact-form-area">
                    <div class="contact-left-bg">
                        <img src="<?php echo base_url() ?>assets/template_front/img/contact-p-2.png" alt="">
                    </div>
                    <div class="contact-form-heading">
                        <h3>Kontak Kami</h3>
                    </div>
                    <div class="contact-form">
                        <form action="<?php echo site_url('contact_us/send') ?>" method="post" class="form-send">
                            <input type="text" name="name" placeholder="Nama Anda">
                            <input type="text" name="phone" placeholder="No Telepon Anda">
                            <input type="text" name="email" placeholder="Alamat Email Anda">
                            <textarea name="message" placeholder="Pesan yang akan dikirimkan"></textarea>
                            <br /><br />
                            <?php echo $captcha ?>
                            <input type="text" name="captcha" placeholder="Kode Keamanan">
                            <div class="send-btn">
                                <input type="submit" value="Kirim Pesan ke Sehat Kerjaku" id="formsend">
                            </div>
                        </form>
                    </div>
                    <div class="contact-right-bg">
                        <img src="<?php echo base_url() ?>assets/template_front/img/contact-p-1.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class='container-loading hide'>
    <div class='loader'>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--text'></div>
        <div class='loader--desc'></div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url('assets/template_front/js/vendor/jquery-2.2.4.min.js') ?>"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.form-send').submit(function (e) {
            e.preventDefault();
            $('.container-loading').hide().removeClass('hide').fadeIn('fast');
            $.ajax({
                url: $(this).attr('action'),
                type: $(this).attr('method'),
                data: $(this).serialize(),
                success: function (response) {
                    $('.container-loading').fadeOut('fast').addClass('hide');
                    var data = JSON.parse(response);
                    $('input, textarea').removeClass('form-error');

                    if(data.status === 'success') {
                        Swal.fire({
                            title: 'Sukses',
                            text: 'Pesan Anda sudah terkirim ke Team Sehat Kerjaku, Terima Kasih',
                            icon: 'success',
                            confirmButtonText: 'Baik'
                        }).then(function (result) {
                            if (result.value) {
                                window.location.reload();
                            }
                        });;
                    } else {
                        Swal.fire({
                            title: 'Perhatian',
                            text: 'Form belum lengkap terisi',
                            icon: 'error',
                            confirmButtonText: 'Baik'
                        });

                        $.each(data.errors  , function(field, desc) {
                            if(desc) {
                                $('[name="'+field+'"]').addClass('form-error');
                            }
                        });
                    }
                }
            });

            return false;
        });
    });
</script>