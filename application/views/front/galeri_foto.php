<div class="about-hero-banner">
    <div class="about-hero-text">
        <h1><?php echo $page->title ?></h1>
        <p><?php echo $page->title_sub ?></p>
    </div>
</div>
<section id="about">
    <div class="abou-us-area">
        <div class="container">
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12">
                    <!-- about brief-->
                    <div class="about-content">
                        <div class="about-section-title">
                            <h4>Galeri Foto, Sehat Kerjaku</h4>
                        </div>
                        <p>Penjelasan singkat tentang galeri foto sehat kerjaku, khususnya mengenai pengantar
                            tentang layanan yang disediakan. There are many use variations of passages of Lorem Ipsum
                            available, but the have suffered alteration in some form, by injected humour, or randomised
                            words which don't look even slightly. If you are going to use a passage of Lorem Ipsum, you
                            need to be sure there .</p>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
            <div class="row gallery-list grid">
                <?php foreach ($gallery as $row) { ?>
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 grid-item">
                        <a data-fancybox="gallery" href="<?php echo $this->main->image_preview_url($row->thumbnail) ?>">
                            <img src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>"
                                 class="img-responsive"
                                 title="<?php echo $row->title ?>"
                                 alt="<?php echo $row->title_sub ?>"
                                 style="width: 100% !important;">
                        </a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>