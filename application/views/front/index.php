<!doctype html>
<html lang="id">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $page->meta_title ?></title>
    <meta name="keywords" content="<?php echo $page->meta_keywords ?>"/>
    <meta name="description" content="<?php echo $page->meta_description ?>">
    <meta name="author" content="<?php echo $author ?>">
    <meta name="revisit-after" content="2 days"/>
    <meta name="robots" content="index, follow"/>
    <meta name="rating" content="General"/>
    <meta http-equiv="charset" content="ISO-8859-1"/>
    <meta http-equiv="content-language" content="indonesia"/>
    <meta name="MSSmartTagsPreventParsing" content="true"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="<?php echo base_url() ?>assets/template_front/img/favicon.png" type="image/png">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css">
    <link rel="stylesheet" href="<?php echo base_url('assets/template_front/css/sehatkerjaku.min.css') ?>">
    <link href="https://fonts.googleapis.com/css?family=Nunito:400,700,900&amp;display=swap" rel="stylesheet">
    <script data-ad-client="ca-pub-5547726169857687" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>

<body>
<?php if(in_array($page->type, array('blog','profile'))) { ?>
    <img src="<?php echo $this->main->image_preview_url($page->thumbnail) ?>" style="display: none" alt="<?php echo $page->title ?>">
<?php } else { ?>
    <img src="<?php echo base_url() ?>assets/template_front/img/logo-sehat-kerjaku.png" style="display: none" alt="Logo Sehat Kerjaku">
<?php } ?>
<?php echo $menu ?>
<?php echo $content ?>
<?php echo $footer ?>

<script src="<?php echo base_url('js/sehatkerjaku.min.js') ?>"></script>
<script src="https://apis.google.com/js/client.js"> </script>
<script async src="https://www.googletagmanager.com/gtag/js?id=G-NGF2FXV26N"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-NGF2FXV26N');
</script>
</body>

</html>