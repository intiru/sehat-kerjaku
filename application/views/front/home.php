<section>
    <div class="header-content-area">
        <div class="container">
            <div class="header-pattern-1">
                <img src="<?php echo base_url() ?>assets/template_front/img/header-pattern-1.png" alt="">
            </div>
            <div class="header-pattern-2">
                <img src="<?php echo base_url() ?>assets/template_front/img/header-pattern2.png" alt="">
            </div>
            <div class="header-animation-area">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12">
                        <div class="intro-text">
                            <h1 data-aos="slide-right" data-aos-anchor-placement="top-bottom" data-aos-delay="700"
                                data-aos-duration="1200"><?php echo $sesi_1->title ?></h1>
                            <p data-aos="fade-in" data-aos-anchor-placement="top-bottom" data-aos-delay="1500"
                               data-aos-duration="1200">
                                <?php echo $sesi_1->description ?>
                            </p>
                            <div class="learnmore" data-aos="fade-in" data-aos-anchor-placement="top-bottom"
                                 data-aos-delay="1500" data-aos-duration="1200">
                                <a href="<?php echo     site_url('kontak-kami') ?>" class="skill-btn"><i class="fa fa-phone"></i> Kontak Kami</a>

                                <div class="learnmore2">
                                    <a href="https://www.youtube.com/watch?v=koF1CfzguGE" class="skill-btn-youtube popup-youtube"><i class="far fa-play-circle"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="header-1-artwork">
                        <div class="layer">
                            <img src="<?php echo base_url() ?>assets/template_front/img/construction-workers.jpg" alt=""
                                 style="width: 700px; margin-right: -200px">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="home-about-area">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="home-about-image">
                        <img src="<?php echo base_url() ?>assets/template_front/img/home-about.jpg" alt="">
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="section-heading-1 home-about-text">
                        <h4><?php echo $sesi_2->title_sub ?></h4>
                        <h3><?php echo $sesi_2->title ?></h3>
                        <?php echo $sesi_2->description ?>
                        <div class="section-button">
                            <a href="<?php echo site_url('profil-kami') ?>"><i class="fa fa-search"></i> Info Selengkapnya</a>
                        </div>
                        <div class="e-pattern">
                            <img src="<?php echo base_url() ?>assets/template_front/img/expertise/e-pattern.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="services-area">
        <svg id="curve" data-name="layer 1" xmlns="http://www.w3.org/2000/svg" viewbox="0 0 1416.99 174.01">
            <path class="cls-1" d="M0,280.8S283.66,59,608.94,163.56s437.93,150.57,808,10.34V309.54H0V280.8Z"
                  transform="translate(0 -135.53)"/>
        </svg>
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-5 d-flex align-items-center">
                    <div class="section-heading-2 section-heading-2-p">
                        <h4><?php echo $sesi_3->title_sub ?></h4>
                        <h3><?php echo $sesi_3->title ?></h3>
                        <?php echo $sesi_3->description ?>
                        <div class="section-button">
                            <a href="<?php echo site_url('layanan-kami') ?>"><i class="fa fa-search"></i> Info Selengkapnya</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-6 col-md-7">
                    <div class="row">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 d-flex align-items-center">
                            <div class="row">
                                <?php foreach($services_left as $row) { ?>
                                <a href="<?php echo $this->main->permalink(array('layanan', $row->title)) ?>" class="col-xl-12">
                                    <div class="single-service service-mt-30 aos-init aos-animate" data-aos="fadein" data-aos-anchor-placement="top-bottom" data-aos-delay="300" data-aos-duration="1000">
                                        <div class="service-icon">
                                            <img src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>" alt="<?php echo $row->title_sub ?>" title="<?php echo $title ?>">
                                        </div>
                                        <div class="service-text">
                                            <h3><?php echo $row->title ?></h3>
                                            <p><?php echo $row->title_sub ?></p>
                                        </div>
                                    </div>
                                </a>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 margin-top-sb-30">
                            <div class="row">
                                <?php foreach($services_right as $row) { ?>
                                    <a href="<?php echo $this->main->permalink(array('layanan', $row->title)) ?>" class="col-xl-12">
                                        <div class="single-service service-mt-30 aos-init aos-animate" data-aos="fadein" data-aos-anchor-placement="top-bottom" data-aos-delay="300" data-aos-duration="1000">
                                            <div class="service-icon">
                                                <img src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>" alt="<?php echo $row->title_sub ?>" title="<?php echo $title ?>">
                                            </div>
                                            <div class="service-text">
                                                <h3><?php echo $row->title ?></h3>
                                                <p><?php echo $row->title_sub ?></p>
                                            </div>
                                        </div>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section>
    <div class="home-blog-area">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="section-heading-3">
                        <h4>Artikel Sehat Kerjaku</h4>
                        <h3>Artikel yang terakhir</h3>
                    </div>
                </div>
            </div>
            <div class="row">

                <?php foreach ($blog_related as $row) { ?>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                        <a href="<?php echo $this->main->permalink(array('artikel', $row->title)) ?>"
                           class="home-single-blog">
                            <div class="s-blog-image">
                                <img src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>" alt="">
                                <div class="blog-post-date">
                                    <span><?php echo $this->main->date_format_view($row->created_at) ?></span>
                                </div>
                                <div class="blog-post-category">
                                    <span><?php echo $row->blog_category_title ?></span>
                                </div>
                                <div class="blog-post-views">
                                    <span><?php echo 'Views : '.number_format($row->views) ?></span>
                                </div>
                            </div>
                            <div class="s-blog-content">
                                <h4><?php echo $row->title ?></h4>
                                <p><?php echo substr(strip_tags($row->description), 0, 150) ?> ... </p>
                                <a href="<?php echo $this->main->permalink(array('artikel', $row->title)) ?>"><i
                                            class="fa fa-chevron-circle-right"></i> Baca Selengkapnya</a>
                            </div>
                        </a>
                    </div>
                <?php } ?>
            </div>
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div class="section-button text-center">
                        <br />
                        <a href="<?php echo site_url('artikel') ?>">
                            <i class="fa fa-search"></i> Lihat Semua Artikel Kita</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>