<br/><br/>
<br/><br/>
<br/>
<div class="blog-body">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12">
                <div class="left-side">
                    <div class="blog-post-heading">
                        <h1><?php echo $page->title ?></h1>
                        <span class="publishe-date"><i
                                    class="fa fa-calendar"></i> Tanggal Publikasi : <?php echo $this->main->date_format_view($page->created_at) ?></span>
                        &nbsp;&nbsp;&nbsp;
                        <span class="publishe-date"><i class="fa fa-address-card"></i> Kategori : <a
                                    href="<?php echo $this->main->permalink(array('artikel', $page->blog_category_title)) ?>"><?php echo $page->blog_category_title ?></a></span>
                        &nbsp;&nbsp;&nbsp;
                        <span class="publishe-date"><i
                                    class="fa fa-eye"></i> Views : </span><?php echo number_format($page->views) ?>
                    </div>
                    <div class="blog-body-content">
                        <div class="text-center">
                            <img src="<?php echo $this->main->image_preview_url($page->thumbnail) ?>" class="thumbnail"
                                 alt="<?php echo $page->title ?>">
                        </div>
                        <div class="artikel-description">
                            <?php echo $page->description ?>
                        </div>

                        <br/>
                        <br/>
                        <h6><strong>Share ke Sosial Media : </strong></h6>
                        <a href="<?php echo $this->main->share_link('facebook', $page->title, $artikel_permalink) ?>"
                           class="fab fa-facebook"></a>
                        <a href="<?php echo $this->main->share_link('twitter', $page->title, $artikel_permalink) ?>"
                           class="fab fa-twitter"></a>
                        <a href="<?php echo $this->main->share_link('googleplus', $page->title, $artikel_permalink) ?>"
                           class="fab fa-google"></a>
                        <a href="<?php echo $this->main->share_link('linkedin', $page->title, $artikel_permalink) ?>"
                           class="fab fa-linkedin"></a>

                    </div>
                    <div id="disqus_thread"></div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                <div class="right-side">
                    <div class="blog-search">
                        <form action="<?php echo site_url('artikel') ?>" method="get">
                            <input type="search" id="blog-search" name="search" placeholder="Pencarian Artikel">
                            <button type="submit" class="blog-search-icon-small"><i class="fas fa-search"></i></button>

                        </form>
                    </div>
                    <div class="author-profile">
                        <h3>Tentang Penulis</h3>
                        <div class="author-content text-center">
                            <a href="<?php echo $this->main->permalink(array($page->team_title)) ?>">
                                <img src="<?php echo $this->main->image_preview_url($page->team_thumbnail) ?>" alt="">
                                <h4><?php echo $page->team_title ?></h4>
                                <span class="designation"><?php echo $page->team_position ?></span>
                            </a>
                        </div>
                    </div>
                    <div class="main-category">
                        <h3>Kategori Artikel</h3>
                        <div class="category-list">
                            <ul>
                                <?php foreach ($category as $row) { ?>
                                    <li>
                                        <a href="<?php echo $this->main->permalink(array('artikel', $row->title)) ?>"><?php echo $row->title ?>
                                            <span>(<?php echo $row->blog_count ?>)</span></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<section>
    <div class="home-blog-area">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="section-heading-3">
                        <h4>Artikel Sehat Kerjaku</h4>
                        <h3>Artikel yang terakhir</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php foreach ($blog_related as $row) { ?>
                    <div class="col-xl-4 col-lg-4 col-md-4 col-sm-6">
                        <a href="<?php echo $this->main->permalink(array('artikel', $row->title)) ?>"
                           class="home-single-blog">
                            <div class="s-blog-image">
                                <img src="<?php echo $this->main->image_preview_url($row->thumbnail) ?>" alt="">
                                <div class="blog-post-date">
                                    <span><?php echo $this->main->date_format_view($row->created_at) ?></span>
                                </div>
                                <div class="blog-post-category">
                                    <span><?php echo $row->blog_category_title ?></span>
                                </div>
                                <div class="blog-post-views">
                                    <span><?php echo 'Views : ' . number_format($row->views) ?></span>
                                </div>
                            </div>
                            <div class="s-blog-content">
                                <h4><?php echo $row->title ?></h4>
                                <p><?php echo substr(strip_tags($row->description), 0, 150) ?> ... </p>
                                <a href="<?php echo $this->main->permalink(array('artikel', $row->title)) ?>"><i
                                            class="fa fa-chevron-circle-right"></i> Baca Selengkapnya</a>
                            </div>
                        </a>
                    </div>
                <?php } ?>
            </div>
            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div class="section-button text-center">
                        <br/>
                        <a href="<?php echo site_url('artikel') ?>">
                            <i class="fa fa-search"></i> Lihat Semua Artikel Kita</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    (function () {
        var d = document, s = d.createElement('script');
        s.src = 'https://sehat-kerjaku.disqus.com/embed.js';
        s.setAttribute('data-timestamp', +new Date());
        (d.head || d.body).appendChild(s);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by
        Disqus.</a></noscript>