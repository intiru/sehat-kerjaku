/*
 Navicat Premium Data Transfer

 Source Server         : 1. localhost
 Source Server Type    : MySQL
 Source Server Version : 100137
 Source Host           : localhost:3306
 Source Schema         : intiru_sehat_kerjaku

 Target Server Type    : MySQL
 Target Server Version : 100137
 File Encoding         : 65001

 Date: 08/01/2020 12:19:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for about
-- ----------------------------
DROP TABLE IF EXISTS `about`;
CREATE TABLE `about` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of about
-- ----------------------------
BEGIN;
INSERT INTO `about` VALUES (1, 'wardana', '<p>sdfsdfsdf</p>', 'wardana', 'wardana', 'wardana');
COMMIT;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of admin
-- ----------------------------
BEGIN;
INSERT INTO `admin` VALUES (2, 'mahendrawardana', '6001c26274f43ac7c6b2be2662a027f6', 'mahendra.adi.wardana@gmail.com', 'Mahendra Wardana');
INSERT INTO `admin` VALUES (32, 'etick', '6c0d2d7387b524b5a342b87b7ec87c6c', 'etick@gmail.com', 'etick');
COMMIT;

-- ----------------------------
-- Table structure for blog
-- ----------------------------
DROP TABLE IF EXISTS `blog`;
CREATE TABLE `blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_team` int(11) DEFAULT NULL,
  `id_blog_category` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `thumbnail` text,
  `thumbnail_alt` text,
  `description` text NOT NULL,
  `use` enum('yes','no') DEFAULT 'no',
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of blog
-- ----------------------------
BEGIN;
INSERT INTO `blog` VALUES (21, 15, 13, 'Waspada Penyakit Leptospirosis Saat Banjir Serta Pencegahannya', 'waspada-penyakit-leptospirosis-saat-banjir-serta-pencegahannya7.jpg', '12', '<p style=\"text-align: justify;\">Hujan deras yang mengguyur sebagian wilah di Indonesia mengakibatkan beberapa wilayah mengalami banjir. Hingga kini sejumlah wilayah masih tergenang air. Banjir membawa dampak besar termasuk kesehatan. Kementerian Kesehatan</p>', 'yes', 'Waspada Penyakit Leptospirosis Saat Banjir Serta Pencegahannya', 'Waspada Penyakit Leptospirosis Saat Banjir Serta Pencegahannya', 'Waspada Penyakit Leptospirosis Saat Banjir Serta Pencegahannya', '2020-01-01 11:21:54');
INSERT INTO `blog` VALUES (22, 15, 7, 'Waspada Sambaran Petir di Musim Hujan', 'waspada-sambaran-petir-di-musim-hujan.jpg', 'Waspada Sambaran Petir di Musim Hujan', '<p style=\"text-align: justify;\">Musim hujan mulai memasuki wilayah Indonesia, selain angin ketika musim hujan juga akan muncul kilat dan petir. Petir adalah fenomena alam berupa cahaya kilat yang muncul karena terjadi tarik menarik antara muatan positif dan muatan negatif. Ada beberapa hal yang harus dilakukan agar aman ketika petir menyambar saat kalian sedang di luar maupun di dalam ruangan<br /><br />Saat berada di luar ruangan hindarilah bidang terbuka, puncak bukit/gunung. Jauhi pohon tinggi. Jika kalian sedang berada dalam satu kelompok menyebarlah. Jauhi lah air, tali, logam seperti pagar dan tiang karena arus dari kilat akan dengan mudah mengalirkan arus dengan jarak yang jauh.<br /><br />Saat berada dalam ruangan jauhilah telepon yang terhubung dengan kabel. Jangan menyentuh peralatan listrik seperti komputer, TV, atau kabel. Hindari air, tundalah sebentar untuk tidak mencuci tangan, mandi atau mencuci piring. Jauhi jendela dan pintu yang mungkin memiliki kebocoran kecil di sekitar sisinya untuk membiarkan masuknya cahaya, dan jauhi teras. Jangan berbaring di lantai beton atau bersandar pada dinding beton. Lindungi hewan peliharaan kalian. Rumah anjing bukan tempat berlindung yang aman. Anjing yang dirantai ke pohon atau benda logam yang terpasang sangat rentan terhadap sambaran petir.<br /><br />Petir menghasilkan lonjakan listrik yang dapat merusak peralatan elektronik. Pelindung lonjakan tidak akan melindungi peralatan dari sambaran petir. Jangan mencabut peralatan saat terjadi hujan petir karena ada risiko kalian bisa tersengat.</p>', 'yes', 'Waspada Sambaran Petir di Musim Hujan', 'Waspada Sambaran Petir di Musim Hujan', 'Waspada Sambaran Petir di Musim Hujan', '2020-01-02 11:22:00');
INSERT INTO `blog` VALUES (23, 15, 7, 'Penuhi Hak Pekerja Untuk Mendapatkan Perlakukan yang Layak di Tempat Kerja', 'penuhi-hak-pekerja-untuk-mendapatkan-perlakukan-yang-layak-di-tempat-kerja.jpg', 'Penuhi Hak Pekerja Untuk Mendapatkan Perlakukan yang Layak di Tempat Kerja', '<p style=\"text-align: justify;\">\"UU no.1 tahun 1970 menyatakan adalah hak pekerja untuk mendapatkan k3 di tempat kerjanya. Aman. Sehat. Istirahat jika lelah adalah salah satu hak yg dimiliki oleh pekerja. So, ternyata Asi tidak hanya baik untuk bayi, ASI versi sehat kerjaku baik juga untuk pekerja..</p>', 'yes', 'Penuhi Hak Pekerja Untuk Mendapatkan Perlakukan yang Layak di Tempat Kerja', 'Penuhi Hak Pekerja Untuk Mendapatkan Perlakukan yang Layak di Tempat Kerja', 'Penuhi Hak Pekerja Untuk Mendapatkan Perlakukan yang Layak di Tempat Kerja', '2020-01-03 11:22:04');
INSERT INTO `blog` VALUES (24, 15, 7, 'Keselamata Saat Berangkat Kerja', 'keselamata-saat-berangkat-kerja.jpg', 'Keselamata Saat Berangkat Kerja', '<p style=\"text-align: justify;\">Jangan terburu-buru dalam bekerja, selow aja... Karena yang terburu-buru bisa membahayakan kalian. Selow&nbsp; aja asal selamat .</p>', 'yes', 'Keselamata Saat Berangkat Kerja', 'Keselamata Saat Berangkat Kerja', 'Keselamata Saat Berangkat Kerja', '2020-01-04 11:22:11');
INSERT INTO `blog` VALUES (25, 15, 7, 'Tips Keselamatan Saat Berkunjung ke Taman Hiburan Bagi Wisatawan', 'tips-keselamatan-saat-berkunjung-ke-taman-hiburan-bagi-wisatawan.jpg', 'Tips Keselamatan Saat Berkunjung ke Taman Hiburan Bagi Wisatawan', '<p style=\"text-align: justify;\">Bermain di taman hiburan menyajikan sensai tersendiri bagi pengunjungnya, saat mencoba wahana-wahana permainan extreme. Selain itu taman hiburan juga menyajikan beragam wahana untuk segala umur yang menjadikan taman hiburan menjadi salah satu pilihan keluarga dalam mengisi waktu libur. Taman hiburan memiliki sejumlah langkah-langkah keselamatan untuk memastikan bahwa waktu bersama keluarga kalian di sana menyenangkan dan bebas dari insiden.<br /><br />Sebelum pergi ke taman hiburan, tinjau tata letak taman secara online maupun offline lewat brosur dan buat rencana tentang wahana dan atraksi apa yang akan Anda kunjungi. Melakukan hal itu tidak hanya akan memanfaatkan waktu Anda di taman dengan lebih baik, tetapi juga akan membantu kalian dan keluarga tetap aman dengan memastikan Anda mengetahui lokasi masuk / keluar, toilet, tempat makan dan minum, dan bilik informasi. Pastikan anak-anak tidak berkeliaran di daerah terlarang. Ada alasan mengapa area tertentu dari taman dibatasi untuk karyawan. Bawalah Kit P3K portable yang lengkap.Sebelum pergi ke taman hiburan, oleskan tabir surya secara merata kekulit dan pastikan untuk sering menggunakannya kembali, Ingat, keringat akan mengurangi perlindungan tabir surya dan sadarlah bahwa anak-anak kecil memiliki kulit yang sensitif.<br /><br />Baca dan ikuti peraturan yang ada. Tanda-tanda yang dipasanga dan peraturan yang dibuat oleh pengelola taman hiburan tidak akan meredam kesenangan Anda, justru mereka dibuat untuk menjaga kalian tetap aman. Jadi bacalah, ikuti mereka dan bersenang-senanglah.<br /><br />Sangat mudah untuk terjebak dalam kegembiraan, antara membeli suvenir dan menikmati sensasi perjalanan, tetapi yang lebih penting kalian harus meluangkan waktu untuk istirahat makan dan minum. Dehidrasi bisa berlangsung sangat cepat, jadi minumlah air sepanjang hari dan isi ulang tubuh kalian dengan camilan sehat.</p>', 'yes', 'Tips Keselamatan Saat Berkunjung ke Taman Hiburan Bagi Wisatawan', 'Tips Keselamatan Saat Berkunjung ke Taman Hiburan Bagi Wisatawan', 'Tips Keselamatan Saat Berkunjung ke Taman Hiburan Bagi Wisatawan', '2020-01-05 11:22:18');
INSERT INTO `blog` VALUES (26, 15, 7, 'Tips Dasar Keselamata di Kebun Binatang Bagi Wisatawan', 'tips-dasar-keselamata-di-kebun-binatang-bagi-wisatawan.jpg', 'Tips Dasar Keselamata di Kebun Binatang Bagi Wisatawan', '<p style=\"text-align: justify;\">Berkunjung ke kebun binatang bisa menjadi pengalaman tersendiri bagi kalian teruntuk kalian yang sudah berkeluarga. Kebun Binatang bisa menjadi sarana edukasi bagi anak-anak serta bisa juga menjadi tempat wisata. Sangat penting untuk memiliki panduan keselamatan kebun binatang sehingga hari-hari kalian di kebun binatang menyenangkan, bahagia, dan aman.<br /><br />Pastikan untuk memakai alas kaki yang nyaman. Bawalah botol air isi ulang untuk digunakan di stasiun isi ulang air di Kebun Binatang. Jangan buang sampah sembarangan. Penting juga untuk diingat bahwa meskipun hewan kebun binatang dipamerkan, mereka harus dihormati karena ini ada rumah untuk binatang. Jangan berteriak atau melempar sesuatu kepada binatang. Jangan menggedor kaca/sekata pepembatas untuk menarik perhatian binatang.Suara-suara keras dan mengganggu juga bisa membuat hewan di kebun binatang stres, yang bisa mencegah mereka makan, bermain, atau terlibat dalam perilaku normal lainnya.<br /><br />Jika kalian membawa anak-anak awasilah mereka. Jika kalian melihat seseorang dalam kondisi bahaya, segeralah hubungi staff kebun binatang. Jangan mengambil risiko, berikan orang yang capable untuk menanganinya. Patuhilah tanda-tanda yang tertera dalam kebun binatang. Mengikuti langkah-langkah dengan baik akan membantu kalian tetap aman di Kebun Binatang.</p>', 'yes', 'Tips Dasar Keselamata di Kebun Binatang Bagi Wisatawan', 'Tips Dasar Keselamata di Kebun Binatang Bagi Wisatawan', 'Tips Dasar Keselamata di Kebun Binatang Bagi Wisatawan', '2020-01-06 11:22:23');
INSERT INTO `blog` VALUES (27, 15, 7, 'Tips Keselamatan Saat Mendaki Gunung Bagi Pendaki Pemula', 'tips-keselamatan-saat-mendaki-gunung-bagi-pendaki-pemula.jpg', 'Tips Keselamatan Saat Mendaki Gunung Bagi Pendaki Pemula', '<p style=\"text-align: justify;\">Apakah kalian berencana mengisi liburan tahun baru ini untuk pergi mendaki gunung untuk pertama kalinya sobat? tentunya mengasyikkan, pemandangan menakjubkan akan kita lihat dan perjalanan menakjubkan juga akan kita alami. Tetapi pada saat yang sama, ini bisa jadi berbahaya karena kurangnya pengalaman kita. Jadi, persiapkan rencana kalian dengan matang ya sobat. Mulailah sekarang dengan membaca tips keselamatan pendakian gunung ini untuk pemula.<br /><br />Mendaki gunung adalah kegiatan fisik yang cukup berat, jadi kalian harus bugar sebelum melakukan perjalanan hiking. Lakukan latihan yang akan memperkuat otot-otot kaki kalian sehingga bisa bertahan berjalan lama. Berlatihlah mengenakan tas punggung yang berat saat berolahraga karena itu akan menjadi simulasi dari apa yang akan kita rasakan ketika mendaki gunung. Konsumsilah makanan yang kaya karbohidrat beberapa hari sebelum pendakian agar tubuh kalian lebih berenergi.<br /><br />Ketika memulai pendakian bagi pemula, tentu saja, disarankan agar kalian memulai dengan jejak yang tidak terlalu sulit. Jangan tergoda untuk langsung melakukan lonjakan besar, tidak perlu terburu-buru. Perhatikan cuaca saat anda akan mendaki, Jika ramalan cuaca buruk, sebaiknya tunda perjalanan agar kalian aman. Pelajarilah track yang akan dilalui apakah miring, melewati sungai, hutan lebat, dll.<br />Pilihlah pakaian yang nyaman serbaguna dan ringan (tetapi tidak terlalu tipis), dan pastikan mereka mampu menutupi kulit Anda dengan cukup baik untuk melindungi diri dari goresan atau cuaca misalnya kaos lengan panjang, kalian dapat melepasnya atau menggulung lengannya jika panas, celana panjang/pendek serta kaos kaki panjang untuk melindungi kaki Anda, dan sarung tangan yang menutupi jari Anda untuk melindungi diri jika perlu berpegang pada batu. gunakan sepatu yang nyaman untuk berjam-jam berjalan, tangguh untuk digunakan mendaki, serta aman untuk melindungi kaki kalian.</p>', 'yes', 'Tips Keselamatan Saat Mendaki Gunung Bagi Pendaki Pemula', 'Tips Keselamatan Saat Mendaki Gunung Bagi Pendaki Pemula', 'Tips Keselamatan Saat Mendaki Gunung Bagi Pendaki Pemula', '2020-01-07 11:22:27');
INSERT INTO `blog` VALUES (28, 15, 7, 'Tips Tetap Aman dan Selamat Saat Berlibur ke Pantai', 'tips-tetap-aman-dan-selamat-saat-berlibur-ke-pantai.jpg', 'Tips Tetap Aman dan Selamat Saat Berlibur ke Pantai', '<p style=\"text-align: justify;\">Musim liburan sudah tiba. Pilihan lokasi yang sering dipilih wisatawan untuk mengisi liburannya adalah berlibur ke pantai mulai dari yang hanya sekedar melepas penat duduk ditepi pantai, bermain pasir dan berenang di laut serta melakukan aktivitas olah raga air. Namun hingga akhir tahun 2019 tercatat 16 kasus laka laut yang terjadi di pulau Bali. Berikut mimin akan berikan tips tetap aman saat berada di pantai<br /><br />Perhatikan selalu bendera peringatan yang terpasang di bibir pantai ya sobat serta ketahuilah artinya. Misal Secara umum, bendera merah menunjukkan ombak dan arus yang kuat, Bendera kuning menunjukkan ombak sedang dan arus - air cenderung kasar tetapi tidak terlalu berbahaya. Bendera hijau menunjukkan lautan tenang atau jernih namun tetap harus waspada, Bendera biru atau ungu sering menunjukkan bahwa kehidupan laut yang berpotensi berbahaya. Tidak semua pantai cocok untuk berenang, jadi ketahuilah aturannya sebelum kalian ingin berenang di pantai tersebut.<br /><br />Berenang di laut berbeda dengan berenang di kolam atau danau yang tenang, bersiaplah untuk menghadapi ombak. Jika kalian berada di pantai bersama anak atau orang dewasa yang tidak bisa berenang, pastikan setiap orang memiliki pakaian yang pas (lifejacket). Jika Anda berperahu, setiap penumpang harus mengenakan lifejacket berukuran tepat setiap saat. Perlu diingat bahwa dasar laut tidak rata dan pantai dapat berubah secara drastis dari tahun ke tahun. Saat menuju ke air, ketahuilah bahwa dasar laut dapat jatuh secara tiba-tiba, jadi jangan bergerak cepat tanpa bersiap untuk berenang di air.<br /><br />Keberadaan Lifeguard sangat penting, ketahuilah di mana posisi mereka. Jangan bermain jauh jauh dari tempat lifeguard berjaga. Sebagian besar kasus tenggelam terjadi di lokasi yang tidak dijaga lifeguard. Ingat: Kondisi, aturan, dan seluk-beluk setiap pantai berbeda dari satu tempat ke tempat lain. Pada akhirnya, lifeguard yang bertugas harus menjadi tujuan kalian untuk setiap pertanyaan. Mereka ada di sana untuk membantu kita.</p>', 'yes', 'Tips Tetap Aman dan Selamat Saat Berlibur ke Pantai', 'Tips Tetap Aman dan Selamat Saat Berlibur ke Pantai', 'Tips Tetap Aman dan Selamat Saat Berlibur ke Pantai', '2020-01-08 11:22:33');
COMMIT;

-- ----------------------------
-- Table structure for blog_category
-- ----------------------------
DROP TABLE IF EXISTS `blog_category`;
CREATE TABLE `blog_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `thumbnail_alt` varchar(255) DEFAULT NULL,
  `use` enum('yes','no') DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` text NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of blog_category
-- ----------------------------
BEGIN;
INSERT INTO `blog_category` VALUES (7, 'Campaign', '<p>Campaign</p>', 'campaign.jpeg', 'Campaign', 'yes', 'Campaign', 'Campaign', 'Campaign');
INSERT INTO `blog_category` VALUES (8, 'K3 Pariwisata', '<p>K3 Pariwisata</p>', 'k3-pariwisata.jpeg', 'K3 Pariwisata', 'yes', 'K3 Pariwisata', 'K3 Pariwisata', 'K3 Pariwisata');
INSERT INTO `blog_category` VALUES (9, 'K3 Perhotelan', '<p>K3 Pariwisata</p>', 'k3-perhotelan1.jpeg', 'K3 Pariwisata', 'yes', 'K3 Pariwisata', 'K3 Pariwisata', 'K3 Pariwisata');
INSERT INTO `blog_category` VALUES (10, 'K3 Rumah Sakit', '<p>K3 Rumah Sakit</p>', 'k3-rumah-sakit.jpeg', 'K3 Rumah Sakit', 'yes', 'K3 Rumah Sakit', 'K3 Rumah Sakit', 'K3 Rumah Sakit');
INSERT INTO `blog_category` VALUES (11, 'Profesi Milenial', '<p>Profesi Milenial</p>', 'profesi-milenial.jpeg', 'Profesi Milenial', 'yes', 'Profesi Milenial', 'Profesi Milenial', 'Profesi Milenial');
INSERT INTO `blog_category` VALUES (12, 'Stress Management', '<p>Stress Management</p>', 'stress-management.jpeg', 'Stress Management', 'yes', 'Stress Management', 'Stress Management', 'Stress Management');
INSERT INTO `blog_category` VALUES (13, 'Gizi Pekerja', '<p>Gizi Pekerja</p>', 'gizi-pekerja.jpeg', 'Gizi Pekerja', 'yes', 'Gizi Pekerja', 'Gizi Pekerja', 'Gizi Pekerja');
INSERT INTO `blog_category` VALUES (14, 'Undang Undang', '<p>Undang Undang</p>', 'undang-undang.jpeg', 'Undang Undang', 'yes', 'Undang Undang', 'Undang Undang', 'Undang Undang');
COMMIT;

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `thumbnail_alt` varchar(255) DEFAULT NULL,
  `use` enum('yes','no') DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` text NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of category
-- ----------------------------
BEGIN;
INSERT INTO `category` VALUES (6, 1, 'Bali Tours Service', '<p style=\"text-align: justify;\">Bali adalah sebuah provinsi di Indonesia yang ibu kota provinsinya bernama Denpasar. Bali juga merupakan salah satu pulau di Kepulauan Nusa Tenggara. Di awal kemerdekaan Indonesia, pulau ini termasuk dalam Provinsi Sunda Kecil yang beribu kota di Singaraja, dan kini terbagi menjadi 3 provinsi: Bali, Nusa Tenggara Barat, dan Nusa Tenggara Timur.[6][7] Selain terdiri dari Pulau Bali, wilayah Provinsi Bali juga terdiri dari pulau-pulau yang lebih kecil di sekitarnya, yaitu Pulau Nusa Penida, Pulau Nusa Lembongan, Pulau Nusa Ceningan, Pulau Serangan, dan Pulau Menjangan. Secara geografis, Bali terletak di antara Pulau Jawa dan Pulau Lombok. Mayoritas penduduk Bali adalah pemeluk agama Hindu. Di dunia, Bali terkenal sebagai tujuan pariwisata dengan keunikan berbagai hasil seni-budayanya, khususnya bagi para wisatawan Jepang dan Australia. Bali juga dikenal dengan julukan Pulau Dewata dan Pulau Seribu Pura.</p>', 'activities.jpeg', 'Activities', 'yes', 'Bali Tours Service', 'Bali Tours Service', 'Bali Tours Service');
INSERT INTO `category` VALUES (7, 1, 'Bali Honey Month Package', '<p style=\"text-align: justify;\">Bali adalah sebuah provinsi di Indonesia yang ibu kota provinsinya bernama Denpasar. Bali juga merupakan salah satu pulau di Kepulauan Nusa Tenggara. Di awal kemerdekaan Indonesia, pulau ini termasuk dalam Provinsi Sunda Kecil yang beribu kota di Singaraja, dan kini terbagi menjadi 3 provinsi: Bali, Nusa Tenggara Barat, dan Nusa Tenggara Timur.[6][7] Selain terdiri dari Pulau Bali, wilayah Provinsi Bali juga terdiri dari pulau-pulau yang lebih kecil di sekitarnya, yaitu Pulau Nusa Penida, Pulau Nusa Lembongan, Pulau Nusa Ceningan, Pulau Serangan, dan Pulau Menjangan. Secara geografis, Bali terletak di antara Pulau Jawa dan Pulau Lombok. Mayoritas penduduk Bali adalah pemeluk agama Hindu. Di dunia, Bali terkenal sebagai tujuan pariwisata dengan keunikan berbagai hasil seni-budayanya, khususnya bagi para wisatawan Jepang dan Australia. Bali juga dikenal dengan julukan Pulau Dewata dan Pulau Seribu Pura.</p>', 'more-interest.jpeg', 'More Interest', 'yes', 'Bali Honey Month Package', 'Bali Honey Month Package', 'Bali Honey Month Package');
INSERT INTO `category` VALUES (8, 1, 'Bali Meeting Package', '<p style=\"text-align: justify;\">Bali adalah sebuah provinsi di Indonesia yang ibu kota provinsinya bernama Denpasar. Bali juga merupakan salah satu pulau di Kepulauan Nusa Tenggara. Di awal kemerdekaan Indonesia, pulau ini termasuk dalam Provinsi Sunda Kecil yang beribu kota di Singaraja, dan kini terbagi menjadi 3 provinsi: Bali, Nusa Tenggara Barat, dan Nusa Tenggara Timur.[6][7] Selain terdiri dari Pulau Bali, wilayah Provinsi Bali juga terdiri dari pulau-pulau yang lebih kecil di sekitarnya, yaitu Pulau Nusa Penida, Pulau Nusa Lembongan, Pulau Nusa Ceningan, Pulau Serangan, dan Pulau Menjangan. Secara geografis, Bali terletak di antara Pulau Jawa dan Pulau Lombok. Mayoritas penduduk Bali adalah pemeluk agama Hindu. Di dunia, Bali terkenal sebagai tujuan pariwisata dengan keunikan berbagai hasil seni-budayanya, khususnya bagi para wisatawan Jepang dan Australia. Bali juga dikenal dengan julukan Pulau Dewata dan Pulau Seribu Pura.</p>', 'park.jpeg', 'Park', 'yes', 'Bali Meeting Package', 'Bali Meeting Package', 'Bali Meeting Package');
INSERT INTO `category` VALUES (9, 1, 'Bali Activities', '<p>Bali Activities</p>', NULL, 'Bali Activities', 'yes', 'Bali Activities', 'Bali Activities', 'Bali Activities');
INSERT INTO `category` VALUES (10, 1, 'Bali Make Up Artist', '<p>Bali Make Up Artist</p>', NULL, 'Bali Make Up Artist', 'yes', 'Bali Make Up Artist', 'Bali Make Up Artist', 'Bali Make Up Artist');
INSERT INTO `category` VALUES (11, 1, 'Bali Wedding Package', '<p>Bali Wedding Package</p>', NULL, 'Bali Wedding Package', 'yes', 'Bali Wedding Package', 'Bali Wedding Package', 'Bali Wedding Package');
INSERT INTO `category` VALUES (12, 1, 'Lembongan & Gili - Nusa Penida', '<p>Lembongan &amp; Gili - Nusa Penida</p>', NULL, 'Lembongan & Gili - Nusa Penida', 'yes', 'Lembongan & Gili - Nusa Penida', 'Lembongan & Gili - Nusa Penida', 'Lembongan & Gili - Nusa Penida');
INSERT INTO `category` VALUES (13, 2, 'Bali Wisata', '<p>test</p>', 'bali-wisata.jpg', 'Bali Wisata', 'yes', 'Bali Wisata', 'Bali Wisata', 'Bali Wisata');
COMMIT;

-- ----------------------------
-- Table structure for comment
-- ----------------------------
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `thumbnail_alt` varchar(255) DEFAULT NULL,
  `use` enum('yes','no') NOT NULL,
  `date` varchar(255) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of comment
-- ----------------------------
BEGIN;
INSERT INTO `comment` VALUES (3, 'Alexander', 'Praesent mattis commodo augue Aliquam ornare hendrerit augue Cras tellus In pulvinar lectus a est.', 'alexander.jpeg', 'Alexander', 'yes', '');
INSERT INTO `comment` VALUES (4, 'Westley', 'Praesent mattis commodo augue Aliquam ornare hendrerit augue Cras tellus In pulvinar lectus a est.', 'westley.jpeg', 'Westley', 'yes', '');
INSERT INTO `comment` VALUES (5, 'Westley Uy', 'Praesent mattis commodo augue Aliquam ornare hendrerit augue Cras tellus In pulvinar lectus a est.', 'westley-uy.jpeg', 'Westley', 'yes', '');
INSERT INTO `comment` VALUES (6, 'Mahendra', 'Praesent mattis commodo augue Aliquam ornare hendrerit augue Cras tellus In pulvinar lectus a est.', 'mahendra.jpeg', 'Praesent mattis commodo augue Aliquam ornare hendrerit augue Cras tellus In pulvinar lectus a est.', 'yes', '');
COMMIT;

-- ----------------------------
-- Table structure for contact
-- ----------------------------
DROP TABLE IF EXISTS `contact`;
CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `position` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `publish` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of contact
-- ----------------------------
BEGIN;
INSERT INTO `contact` VALUES (4, 'Link', 'kusuma', 'https://www.youtube.com/watch?v=t9-z8EnRCpo', 'yes', '545733.jpg');
COMMIT;

-- ----------------------------
-- Table structure for email
-- ----------------------------
DROP TABLE IF EXISTS `email`;
CREATE TABLE `email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `use` enum('yes','no') NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of email
-- ----------------------------
BEGIN;
INSERT INTO `email` VALUES (3, 'mahendra.adi.wardana@gmail.com', 'yes');
COMMIT;

-- ----------------------------
-- Table structure for gallery
-- ----------------------------
DROP TABLE IF EXISTS `gallery`;
CREATE TABLE `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `thumbnail_alt` text,
  `use` enum('yes','no') DEFAULT 'no',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of gallery
-- ----------------------------
BEGIN;
INSERT INTO `gallery` VALUES (5, 'Mengapa1', '21', 'mengapa1.jpeg', '1', 'yes');
INSERT INTO `gallery` VALUES (7, '1', '2', '1.jpeg', 'tes', 'yes');
INSERT INTO `gallery` VALUES (8, 'af', '', 'af.jpeg', 'fa', 'yes');
INSERT INTO `gallery` VALUES (9, 'af1', '', 'af1.jpeg', 'fa', 'yes');
INSERT INTO `gallery` VALUES (10, 'asdf', '', 'asdf.jpeg', 'asdf', 'yes');
INSERT INTO `gallery` VALUES (11, 'asdf 1', '', 'asdf-1.jpeg', 'asdf', 'yes');
INSERT INTO `gallery` VALUES (12, 'asdfasdf', '', 'asdfasdf.jpeg', 'asdfadf', 'yes');
INSERT INTO `gallery` VALUES (13, 'asdf', '', 'asdf.jpeg', 'asdfasdf', 'yes');
INSERT INTO `gallery` VALUES (14, 'asdfasdf', '', 'asdfasdf.jpeg', 'asdfasdf', 'yes');
COMMIT;

-- ----------------------------
-- Table structure for language
-- ----------------------------
DROP TABLE IF EXISTS `language`;
CREATE TABLE `language` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `code` char(10) DEFAULT NULL,
  `use` enum('yes','no') DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of language
-- ----------------------------
BEGIN;
INSERT INTO `language` VALUES (1, 'English', 'english.png', 'en', 'no');
INSERT INTO `language` VALUES (2, 'Indonesia', 'indonesia.png', 'id', 'yes');
INSERT INTO `language` VALUES (3, 'Chinese', 'chinese.png', 'zh', 'no');
INSERT INTO `language` VALUES (4, 'Japan', 'japan.jpg', 'jp', 'no');
COMMIT;

-- ----------------------------
-- Table structure for pages
-- ----------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `title_sub` varchar(255) DEFAULT NULL,
  `description` text,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `seo` enum('yes','no') DEFAULT 'yes',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pages
-- ----------------------------
BEGIN;
INSERT INTO `pages` VALUES (1, 1, 'Welcome to Harsa Bali Holiday', NULL, '<p>Bali adalah sebuah provinsi di Indonesia yang ibu kota provinsinya bernama Denpasar. Bali juga merupakan salah satu pulau di Kepulauan Nusa Tenggara. Di awal kemerdekaan Indonesia, pulau ini termasuk dalam Provinsi Sunda Kecil yang beribu kota di Singaraja, dan kini terbagi menjadi 3 provinsi: Bali, Nusa Tenggara Barat, dan Nusa Tenggara Timur.[6][7] Selain terdiri dari Pulau Bali, wilayah Provinsi Bali juga terdiri dari pulau-pulau yang lebih kecil di sekitarnya, yaitu Pulau Nusa Penida, Pulau Nusa Lembongan, Pulau Nusa Ceningan, Pulau Serangan, dan Pulau Menjangan. Secara geografis, Bali terletak di antara Pulau Jawa dan Pulau Lombok. Mayoritas penduduk Bali adalah pemeluk agama Hindu. Di dunia, Bali terkenal sebagai tujuan pariwisata dengan keunikan berbagai hasil seni-budayanya, khususnya bagi para wisatawan Jepang dan Australia. Bali juga dikenal dengan julukan Pulau Dewata dan Pulau Seribu Pura.</p>', 'Welcome to Harsa Bali Holiday', 'Welcome to Harsa Bali Holiday', 'Welcome to Harsa Bali Holiday', 'home', 'yes');
INSERT INTO `pages` VALUES (2, 1, 'Services', NULL, NULL, NULL, NULL, NULL, 'services', 'yes');
INSERT INTO `pages` VALUES (3, 1, 'Testimonial', NULL, '<p xss=removed>Bali adalah sebuah provinsi di Indonesia yang ibu kota provinsinya bernama Denpasar. Bali juga merupakan salah satu pulau di Kepulauan Nusa Tenggara. Di awal kemerdekaan Indonesia, pulau ini termasuk dalam Provinsi Sunda Kecil yang beribu kota di Singaraja, dan kini terbagi menjadi 3 provinsi: Bali, Nusa Tenggara Barat, dan Nusa Tenggara Timur.[6][7] Selain terdiri dari Pulau Bali, wilayah Provinsi Bali juga terdiri dari pulau-pulau yang lebih kecil di sekitarnya, yaitu Pulau Nusa Penida, Pulau Nusa Lembongan, Pulau Nusa Ceningan, Pulau Serangan, dan Pulau Menjangan. Secara geografis, Bali terletak di antara Pulau Jawa dan Pulau Lombok. Mayoritas penduduk Bali adalah pemeluk agama Hindu. Di dunia, Bali terkenal sebagai tujuan pariwisata dengan keunikan berbagai hasil seni-budayanya, khususnya bagi para wisatawan Jepang dan Australia. Bali juga dikenal dengan julukan Pulau Dewata dan Pulau Seribu Pura.</p>', 'Testimonial', 'Testimonial', 'Testimonial', 'testimonial', 'yes');
INSERT INTO `pages` VALUES (4, 1, 'Gallery Photo', NULL, '<div class=\"clearfix\" xss=removed> </div>\r\n<div class=\"col-md-12 nopadding\">\r\n<p class=\"font-weight-4 line-height-3 text-justify\" xss=removed>Bali adalah sebuah provinsi di Indonesia yang ibu kota provinsinya bernama Denpasar. Bali juga merupakan salah satu pulau di Kepulauan Nusa Tenggara. Di awal kemerdekaan Indonesia, pulau ini termasuk dalam Provinsi Sunda Kecil yang beribu kota di Singaraja, dan kini terbagi menjadi 3 provinsi: Bali, Nusa Tenggara Barat, dan Nusa Tenggara Timur.[6][7] Selain terdiri dari Pulau Bali, wilayah Provinsi Bali juga terdiri dari pulau-pulau yang lebih kecil di sekitarnya, yaitu Pulau Nusa Penida, Pulau Nusa Lembongan, Pulau Nusa Ceningan, Pulau Serangan, dan Pulau Menjangan. Secara geografis, Bali terletak di antara Pulau Jawa dan Pulau Lombok. Mayoritas penduduk Bali adalah pemeluk agama Hindu. Di dunia, Bali terkenal sebagai tujuan pariwisata dengan keunikan berbagai hasil seni-budayanya, khususnya bagi para wisatawan Jepang dan Australia. Bali juga dikenal dengan julukan Pulau Dewata dan Pulau Seribu Pura.</p>\r\n</div>', 'Gallery Photo', 'Gallery Photo', 'Gallery Photo', 'gallery_photo', 'yes');
INSERT INTO `pages` VALUES (5, 1, 'Gallery Video', NULL, NULL, NULL, NULL, NULL, 'gallery_video', 'yes');
INSERT INTO `pages` VALUES (6, 1, 'Profil Kami', NULL, '<p xss=removed>Bali adalah sebuah provinsi di Indonesia yang ibu kota provinsinya bernama Denpasar. Bali juga merupakan salah satu pulau di Kepulauan Nusa Tenggara. Di awal kemerdekaan Indonesia, pulau ini termasuk dalam Provinsi Sunda Kecil yang beribu kota di Singaraja, dan kini terbagi menjadi 3 provinsi: Bali, Nusa Tenggara Barat, dan Nusa Tenggara Timur.[6][7] Selain terdiri dari Pulau Bali, wilayah Provinsi Bali juga terdiri dari pulau-pulau yang lebih kecil di sekitarnya, yaitu Pulau Nusa Penida, Pulau Nusa Lembongan, Pulau Nusa Ceningan, Pulau Serangan, dan Pulau Menjangan. Secara geografis, Bali terletak di antara Pulau Jawa dan Pulau Lombok. Mayoritas penduduk Bali adalah pemeluk agama Hindu. Di dunia, Bali terkenal sebagai tujuan pariwisata dengan keunikan berbagai hasil seni-budayanya, khususnya bagi para wisatawan Jepang dan Australia. Bali juga dikenal dengan julukan Pulau Dewata dan Pulau Seribu Pura.</p>', 'About Us', 'About Us', 'About Us', 'profile', 'yes');
INSERT INTO `pages` VALUES (7, 1, 'Blog', NULL, NULL, NULL, NULL, NULL, 'blog', 'yes');
INSERT INTO `pages` VALUES (8, 1, 'Contact Us', NULL, '<p>Contact Us</p>', 'Contact Us', 'Contact Us', 'Contact Us', 'contact_us', 'yes');
INSERT INTO `pages` VALUES (9, 1, 'Reservation', NULL, '<p>Reservation</p>', 'Reservation', 'Reservation', 'Reservation', 'reservation', 'yes');
INSERT INTO `pages` VALUES (16, 2, 'Sehat Kerjaku', NULL, '<p>-</p>', 'Sehat Kerjaku - Occupational Health Safety and Environment Education', 'occupational health safety, environment education, OHSE education in the workplace', 'Occupational Health Safety and Environment Education. A Platform for promotive and preventive in OHSE education in the workplace', 'home', 'yes');
INSERT INTO `pages` VALUES (17, 2, 'Testimonial ID', NULL, '<p>Testimonial ID</p>', 'Testimonial ID', 'Testimonial ID', 'Testimonial ID', 'testimonial', 'yes');
INSERT INTO `pages` VALUES (18, 2, 'Galeri Foto', 'Daftar kegiatan kami di Sehat Kerjaku.', '<div class=\"about-section-title\">\r\n<h4>Galeri Foto, Sehat Kerjaku</h4>\r\n</div>\r\n<p>Penjelasan singkat tentang galeri foto sehat kerjaku, khususnya mengenai pengantar tentang layanan yang disediakan. There are many use variations of passages of Lorem Ipsum available, but the have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly. If you are going to use a passage of Lorem Ipsum, you need to be sure there .</p>', 'Galeri Foto', 'Galeri Foto', 'Galeri Foto', 'gallery_photo', 'yes');
INSERT INTO `pages` VALUES (19, 2, 'Gallery Video id', NULL, '<p>Gallery Video id</p>', 'Gallery Video id', 'Gallery Video id', 'Gallery Video id', 'gallery_video', 'yes');
INSERT INTO `pages` VALUES (20, 2, 'Profil Kami - Sehat Kerjaku', 'Penjelasan', '<p align=\"justify\"><strong>Sehat Kerjaku</strong> adalah startup digital di bidang Kesehatan dan keselamatan kerja di Indonesia yang berfokus pada layanan bebasis edukasi kesehatan keselamatan kerja serta lingkungan. Sehat kerjaku bersama para mitra baik akedimisi, praktisi di bidang K3 berkomitmen untuk mengembangkan layanan K3 bebasis teknologi, termasuk memberikan virtual edukasi k3 yang dikemas dan disampaikan secara kekinian, platform training online, marketplace konsultasi k3, serta konten-konten K3 lainnya yang bisa diakses melalui web dan aplikasi sehat kerjaku. Perusahaan ini didirikan pada tahun 2019.</p>\r\n<p> </p>\r\n<p align=\"justify\"><strong>Sehat Kerjaku</strong> percaya bahwa pemenuhan K3 adalah hak seluruh pekerja di tempat kerja. Kami meyakini bawa K3 adalah awal dari kesejahteraan pekerja. Maka dari itu sehat kerjaku memiliki visi dan misi sebagai berikut:</p>\r\n<p> </p>\r\n<p align=\"justify\"><strong>Visi :</strong> <br> Sebagai Platform dan mitra bertukar ilmu serta pengalaman bagi komunitas bergerak di bidang ketenagakerjaan, Sistem manajemen K3 baik profesional maupun akademisi di Indonesia</p>\r\n<p> </p>\r\n<p align=\"justify\"><strong>Misi :</strong></p>\r\n<ol>\r\n<li>Membantu Pemerintah dalam melakukan sosialisasi, pembinaan, pengendalian K3 di tempat kerja sesuai dengan UU dan peraturan K3</li>\r\n<li>Memberdayakan dan meningkatkan kompetensi profesi ahli K3</li>\r\n<li>Memberikan manfaat bagi anggota dan mitra serta mendorong terbentuknya masyarakat Indonesia berbudaya K3</li>\r\n</ol>', 'Profil Kami - Sehat Kerjaku', 'Profil Kami - Sehat Kerjaku', 'Profil Kami - Sehat Kerjaku', 'profile', 'yes');
INSERT INTO `pages` VALUES (21, 2, 'Kontak Kami', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis aliquet eros. Integer placerat ultricesLorem ipsum dolor sit amet, consectetur adipiscing elit. In quis .', '<p>-</p>', 'Kontak Kami', 'Kontak Kami', 'Kontak Kami', 'contact_us', 'yes');
INSERT INTO `pages` VALUES (22, 2, 'Reservation id', NULL, '<p>Reservation id</p>', 'Reservation id', 'Reservation id', 'Reservation id', 'reservation', 'yes');
INSERT INTO `pages` VALUES (23, 2, 'Layanan Kami', 'Kami menyediakan pelayanan tentang kesehatan di masyarakat dan lain sebagainya.', '<div class=\"about-section-title\">\r\n<h4>Tentang Layanan Kami, Sehat Kerjaku</h4>\r\n</div>\r\n<p>Penjelasan singkat tentang yang dilakukan di sehat kerjaku, khususnya mengenai pengantar tentang layanan yang disediakan. There are many use variations of passages of Lorem Ipsum available, but the have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly. If you are going to use a passage of Lorem Ipsum, you need to be sure there .</p>', 'Layanan Kami', 'Layanan Kami', 'Layanan Kami', 'services', 'yes');
INSERT INTO `pages` VALUES (24, 2, 'Daftar Artikel Sehat Kerjaku', 'Baca Artikel', '<p>Berikut merupakan daftar artikel yang sudah kami buat, untuk semua teman-teman sehat kerjaku selalu mengikuti keselamatan saat bekerja di bidang apapun.</p>', 'Artikel', 'Artikel', 'Artikel', 'blog', 'yes');
INSERT INTO `pages` VALUES (25, 2, 'Sehat Kerjaku', NULL, '<p>Occupational Health Safety and Environment Education. <br>A Platform for promotive and preventive in OHSE education in the workplace</p>', NULL, NULL, NULL, 'home_sesi_1', 'no');
INSERT INTO `pages` VALUES (26, 2, 'Perkenalan Singkat Kami', 'Tentang Sehat Kerjaku', '<p>Sehat kerjaku adalah startup digital di bidang Kesehatan dan<br>Keselamatan kerja di Indonesia yang berfokus pada layanan<br>Berbasis Edukasi kesehatan keselamatan kerja serta lingkungan.</p>', NULL, NULL, NULL, 'home_sesi_2', 'no');
INSERT INTO `pages` VALUES (27, 2, 'Layanan Yang Kami Berikan', 'Layanan Kami', '<p>Pelayanan kesehatan adalah sebuah konsep yang digunakan dalam<br>memberikan layanan kesehatan kepada masyarakat.<br>Definisi pelayanan kesehatan menurut Prof. Dr. Soekidjo Notoatmojo adalah <br>sebuah sub sistem pelayanan kesehatan yang tujuan utamanya adalah <br>pelayanan preventif (pencegahan) dan promotif( peningkatan kesehatan ) dengan sasaran masyarakat.</p>', NULL, NULL, NULL, 'home_sesi_3', 'no');
INSERT INTO `pages` VALUES (28, 2, 'Footer', '', '<p>Occupational Health Safety and Environment Education.<br>A Platform for promotive and preventive in OHSE education in the workplace<br>email : <a title=\"Email Sehat Kerjaku\" href=\"mailto:sehatkerjaku@gmail.com\" target=\"_blank\">sehatkerjaku@gmail.com</a> <br>hp : <a href=\"tel:081934364063\">081 934 364 063</a><br>alamat : <a href=\"https://goo.gl/maps/fAp3Bzo5bv4FE8E57\" target=\"_blank\">Laplapan, Ubud, Gianyar, Bali</a></p>', NULL, NULL, NULL, 'footer', 'no');
COMMIT;

-- ----------------------------
-- Table structure for reservation
-- ----------------------------
DROP TABLE IF EXISTS `reservation`;
CREATE TABLE `reservation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tour` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` int(50) NOT NULL,
  `address` text,
  `country` varchar(255) NOT NULL,
  `tour_start` varchar(255) NOT NULL,
  `total_adult` int(50) NOT NULL,
  `total_children` int(50) NOT NULL,
  `message` int(50) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of reservation
-- ----------------------------
BEGIN;
INSERT INTO `reservation` VALUES (1, NULL, 'mr', 'kusuma', 'wardana', 'kadek.kusuma.wardana@gmail.com', 895616869, NULL, 'indonesia', 'besok', 1, 1, 1, '0000-00-00 00:00:00');
INSERT INTO `reservation` VALUES (2, 3, 'Mr.', 'Mahendra', 'Wardana', 'mahendra.adi.wardana@gmail.com', 2147483647, 'Jalan Ratna', 'Indonesia', '06/10/2019 6:27 AM', 1, 0, 0, '2019-10-06 06:29:50');
COMMIT;

-- ----------------------------
-- Table structure for slider
-- ----------------------------
DROP TABLE IF EXISTS `slider`;
CREATE TABLE `slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `thumbnail_alt` varchar(255) DEFAULT NULL,
  `url` varchar(255) NOT NULL,
  `use` varchar(255) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of slider
-- ----------------------------
BEGIN;
INSERT INTO `slider` VALUES (6, 1, 'Harsa Bali Holiday Title', 'Bring your holiday more interesting', 'harsa-bali-holiday-title.jpg', 'Bring your holiday more interesting', '-', 'yes');
INSERT INTO `slider` VALUES (8, 1, 'Holiday More be Fun', 'Harsa Bali Holiday is your partner holiday', 'holiday-more-be-fun.jpg', 'Holiday More be Fun', '-', 'yes');
COMMIT;

-- ----------------------------
-- Table structure for team
-- ----------------------------
DROP TABLE IF EXISTS `team`;
CREATE TABLE `team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `thumbnail_alt` text,
  `use` enum('yes','no') DEFAULT 'no',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of team
-- ----------------------------
BEGIN;
INSERT INTO `team` VALUES (15, 'Etick Pristyan Dewi', 'Team Hore', 'etick-pristyan-dewi.jpeg', NULL, 'yes');
INSERT INTO `team` VALUES (16, 'Ida Ayu Indira Dwika Lestari', 'CEO', 'ida-ayu-indira-dwika-lestari.jpeg', NULL, 'yes');
INSERT INTO `team` VALUES (17, 'Made Awi', 'CPO', 'made-awi.jpeg', NULL, 'yes');
INSERT INTO `team` VALUES (18, 'Irwan', 'COO', 'irwan.jpeg', NULL, 'yes');
INSERT INTO `team` VALUES (19, 'Gung De', 'CIO', 'gung-de.jpeg', NULL, 'yes');
COMMIT;

-- ----------------------------
-- Table structure for tour
-- ----------------------------
DROP TABLE IF EXISTS `tour`;
CREATE TABLE `tour` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) DEFAULT NULL,
  `id_category` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `title_sub` varchar(255) DEFAULT NULL,
  `use` enum('yes','no') DEFAULT NULL,
  `description` text NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `thumbnail_alt` varchar(255) NOT NULL,
  `meta_title` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tour
-- ----------------------------
BEGIN;
INSERT INTO `tour` VALUES (5, 2, '', 'Education, Sosialitation and Training', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent a viverra leo.', 'yes', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent a viverra leo. Morbi purus\n                            augue, lacinia vel molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit.\n                            Praesent a viverra leo. Morbi purus augue, lacinia vel molestie. Lorem ipsum dolor sit amet,\n                            consectetur adipiscing elit. Praesent a viverra leo. Morbi purus augue, lacinia vel\n                            molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent a viverra\n                            leo.</p>', 'education--sosialitation-and-training.png', '', 'Education, Sosialitation and Training', 'Education, Sosialitation and Training', 'Education, Sosialitation and Training', '2020-01-07 07:51:22', '2020-01-07 08:01:50');
INSERT INTO `tour` VALUES (6, 2, '', 'Health Risk Assesment', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent a viverra leo.', 'yes', '', 'health-risk-assesment.png', '', 'Health Risk Assesment', 'Health Risk Assesment', 'Health Risk Assesment', '2020-01-07 07:59:36', '2020-01-07 08:02:52');
INSERT INTO `tour` VALUES (7, 2, '', 'HSE for Event', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent a viverra leo.', 'yes', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent a viverra leo. Morbi purus\n                            augue, lacinia vel molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit.\n                            Praesent a viverra leo. Morbi purus augue, lacinia vel molestie. Lorem ipsum dolor sit amet,\n                            consectetur adipiscing elit. Praesent a viverra leo. Morbi purus augue, lacinia vel\n                            molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent a viverra\n                            leo.</p>', 'hse-for-event.png', '', 'HSE for Event', 'HSE for Event', 'HSE for Event', '2020-01-07 08:00:19', '2020-01-07 08:02:59');
INSERT INTO `tour` VALUES (8, 2, '', 'Fire Risk Assesment', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent a viverra leo.', 'yes', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent a viverra leo. Morbi purus\n                            augue, lacinia vel molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit.\n                            Praesent a viverra leo. Morbi purus augue, lacinia vel molestie. Lorem ipsum dolor sit amet,\n                            consectetur adipiscing elit. Praesent a viverra leo. Morbi purus augue, lacinia vel\n                            molestie.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent a viverra\n                            leo.</p>', 'fire-risk-assesment.png', '', 'Fire Risk Assesment', 'Fire Risk Assesment', 'Fire Risk Assesment', '2020-01-07 08:00:42', '2020-01-07 08:03:06');
COMMIT;

-- ----------------------------
-- Table structure for tour_gallery
-- ----------------------------
DROP TABLE IF EXISTS `tour_gallery`;
CREATE TABLE `tour_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_tour` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `thumbnail_alt` text,
  `use` enum('yes','no') DEFAULT 'no',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tour_gallery
-- ----------------------------
BEGIN;
INSERT INTO `tour_gallery` VALUES (5, NULL, 'Mengapa1', '21', 'mengapa11.jpeg', '1', 'yes');
INSERT INTO `tour_gallery` VALUES (7, NULL, '1', '2', '15.jpeg', 'tes', 'yes');
INSERT INTO `tour_gallery` VALUES (9, 3, '11', '', '11.jpeg', '31', 'yes');
INSERT INTO `tour_gallery` VALUES (10, 3, '1', '', '1.jpeg', '2', 'yes');
INSERT INTO `tour_gallery` VALUES (11, 3, '2', '', '2.jpeg', '3', 'yes');
INSERT INTO `tour_gallery` VALUES (12, 3, 'Gili Gili Fast Boat', '', 'gili-gili-fast-boat.jpeg', '2', 'yes');
INSERT INTO `tour_gallery` VALUES (13, 3, '1', '', '1.jpeg', '2', 'yes');
INSERT INTO `tour_gallery` VALUES (14, 3, '1 2', '', '1-2.jpeg', '2', 'yes');
INSERT INTO `tour_gallery` VALUES (15, 4, '1', '', '1.jpeg', 'test', 'yes');
INSERT INTO `tour_gallery` VALUES (16, 5, '1', '', '1.jpeg', '2', 'yes');
COMMIT;

-- ----------------------------
-- Table structure for video
-- ----------------------------
DROP TABLE IF EXISTS `video`;
CREATE TABLE `video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `video` varchar(255) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of video
-- ----------------------------
BEGIN;
INSERT INTO `video` VALUES (3, 'kusuma', 'film eksklusif kusuma ', '<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/GOEf6IaCnjU\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
