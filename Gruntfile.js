module.exports = function (grunt) {

    var jsFiles = [
        'assets/template_front/js/vendor/jquery-2.2.4.min.js',
        'assets/template_front/js/popper.min.js',
        'assets/template_front/js/bootstrap.min.js',
        'assets/template_front/js/waypoints.min.js',
        'assets/template_front/js/counterup.min.js',
        'assets/template_front/js/meanmenu.min.js',
        'assets/template_front/js/aos.min.js',
        'assets/template_front/js/isotope.min.js',
        'assets/template_front/js/jquery.backgroundMove.js',
        'assets/template_front/js/slick.min.js',
        'assets/template_front/js/scrollUp.js',
        'assets/template_front/js/masonry.js',
        'assets/template_front/js/fancybox.js',
        'assets/template_front/js/magnific-popup.min.js',

        'assets/template_front/js/main.js',
    ];
    var cssFiles = [
        'assets/template_front/css/bootstrap.min.css',
        'assets/template_front/css/magnific-popup.min.css',
        'assets/template_front/css/aos.min.css',
        'assets/template_front/css/meanmenu.css',
        'assets/template_front/css/style.css',
        'assets/template_front/css/responsive.css',
        'assets/template_front/css/fancybox.css'
    ];
    var allFiles = jsFiles.concat(cssFiles); // merge js & css files directory

    grunt.initConfig({
        jsDistDir: 'js/',
        cssDistDir: 'assets/template_front/css/',
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            js: {
                options: {
                    separator: ';'
                },
                src: jsFiles,
                dest: '<%=jsDistDir%><%= pkg.name %>.js'
            },
            css: {
                src: cssFiles,
                dest: '<%=cssDistDir%><%= pkg.name %>.css'
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
                    '<%=jsDistDir%><%= pkg.name %>.min.js': ['<%= concat.js.dest %>']
                }
            }
        },
        cssmin: {
            add_banner: {
                options: {
                    banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
                },
                files: {
                    '<%=cssDistDir%><%= pkg.name %>.min.css': ['<%= concat.css.dest %>']
                }
            }
        },
        watch: {
            files: allFiles,
            tasks: ['concat', 'uglify', 'cssmin']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', [
        'concat',
        'uglify',
        'cssmin',
        'watch'
    ]);

};